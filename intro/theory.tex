\chapter{Tokamak Plasma Physics} \label{sec:fusion_plasmas}

There are several concepts in tokamak plasma physics that are important to understand. This chapter is a more detailed look into the pedestal physics, including the \ac{hmode}, the \acp{elm} and the \acs{eped} model; the plasma-wall interactions, and the relevant empirical limits and scalings that are used in this work.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Pedestal Physics}

The pedestal can be a crucial region to understand, as it is the connection between the hot core and the cold edge. As confinement improves, these two regions become more different and the pedestal becomes more pronounced, and the confinement of the whole system can depend on the physics of the pedestal.

\subsection{High Confinement Mode - H-Mode}

Without any external heating, the plasma is in an ohmic mode, where the plasma heats from its own current and resistivity. Since this is not sufficient to reach fusion temperatures, extra heating is necessary. This regime with external heating, \ac{lmode}, did not improve confinement with increased heating power, as plasma turbulence increased the transport. The \ac{hmode} was discovered in 1982 on the ASDEX machine, as a regime where confinement increased significantly compared to \ac{lmode} \cite{wagner_regime_1982,wagner_quarter-century_2007}. It was found that this regime could be triggered under specific conditions when power was increased over a certain threshold \cite{burrell_physics_1992,birkenmeier_role_2023}.

The \ac{hmode} improves confinement due to the creation of an edge transport barrier that decreases turbulence \cite{wagner_development_1984,groebner_role_1990,field_measurement_1992,terry_suppression_2000}. This barrier is formed from a strengthening of the radial electrical field that decreases the perpendicular plasma flow \cite{groebner_progress_2001,burrell_confinement_1989,silva_structure_2021,silva_effect_2022}, as the two are related through the pressure balance \cite{wesson_tokamaks_2004}
\begin{equation}
  n\ e \left( E_r + v_\perp B \right) = - \frac{\mathrm{d} p}{\mathrm{d} r} \text{ ,}
\end{equation}
where $E_r$ is the radial electric field and $v_\perp$ the perpendicular particle flow velocity.

\Cref{fig:hmode_efield} shows two profiles for the radial electric field, one in \ac{lmode} (blue) and one in \ac{hmode} (red). The minimum of the field in \ac{hmode} is an order of magnitude stronger than the one in \ac{lmode}, which can explain the formation of the transport barrier that increases the pedestal gradients.

\begin{figure}
  \centering
  \includegraphics[width=.8\textwidth]{images/theory/hmode_efield}
  \caption[Radial Electric Field Profiles in L-Mode and H-Mode]{Radial electric field profiles for \ac{lmode} and \ac{hmode} in ASDEX \cite{schneider_characterization_2012,viezzer_high-resolution_2012}.}
  \label{fig:hmode_efield}
\end{figure}

\subsection{Edge Localised Mode - ELM}
\label{sec:elms}

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{images/theory/elm_signals}
  \caption[ELMs as seen by different signals]{ELMs as seen by different signals: phase transitions for Beryllium and Hydrogen, line-average density in the core region, the stored energy in the plasma and the total radiated power. These signals show how ELMs affect the entire system.}
  \label{fig:elm_signals}
\end{figure}

One of the most noticeable downsides of the \ac{hmode} is the new instabilities that it creates: the \acp{elm}. The transport barrier slowly increases the plasma pressure and energy, and once a certain threshold is reached, a crash occurs \cite{loarte_chaos_2006}. This crash is a relaxation and releases a burst of particles and energy, and after a short interval, the barrier starts rebuilding, increasing the gradients once again. \Acp{elm} are then a quasi-periodic instability, and depending on the conditions, their frequency and intensity can vary. \Acp{elm} with large amplitudes and lower frequency are called Type I, and on the other extreme, there are the Type III \acp{elm} with high frequency and low amplitude. In the middle exists the Type II \ac{elm}, a rarer type with frequencies and amplitudes in between the other two types.

\acp{elm} can affect the entire system, across the entire plasma and their influence can be seen in many different places. \Cref{fig:elm_signals} shows an experiment with constant \acp{elm} and their impact on various signals. The divertor photon fluxes indicate that numerous particles, both fuel (D-alpha line) and impurities (Be II line) reach both divertors during an \ac{elm}. The interferometer shows a drop in core density, consistent with the particle loss. There is also a noticeable loss in the stored energy, which is reflected in large peaks of radiated power. The edge density profile also sees a noticeable impact, and \cref{sec:time_evo} explores the behaviours of the different plasma and profile parameters during \acp{elm} in more depth.

\Acp{elm} have some benefits and downsides. The clear downside is the large transient power load that it creates, which can cause damage to any surface affected, and in the case of Type I \acp{elm} this damage can be enough to make the lifetime of components unacceptably short. The core confinement is also affected by \acp{elm}, particularly in the case of the Type III \acp{elm}. However, \acp{elm} can also improve operations, by expelling impurities; and by preventing the plasma from reaching conditions where stronger instabilities, that could collapse confinement, could occur. \ac{elm}y modes are usually preferred for these reasons, and to reduce their peak power loads, techniques such as magnetic perturbations \cite{evans2005suppression,liang2007active}, fast vertical plasma motion (also called 'vertical kicks') \cite{de2015understanding,artola2018non} and pellet pacing \cite{lang2003elm} are in active development.

\subsection{ELM and Pedestal Model --- EPED}

To simplify the study of fusion plasmas, the \ac{mhd} model is used. \Ac{mhd} theory treats the plasma as an electrically charged single-fluid, combining fluid equations with Maxwell's equations. This reduces the complexity of a system with $10^{20}$ charged particles interacting with each other and external forces, to a limited set of equations. However, the \ac{mhd} model does not accurately describe all behaviours in a tokamak plasma, and in many situations, a kinetic model or simulations are required to study a particular effect.

The relative simplicity of \ac{mhd} allows for the study of the macroscopic behaviours of the plasma, including its equilibrium and stability. It is also common to make certain assumptions when using the \ac{mhd} theory, particularly that the plasma is perfectly conducting, resulting in what is called the ideal \ac{mhd}. Studying the limits of the ideal \ac{mhd} models is key to understanding the limits of real tokamak plasmas, as there are several types of \ac{mhd} instabilities that can disrupt the plasma, and potentially even damage the components. The instabilities can be divided into internal and external modes, pressure-driven and current-driven modes.

Magnetic field lines can become "bent" when a current-driver perturbation appears, creating a kink, giving the name to the kink instabilities, sometimes also called peeling. Ballooning instabilities are pressure-driven, when plasma perturbations concentrate in regions of unfavourable curvature, distorting the magnetic flux lines similarly to a balloon expanding. Kink instabilities can be stabilised by increasing the pressure gradient while ballooning instabilities can be stabilised by increasing the edge current. The interplay between the limits of pressure and current makes stability at the edge a complex problem, worsened by the possibility of instabilities that are a result of the combination of both, the \ac{pb} instabilities \cite{wilson_numerical_2002,snyder_edge_2002}.

The \ac{eped} model combines these \ac{mhd} instabilities with the pressure gradient instabilities to create a model that can predict both the pedestal structure and the \ac{elm} stability \cite{snyder_edge_2002,snyder_first-principles_2011}. In the \ac{eped} model, two quantities are crucial: the normalised edge current, $j_{ped}$; and the normalised pressure gradient $\alpha$, defined as \cite{stefanikova_pedestal_2020}
\begin{equation}
  \alpha = - \frac{2\partial_\psi V}{\left(2 \pi\right)^2}\left(\frac{V}{2 \pi^2 R}\right)^{1/2} \mu_0\ \partial_\psi p \text{ ,}
\end{equation}
where $\partial_\psi$ is the derivative in poloidal flux $\psi$, $V$ is the plasma volume enclosed by the flux surface, $R$ the major radius and $p$ the plasma pressure. \Ac{pb} stability can be obtained using \ac{mhd} solvers, that first calculate the equilibrium to obtain $j_{ped}$ and $\alpha$, and then evaluate the growth rate of the different instabilities for that case. A sketch showing the type of analysis done is presented in \cref{fig:alpha_j_plot}, where the stability limit is shown as a red line with the initial operating conditions shown as a red star. During a \ac{pb} instability, the plasma pressure/current will increase towards the limit, and upon reaching it, a crash occurs, quickly reducing the pressure/current back into stability.

\begin{figure}
  \centering
  \includegraphics[width=.6\textwidth]{images/theory/alpha_j_plot}
  \caption[Sketch of Peeling-Balloning Stability Limit]{Sketch of peeling-ballooning stability limit as a function of normalised pressure gradient and edge current \cite{stefanikova_pedestal_2020}.}
  \label{fig:alpha_j_plot}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Edge Physics --- Plasma-Wall Interaction}

While the magnetic fields confine most of the plasma, there is always a fraction, mostly outside the separatrix, that will escape confinement. There will be interactions between the escaping particles and radiation with the surrounding wall. These interactions will generate impurities composed of the material of the wall or other elements trapped in the wall. Impurities can become problematic as their elements are heavier than the hydrogen fuels. A heavier particle will have a larger radiation loss, and when ionising, each atom will release many electrons, diluting the plasma mix and decreasing the efficacy of heating. Several processes release atoms from the wall: sputtering, arcing and evaporation. Sputtering occurs when energetic particles collide with a solid surface, releasing atoms from it. Arcing is created by strong enough potentials in the regions close to the surface. And high enough radiation can heat the surface to the point of evaporation.

An ion that escapes confinement and collides with a solid material may be neutralised and return to the plasma, due to backscattering or some other process. This phenomenon creates a flow of neutral particles back to plasma, where they will be again ionised. This process is known as recycling and in a tokamak, ions will go through recycling several times during the experiment.

\subsection{Divertor}

To protect the walls against erosion and thermal loads, and to decrease the fraction of impurities, limiters and divertors are used. In its simplest form, a limiter is some solid material placed closer to the wall, and it becomes the edge of the plasma. Limiters have the advantages of being made of materials that can handle high thermal loads, such as carbon or tungsten; and of localising the plasma-wall interactions, including particle recycling. However, since the limiter is close to the core plasma, impurities released can easily penetrate the confined plasma and ionise inside it.

A divertor configuration attempts to solve this problem by increasing the distance between the core plasma and the region where impurities are ionised. While there are many possible designs, the most common has a toroidally symmetric divertor and a magnetic configuration that creates a null point and guides the open magnetic surfaces to the divertor.

The divertor configuration reduces the impurity content of the plasma by reducing the flow of neutral particles to the core, and by creating a region where neutrals can ionise in the \ac{sol}, confining them to the open field lines that end at the divertor. It also moves the power flow from alpha particles from a solid surface to a plasma medium that can be more easily used to generate electricity. And finally, impurities that escape confinement will concentrate on the divertor region, reducing their impact on the core plasma. There is also active pumping at this region, that helps remove impurities and improve confinement.

There is a regime called detached divertor where the ionisation region moves closer to the core, and density and temperature have a sharper drop closer to the wall. In this regime, the power load on the divertor decreases, but its ability to remove unwanted particles also reduces.

\subsection{Scrape-Off Layer --- SOL}

\begin{figure}
  \centering
  \includegraphics[width=.6\textwidth]{images/theory/sol_shoulder_lit}
  \caption[Example of SOL Shoulders]{Example of \ac{sol} regions with and without shoulder in \ac{lmode} at \ac{jet} \cite{wynn_investigation_2018}.}
  \label{fig:sol_shoulder}
\end{figure}

As seen before, the \ac{sol} is the closest region of the plasma to the wall. In a simplified model of a 1D limiter with spacing equal to 2L, in a steady state, the perpendicular diffusion flow is balanced by the flow of particles along the open field lines, assuming no other sources or sinks. The resulting equation is \cite{stangeby_plasma_2000}
\begin{equation}
  \frac{\mathrm{d}}{\mathrm{d} r} \left[-D_\perp \frac{\mathrm{d} n}{\mathrm{d}r}\right] = \frac{n c_s}{L} \text{ ,}
\end{equation}
where $D_\perp$ is the perpendicular diffusion coefficient and $c_s$ is plasma speed sound which is in the order of the parallel velocity of particles. If $D_\perp$ and $c_s$ are assumed to be independent of $r$, the resulting integration resolves into
\begin{equation}
  n(r) = n(r_0) \exp{\left(-\frac{r - r_0}{\lambda_r}\right)} \label{eq:sol_density} \text{ ,}
\end{equation}
with
\begin{equation}
  \lambda_r = \sqrt{\frac{D_\perp L}{c_s}} \text{ ,}
\end{equation}
where $r_0$ is the minor radius at the limiter and $\lambda_r$ is the \ac{sol} width, also called \acs{lsol} in this thesis. While this is a simplified model, with several different assumptions, most \ac{sol} profiles studied in this work present exponential behaviours.

Under some conditions, the \ac{sol} can divide into two regions with different behaviours. Close to the separatrix is the near \ac{sol}, which exhibits the expected steep gradient. The far \ac{sol}, close to the wall, has a flatter profile. This higher density and temperature will increase the power load on the wall, potentially over the limit of safe operation. This regime was first observed in \ac{lmode} and the generally accepted cause is the increase of particle transport at high enough collisionality. More recent studies have shown the shoulder can also appear in \ac{hmode} and that other factors, such as divertor condition and interactions with neutrals, may be necessary for the shoulder to form \cite{wynn_investigation_2018,sun_broadening_2022,stagni_effect_2024,carralero_study_2017}. \Cref{fig:sol_shoulder} shows two \ac{jet} experiments in \ac{lmode}, one with and one without a \ac{sol} shoulder.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Empirical Limits and Scalings}

Due to the high complexity of the physics in tokamak plasmas, several behaviours have only been well described through empirical methods. By analysing data from multiple different machines and operating conditions, it was possible to discover new stability limits, that restrict the working parameters; and scaling laws, that help predict and design future machines.

% \subsection{Limits on Stability}

\subsection{Pressure Limit}

The plasma pressure is a parameter with a big influence on the overall stability. It is possible to normalise it to the magnetic pressure using
\begin{equation}
  \beta = \frac{p}{B_0^2 / 2 \mu_0} \text{ ,}
\end{equation}
where $p$ is the plasma pressure and $B_0$ is the total magnetic field strength. \Ac{beta} can give a measure of the efficiency of the magnetic field in confining the plasma. Higher \acs{beta} improves the power balance but also increases the frequency and amplitude of instabilities. An upper limit for \ac{beta}, called Troyon limit, has been found \cite{troyon_mhd-limits_1984}, above which the plasma becomes very unstable. The \ac{betan} is defined as
\begin{equation}
  \beta_N = 100 \beta \frac{a B_T}{I_p} \text{ ,}
\end{equation}
where \acs{bt} is the \acl{bt} in T and \acs{ip} is the \acl{ip} in MA. \Acs{betan} includes the factor of 100, to behave as a percentage with values in the low units - the current empirical studies place the limit at $\beta_N = 3.5$, though it has been possible to obtain much higher values.

\begin{figure}
  \centering
  \includegraphics[height=.4\textheight]{images/intro/ipb98.jpg}
  \caption[IPB98 scaling of confinement time]{The \acs{ipb98} scaling for the \acf{taue} with experiments from all \acs{hmode} machines, including a prediction for \acs{iter} \cite{iter_physics_basis_editors_chapter_1999-2}.}
  \label{fig:ipb98}
\end{figure}

\subsection{Density Limit}

A limit was also found on the maximum density of a tokamak plasma, as reaching critical edge densities can trigger large instabilities. The empirical \ac{ngw} is calculated as \cite{greenwald_new_1988,greenwald_density_2002}
\begin{equation}
  n_{GW} = \frac{I_p}{\pi a^2} \text{ ,}
\end{equation}
where $n_{GW}$ is in $\times 10^{20}\ $m$^{-3}$, $I_p$ in MA and $a$ in m. This allows the measurement of how close to the limit a certain plasma is by using the \ac{fgw} defined as
\begin{equation}
  f_{GW} = \frac{n_e}{n_{GW}} \text{ ,}
\end{equation}
and in normal operation, \acs{fgw} lies between 0.5 and 0.9.

\subsection{Energy Confinement Time Scaling - H-factor to IPB98}

In preparation for \ac{iter}, a large \ac{hmode} database was created to create scalings that could predict the conditions at \ac{iter}, named after the document written, \ac{ipb98}. Scalings were developed for many of the parameters present in a tokamak plasma, and the most important is one scaling for \ac{taue} defined as \cite{iter_physics_basis_editors_chapter_1999-2}
\begin{equation}
  \tau_{E,IPB98y2} = 0.0562 I_p^{0.93} B_T^{0.15} \left\langle n_e \right\rangle^{0.41} P_{loss}^{-0.69} R^{1.97} \kappa_a^{0.78} \epsilon^{0.58} M^{0.19} \text{ ,}
  \label{eq:taue_ipb98y2}
\end{equation}
where $\left\langle n_e \right\rangle$ is the average electron density, $P_{loss}$ is the power lost through the separatrix, \acs{epsilon} is the \acl{epsilon} defined as $\epsilon = a/R$ and $M$ is the ion mass.

It is then possible to compare the \ac{taue} of any particular experiment with the predicted value from \ac{ipb98}, creating a new metric called \ac{h98} defined as
\begin{equation}
  H_{98} = \frac{\tau_E}{\tau_{E,IPB98y2}} \text{.}
\end{equation}
This allows the easy comparison of scenarios and machines, for example, a regular \ac{hmode} experiment would have $H_{98} \approx 1.0$ while \ac{lmode} would have $H_{98} \ll 1.0$.

% \subsection{Plasma Energy Scaling}

