\chapter{Reflectometry} \label{sec:reflectometry}

\section{Wave Propagation}

The charged particles—ions and electrons—in a plasma are influenced by electromagnetic forces and, in turn, contribute to those same forces. As ions are considerably heavier than electrons (a proton weighs about 1836 times more than an electron), it is possible to assume that, at small (temporal and spatial) scales, only electrons are moving.

Consider a quasi-neutral plasma where a small perturbation of charge is introduced, for instance by moving some electrons. The charge differential will generate a Coulomb force and electrons will move to restore the quasi-neutrality, and will naturally oscillate a certain frequency, called the \ac{wpe} given by
\begin{equation}
    \omega_{pe}^2 = \frac{e^2 n_e}{m_e \epsilon_0} \label{eq:wpe} \text{ ,}
\end{equation}
where $e$ is the electron charge, $n_e$ is the electron density, $m_e$ is the electron mass and $\epsilon_0$ is the vacuum permittivity.
% At JET, a typical pedestal density is $5 \times 10^{19} \text{m}^{-3}$, which results in an electron plasma frequency of around 63 GHz.

Electrons will also orbit around
%field lines of a
magnetic field lines $B$, with a \ac{wce} given by
\begin{equation}
    \omega_{ce} = \frac{e B}{m_e} \label{eq:wce} \text{ .}
\end{equation}
% which varies from 50 GHz (edge) to 100 GHz (core) depending on position, for a typical magnetic field at JET of 2.5T.

\begin{figure}
    \centering
    \includegraphics[width=\linewidth]{images/reflectometry/dispersion}
    \caption[Dispersion Diagram for O and X-modes]{Dispersion diagram for O and X-modes.}
    \label{fig:dispersion}
\end{figure}

The dispersion relation of a wave in a plasma depends on the polarisation of the wave in relation to the external magnetic field $\vec{B_0}$. There are two modes of interest, the \ac{omode}, where the wave electric field is parallel to $\vec{B_0}$, $\vec{E_1} \parallel \vec{B_0}$; and the \ac{xmode}, where $\vec{E_1} \perp \vec{B_0}$. In \ac{omode}, there is no interaction with the magnetic field, and the refractive index depends solely on the plasma density. In \ac{xmode}, the magnetic field affects the wave, and the refractive index depends on both plasma density and magnetic field. Their respective refractive indexes can be calculated and are defined as
\begin{align}
    N_O^2(\omega) = & 1 - \frac{\omega_{pe}^2}{\omega^2} \text{ ,}
    \label{eq:refract_index_O}                                                                                                                \\
    N_X^2(\omega) = & 1 - \frac{\omega_{pe}^2}{\omega^2}\frac{\omega^2 - \omega_{pe}^2}{\omega^2 - (\omega_{pe}^2 + \omega_{ce}^2)} \text{ .}
    \label{eq:refract_index_X}
\end{align}

From these equations, it is possible to derive some important quantities:
%thresholds:
for \ac{omode}, there is a single cut-off ($N_O^2 = 0$), the \ac{wo}; while for \ac{xmode}, there are two cut-offs ($N_X^2 = 0$), \new{the \ac{wxl}, also called the lower cut-off, and the \ac{wxr}, also called the upper cut-off}, and a resonance ($N_X^2 \rightarrow \pm \infty$), the \ac{wuh}. These are defined as
\begin{align}
    \omega_{O}    & = \omega_{pe} \label{eq:cutoff_o} \text{ ,}                                                              \\
    \omega_{X,L}  & = \sqrt{\omega^2_{pe} + \frac{\omega^2_{ce}}{4}} - \frac{\omega_{ce}}{2}   \label{eq:cutoff_l} \text{ ,} \\
    \omega_{X,R}  & = \sqrt{\omega^2_{pe} + \frac{\omega^2_{ce}}{4}} + \frac{\omega_{ce}}{2}   \label{eq:cutoff_r} \text{ ,} \\
    \omega_{X,UH} & = \sqrt{\omega^2_{pe} + \omega^2_{ce}} \label{eq:cutoff_uh} \text{ .}
\end{align}
There are also resonances at the \ac{wce} harmonics for \ac{xmode}. \Cref{fig:dispersion} shows the dispersion diagram for both O and X modes and the relevant frequencies.
It is important to note that while the \ac{omode} has only two regions, one with $N^2_O < 0$ and the other with $N^2_O > 0$, the \ac{xmode} has five distinct regions: two with $N^2_X < 0$, for left and right cut-offs; two with $0 < N^2_X < 1$, also for left and right cut-offs; and finally a region with $N^2_X > 1$. It is also clear that $N_X^2$ is discontinuous at \ac{wuh}. The resonance at $2\omega_{ce}$ for the \ac{xmode} is also relevant, as it is a common upper limit for reflectometers. \Cref{fig:dispersion} shows the dispersion diagram for both O and X-modes, with \ac{omode} in orange and \ac{xmode} in red, and the critical frequencies marked with black and blue vertical lines.

\begin{figure}
    \centering
    \includegraphics[width=\linewidth]{images/reflectometry/x_mode_frequencies}
    \caption[\Acs{xmode} refractive index matrix]{On the left, the profiles of density and magnetic field for a representative JET experiment are shown. On the right, the refractive index matrix X-mode is presented - \new{the region in gray/red is transparent ($N^2 > 0$) while the region in blue is opaque ($N^2 < 0$)}. The frequency bands of the reflectometer at JET are also present on the side of the image. In green are shown the reflection positions for waves sent from the low field side.}
    \label{fig:x_mode_frequencies}
\end{figure}

A refractive index matrix for \ac{xmode} is shown on the right side of figure \ref{fig:x_mode_frequencies}, where $N_X^2$ is calculated for all positions and probing frequencies, with the density and magnetic field profiles presented on the left side. The areas in blue are opaque to waves of that frequency in that region of the plasma, and in grey and red, the plasma is transparent. Cut-offs and resonances are also shown, as well as the available frequency bands for the reflectometer at JET (see \cref{sec:jet_reflectometer}).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Probing the plasma}

The basic principle of reflectometry is straightforward: a microwave pulse is sent into the plasma, and the time it takes to return after reflecting on the plasma is measured.
As seen in \cref{eq:cutoff_o,eq:cutoff_l,eq:cutoff_r,eq:cutoff_uh}, the reflection point depends only on the magnetic field (for X-mode) and the plasma density. The frequency of the probing wave is chosen and the magnetic field profile can be measured through magnetic diagnostics \cite{lao_separation_1985,peruzzo2009installation,artaserse2019refurbishment}, allowing for the calculation of the density where the reflection takes place. \new{The time delay of the wave is simply its propagation time to the reflection position and back, from which a pair of position and density can be obtained.}
By sending multiple pulses with different frequencies, it is possible to create a density profile. \Cref{fig:x_mode_frequencies} also shows the reflection positions for all frequencies in green, for pulses injected from the low field side (right side of the picture).

\new{In a reflectometry system, a pulse of increasing frequency can be sent to the plasma, a technique called \ac{fmcw}}. Its delay can be calculated through the phase difference between the sent and received waves. This phase shift can be approximated to
\begin{equation}
    \phi(\omega) = 2 \frac{\omega}{c} \int_{r_{0}}^{r_{r}(\omega)} N(r, \omega)\ \mathrm{d} r - \frac{\pi}{2} \text{ .}
    \label{eq:phase_shift}
\end{equation}
where $r_0$ is the antenna position and $r_r$ is the reflection position. To be able to measure this phase shift, the returning wave is mixed with the outgoing wave, creating a beat signal. The time delay, the \ac{taug}, is thus defined as
\begin{align}
    \tau_g(\omega) & = \frac{\mathrm{d} \phi(\omega)}{\mathrm{d} \omega} = \frac{2}{c} \int_{r_{0}}^{r_{r}(\omega)} N(r, \omega)\ \mathrm{d} r \text{ ,} \label{eq:taug_N}                                                       \\
    % \tau_g(f) = \frac{1}{2 \pi} \frac{\mathrm{d} \phi}{\mathrm{d} f}
                   & = \frac{1}{2 \pi} \frac{\mathrm{d} \phi(\omega)}{\mathrm{d} t} \left(\frac{\mathrm{d} f}{\mathrm{d} t}\right)^{-1} = f_b \left(\frac{\mathrm{d} f}{\mathrm{d} t}\right)^{-1} \text{ ,} \label{eq:taug_beat}
\end{align}
where $f_b$ is the measured \acl{fb} and $\mathrm{d} f/\mathrm{d} t$ is the frequency sweep rate \textcolor{blue}.

The raw signal measured by the reflectometer is the beat signal. To reconstruct the profile, it is necessary to extract \ac{taug} from this signal. This can be done by computing the  spectrogram of the signal, a 2D image representing the spectrum of beat frequencies for each frequency of the probing wave. The \acl{taug} can be recovered by calculating the strongest signal for each probing wave frequency.

The profile can then be obtained by inverting and solving \cref{eq:taug_N}. For the \ac{omode}, this inversion can be performed analytically using an Abel inversion \cite{budden1988propagation}
\begin{equation}
    r(\omega_r=\omega_{pe}) = r_{0} + \frac{c}{\pi} \int_{0}^{\omega_{pe}} \frac{\tau_g(\omega)}{\sqrt{\omega_{pe}^2 - \omega^2}} \mathrm{d} \omega \text{ ,}
\end{equation}
and is calculated for each plasma frequency, creating a frequency profile. The density profile is then obtained through \cref{eq:wpe}.

It is impossible to solve \cref{eq:phase_shift} analytically for the \ac{xmode}, as the refractive index, and thus the group delay, depends on the position through the magnetic field profile, requiring a numerical method \cite{mazzucato_microwave_1998,manso_reflectometry_1993,morales_density_2018}. By iterating over the measured phase shifts, it is possible to reconstruct the profile step by step. \textcolor{blue}{Está bastante sintetico ...} .
% \question{should this be expanded with the detailed description of the numerical method?}

\Ac{omode} has the clear advantage of easier profile reconstruction. \new{However, due to the difficulty of building systems capable of sweeping a large range of frequencies, it is not feasible to sweep frequencies starting from 0 to the tens of GHz. Reconstructing \ac{omode} thus requires a known initial profile for the frequencies not measured, either from other measurements (including \ac{xmode}) or from a theoretical shape.}

% \note{add figure with profiles with same measurements and different $f_0$}

\Ac{xmode} can measure densities close to 0 with higher frequencies, by using the \ac{wxr}. \new{As a vacuum (i.e. 0 density) does not reflect the wave, the first reflection will be at a non-zero density. The measurement of the frequency of this first reflection, called first fringe, is crucial for the profile reconstruction. Experimental measurements of the first fringe have a relatively large error, that can result in a noticeable position shift \cite{aguiam_implementation_2018}. The methods to find and improve the calculation of the first fringe have been a topic of much discussion, and include statistical methods to reduce the individual error; assuming larger initial densities to compensate for a higher calculated first fringe than the real; and even methods using machine learning/artificial intelligence to better detect $f_0$ \cite{aguiam_implementation_2018,seliunin_validation_2021}. }

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{images/reflectometry/reconstruction_steps}
    \caption[Reflectometry Reconstruction Steps with Simulated Data]{Steps to reconstruct a reflectometer profile, using simulated data with added noise. \textcolor{blue}{Adicionar uma descrição mais detalhada do que é mostrado nesta figura e principalmente na proxima. }}
    \label{fig:reconstruction_steps}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{images/reflectometry/reconstruction_steps_real}
    \caption[Reflectometry Reconstruction Steps with Real Data]{Steps to reconstruct a reflectometer profile, using real \ac{jet} data.}
    \label{fig:reconstruction_steps_real}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{images/reflectometry/f0_spread}
    \caption[First Fringe Spread]{Spread of the calculated first fringe of a \ac{jet} experiment. The first fringe of the data used in \cref{fig:reconstruction_steps_real} is marked as a darker square. Note the higher temporal resolution around the 47.2s mark.}
    \label{fig:f0_spread}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{images/reflectometry/prof_reconstructed}
    \caption[Reflectometry Reconstructed Profile]{Frequency and density profile reconstructed from the steps in \cref{fig:reconstruction_steps_real}.}
    \label{fig:prof_reconstructed}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Reflectometry at JET}
\label{sec:jet_reflectometer}

To understand better the methods explained in the previous section, it is useful to look at simulated and real examples (from \ac{jet}). \Cref{fig:reconstruction_steps,fig:reconstruction_steps_real} show the reconstruction steps for both simulated and real data respectively. \new{The simulated data was created using a simple model that calculates the beat frequency from the magnetic field and density profiles (seen in \cref{fig:x_mode_frequencies}) using \cref{eq:phase_shift,eq:taug_N,eq:taug_beat}, and adds some amplitude noise.} The real data comes from a \ac{jet} experiment and the reconstructed frequency and density profile is shown in \cref{fig:prof_reconstructed}. While the simulation allows the use of all wave frequencies, up from 0 GHz, a real reflectometer is constrained by the hardware and can only measure in a certain range of frequencies. At \ac{jet}, there are four \ac{xmode} frequency bands (see more in \cref{sec:jet_reflectometer}), and they are represented in differently coloured backgrounds in both figures, and the measured data from each is on their own column in \cref{fig:reconstruction_steps_real}. The calculated first fringe is also represented as a blue dotted line. \new{The beat frequency signal is shown in a).} This signal is a wave described by a phase ($\phi$) and amplitude, which are presented in b). The spectrogram to obtain \ac{taug} is shown in c), normalised to make the dominant frequencies more clear, and the calculated maximums are the dashed black line.

It is clear that in \cref{fig:reconstruction_steps} the first fringe can be calculated with a small error, as the signal is strong and there is an obvious change in \ac{taug}. However, in \cref{fig:reconstruction_steps_real}, recovering $f_0$ becomes a much harder task.
\textcolor{blue}{Seria util explicar como se calcula f0 e indicar o seu impacto, por exemplo de quanto é o shift radial para uma variação de por exemplo 1 GHz. }
In this case, the first fringe is in the overlap region of both Q and V bands, however, none of them can easily detect it: in the V band there is no clear change in behaviour, and in the Q band the signal-to-noise ratio is low. These factors lead to the uncertainty of $f_0$ mentioned before, and \Cref{fig:f0_spread} shows an example from \ac{jet} where the calculated first fringe ranges between 51 and 54 GHz, despite the plasma conditions remaining similar throughout.

Joining data from the different bands is also a complex issue to solve. There is some overlap on the bands, and while it allows for more flexibility to improve the quality of the reconstruction, deciding where to cut and glue the data becomes harder. Not only is care required when matching the frequencies of the join, but the \ac{taug} itself is not trivial, as each band can have slightly different paths in the reflectometer hardware and on the vacuum chamber, resulting in different delays even for the same density. These then result in "broken" profiles (see the "pre-upgrade" line in \cref{fig:profile_upgrade_comp} for an example), and the error may be large enough that the entire profile must be discarded.
\textcolor{blue}{Demasiado vago, seria util explicar melhor os erros e apresentar exemplos.  }

\begin{table}
    \centering
    \begin{tabularx}{.5\linewidth}{cArC{5pt}l}
        \toprule
        Band & Polarisation & \multicolumn{3}{l}{Frequencies (GHz)}           \\
        \midrule
        Q    & X-mode       & 33                                    & - & 51  \\
        V    & X-mode       & 49                                    & - & 76  \\
        W    & X-mode       & 74                                    & - & 111 \\
        D    & X-mode       & 109                                   & - & 150 \\
        \midrule
        V    & O-mode       & 49                                    & - & 76  \\
        W    & O-mode       & 74                                    & - & 111 \\
        \bottomrule
    \end{tabularx}
    \caption[Bands in the \acs{jet} reflectometer]{Available bands in the \ac{jet} reflectometer.}
    \label{tbl:kg10_bands}
\end{table}

\textcolor{blue}{Estranho teres uma secção sobre o reflectometro do JET agora, tens estado constantemente a falar do sistema do JET..  }

The current profile reflectometer at \ac{jet} was installed during \ac{jet}'s renovation in 2010. It has four \ac{xmode} bands and two \ac{omode} bands, as shown in \cref{tbl:kg10_bands}. The Q-band lower limit was set due to loss of signal in the waveguides and the D-band upper limit was set from simulations that calculated it was enough to cover all \ac{jet} discharges \cite{sirinelli_multiband_2010}.

\begin{figure}
    \centering
    \includegraphics[width=\linewidth]{images/reflectometry/kg10_range}
    \caption[Densities Accessible by the JET reflectometer in X-mode]{Edge densities accessible by the KG10 reflectometer in X-mode for different magnetic fields. Each band range is represented by a different colour and pattern. The $2 f_{ce}$ resonance is also shown.}
    \label{fig:kg10_range}
\end{figure}

By finding the points where $N_X^2 = 0$ using \cref{eq:refract_index_X} and solving for the density, it is possible to obtain
\begin{equation}
    n_e = \frac{\epsilon_0 m_e}{e^2} (2 \pi f_{wave}) \left(2 \pi f_{wave} \pm \omega_{ce}\right)
\end{equation}
where the solution with $+$ corresponds to the left cut-off and the solution with $-$ to the right cutoff. Using this condition and the resonance at $2\omega_{ce}$ and replacing $f_{wave}$ with the limits of the \ac{kg10} bands, a diagram showing which densities are accessible to the reflectometer can be created. \Cref{fig:kg10_range} is an example of such a diagram for the X-mode, right cut-off of the \ac{kg10} reflectometer. It is possible to observe that below 1.9T, the reflectometer can not measure densities close to zero. The maximum measurable densities for each magnetic field and band are also easy to obtain. The Q-band at \ac{jet} has had many hardware issues and is not reliable, and the D-band has a much higher noise-to-signal ratio, which means that in most experiments the actual density range of the \ac{kg10} will be lower than the one presented here. It also means that experiments between 1.9 T and 2.3 T, while theoretically possible to measure the entire density profile with the reflectometer, will usually not be able to reconstruct the entire profile.

To be able to study fast events, like \acp{elm}, the sweep  rate of the JET reflectometry system can be set to a repetition rate of up to 67kHz but as it can only store 100'000 profiles per shot, very high repetition rates are usually only set for shorter time intervals to study a particular phenomenon. The most common setting is to have a base 1kHz repetition rate with up to four time intervals (adding up to 1s in total) of a higher repetition rate (between 30kHz and 67kHz). This allows to have profiles for the entire shot and also a window into the fast events in those intervals of interest.

All profiles used for this work were reconstructed using only the \ac{xmode}, as the \ac{omode} data had a much higher noise level and for most shots, was not necessary to have a complete picture.

At the time this work started, the reflectometer profiles at JET were manually calibrated to the interferometer data by its \ac{ro}, by individually looking at each shot and correcting it.
\textcolor{blue}{Explicar a razão para serem precisos estes ajustes.  }
This calibration worked by adjusting the magnetic field used in the reconstruction in such a way that the reconstructed profile agreed with the core density measured by the interferometer. \new{During the duration of this work, several fixes and improvements were done on the reconstruction code by the \ac{ro} \cite{morales_improved_2024,morales2022proceedings}. }

\begin{figure}
    \centering
    \includegraphics[width=\linewidth]{images/reflectometry/profile_upgrade_comp}
    \caption[Comparison of profiles pre- and post-upgrade]{Comparison of density profiles pre- and post-upgrade of the analysis software. Notice that the jump in the radial position around ?? $\times 10^{19}\ $m$^{-3}$ disappears and that the profile becomes steeper.}
    \label{fig:profile_upgrade_comp}
\end{figure}

It is relevant to point out some of the improvements made, as they are present in some of the shots used. Data from different bands could be misaligned, and in some profiles, could represent a profile shift of several centimetres for only part of the profile, \new{resulting in two separate sections of the profile}. This was solved by simulating the path of the reflectometer wave and noticing that different bands (as they used different waveguides) would reflect at different positions. Since the reconstruction relies on a calibration using no plasma and measuring the reflection at the wall, this distance discrepancy between bands meant that the measured radial position for each band would be also different, creating a radial position jump. An example of this phenomenon and its fix is shown in \cref{fig:profile_upgrade_comp}, where it is possible to see that the pre-upgrade profile (in blue) has a radial position jump around 2 $\times 10^{19}\ $m$^{-3}$ that is corrected in the post-upgrade profile (in orange). It is also noticeable other miscellaneous changes and fixes in the code, that result in steeper profiles. As the pre-upgrade profile is calibrated to have the same density in the core as the interferometer, the steeper profile also results in a higher pedestal.

The new density profiles are now more accurate and robust, which meant that manual validation was no longer necessary. It was also decided to automatically reprocess all shots up from a certain point, C38 campaign, meaning shots after 2019 and with numbers over 92505.

Fusion plasmas are known to be turbulent, being characterized by fast fluctuations, below the millisecond time scale, and with a relatively small amplitude, in the order of 10\% of the local density,  that can have a measurable impact on the reconstructed profile \cite{nikolaeva_characterization_2018}, changing its shape or in the most extreme cases, creating large enough perturbations such that the profile become invalid. A technique to reduce the effect of the plasma fluctuations on the profile reconstructions  and to improve the first fringe detection has been developed. It averages the raw data of several probing frequency sweeps (usually between three and ten) and reconstructs the profile from the averaged raw data. This technique improves the profile quality, as the data scatter is attenuated and the first fringe is better estimated. However, profiles created in this way were not used in this work, as it was a process still in the early stages of testing and validation.

As a large part of the shots used in this work are before the new fixes, there are two main databases (see \cref{sec:databases} for more detail), one with all shots "pre-upgrade" and another with all shots "post-upgrade". As the changes modify the profiles significantly, it means that any comparison between shots pre- and post-upgrade must be very careful.
