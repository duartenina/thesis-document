\chapter{The Joint European Torus (JET)} \label{sec:jet}

\begin{figure}
    \centering
    \includegraphics[width=.7\linewidth]{images/jet/JET_interior}
    \caption[JET Interior]{Inside the Joint European Torus tokamak (JET) at Culham, UK, with a superimposed image of the hot plasma (credit \cite{eurofusion_jet_interior}).}
    \label{fig:JET_interior}
\end{figure}

The \acf{jet} was the largest operating tokamak until December 2023, when it finished operations \cite{rimini_jet_2024}. \Ac{jet} was located in the United Kingdom and began operation in 1983, with the main scientific focus aiming at the investigation of plasma confinement and heating in a reactor relevant plasma conditions and the investigation of plasma-wall interactions. JET holds the record for achieving the highest fusion energy of 69.26 MJ of heat released during a single pulse \cite{eurofusion_jet_record}. \Ac{jet} was upgraded to an ITER-like wall (beryllium-tungsten wall) in 2010/11, replacing its plasma-facing components consisting of carbon fibre composite.
\Ac{jet} was the only tokamak capable of tritium operations. \Ac{jet} performed recently two deuterium-tritium campaigns to demonstrate fusion power generation with the ITER-like wall. Preparations included operations with H, D, and T hydrogen isotopes and He. \Cref{fig:JET_interior} shows a view of the JET vacuum vessel with the beryllium-tungsten wall, and \cref{tbl:jet_properties} summarizes the values of \ac{jet} engineering parameters.

This chapter presents the more unique characteristics of \ac{jet} relevant for the experiments studied in this thesis, including how its magnetic field is shaped, how its divertor is configured, how its plasma is heated and the electron density diagnostics used in this work.

\begin{table}
    \centering
    \begin{tabular}{lrll}
        \toprule
        Parameter & \multicolumn{3}{c}{Value} \\
        \midrule
        Major radius            & $\approx$     & 3             & m \\
        Minor radius            & $\approx$     & 0.9           & m \\
        Magnetic field          & $\leq$        & 4             & T \\
        Plasma current          & $\leq$        & 5             & MA \\
        NBI power               & $\leq$        & 30            & MW \\
        NBI power               & $\leq$        & 8             & MW \\
        \bottomrule
    \end{tabular}
    \caption[JET Engineering Parameters]{Overview of the main JET engineering parameters.}
    \label{tbl:jet_properties}
\end{table}

\section{Plasma Shaping}

\begin{figure}
    \centering
    \includegraphics[width=.7\linewidth]{images/jet/elongation_triangularity}
    \caption[Elongation and Triangularity]{Illustration of perturbations to the magnetic field cross section, including elongation and triangularity \cite{fusionwiki_triangularity}.}
    \label{fig:elongation_triangularity}
\end{figure}

Early tokamaks were limited to plasmas with circular cross-sections, but modern machines typically employ non-circular shapes as shown in \cref{fig:elongation_triangularity} to enhance plasma confinement. The elongation ($\kappa_a$) and triangularity ($\delta$) are two key parameters used to describe the shape of the plasma in the poloidal plane. Research, both theoretical and experimental, indicates that stability is maximized when the cross-section combines elongation with outward-pointing triangularity. Elongation is defined as
\begin{equation}
    \kappa_a = \frac{Z_{max} - Z_{min}}{2 a} \text{ ,}
\end{equation}
where $Z_{max}$ and $Z_{min}$ are respectively the maximum and minimum value of $Z$ along the separatrix and \acs{a} is the \acl{a}. A perfect circle would have $\kappa_a = 1$ and at \ac{jet} elongation varies between 1.1 and 1.8. Triangularity changes the cross-section closer to a triangle and is defined for both the upper and lower sections of the cross-section as
\begin{equation}
    \delta_{upper,lower} = \frac{R - R_{upper,lower}}{a} \text{ ,}
\end{equation}
where \acs{R} is the \acl{R}, $R_{upper,lower}$ is the major radius of the highest/lowest vertical point of the separatrix and \acs{a} is the \acl{a}. Typical values for \ac{tril} are between 0.0 and 0.5, while \ac{triu} varies between 0.0 and 0.8.

\begin{figure}
    \centering
    \includegraphics[height=.4\textheight]{images/jet/mag_field}
    \caption[JET Magnetic Flux Lines]{Typical magnetic equilibrium at \acs{jet}. Notice that the separatrix, in red, and the open flux surfaces, in blue, leading to the divertor, in black.}
    \label{fig:mag_field_lines}
\end{figure}

\section{Divertor Configuration}

\begin{figure}
    \centering
    \includegraphics[width=.7\linewidth]{images/jet/divertor_configs}
    \caption[JET Divertor Tiles]{Schematic of divertor tiles at \acs{jet} with three examples of divertor  configurations: HT in blue, CC in purple and VT in red.}
    \label{fig:divertor_tiles}
\end{figure}

As seen in the previous chapter, tokamaks normally use a divertor configuration to better control the plasma-wall interactions and thus the overall confinement. The divertor configuration is produced by a set of poloidal field coils that introduce a point of zero poloidal magnetic field, the so-called X-point. The two strike-points are the locations at which the separatrix intersects a vessel component. In the divertor configuration, a portion of the plasma is therefore in direct contact with a specific region of the vessel, at locations with special target plates.

A typical magnetic equilibrium in divertor configuration can be found in \cref{fig:mag_field_lines}, with the divertor marked in black at the bottom of the vacuum vessel, and the flux surfaces coloured by type (closed in green, separatrix in red and open in blue). It is also possible to notice that the flux surfaces are elongated, and with a small positive triangularity.

The JET divertor includes vertical and horizontal target plates as shown black in  \cref{fig:mag_field_lines}.
These tiles are numbered according to their position and are shown in \cref{fig:divertor_tiles}. Tile 5 is called horizontal (H), tiles 4 and 6 are called corner (C) and tiles 3 and 7 vertical (V).
To evaluate the impact of the divertor configuration on plasma confinement, several experiments have been conducted at JET over the past decade with the strike-point at different positions (e.g. \cite{maggi_pedestal_2015,joffrin_impact_2014,joffrin_impact_2017,de_la_luna_recent_2016}).  Here we report on experiments with the outer divertor strike-point at different positions as illustrated in \Cref{fig:divertor_tiles}: tile 5 of the horizontal target (HT, blue); vertical target (VT, red); and in the corner (CC, purple) configuration (between the horizontal and vertical targets close to the cryopump opening).

% \Ac{jet} is not an exception and one example of its magnetic configuration can be found in \cref{fig:mag_field_lines}, with the divertor marked in black at the bottom of the vacuum vessel, and the magnetic field lines coloured by type (closed in green, separatrix in red and open in blue). It is also possible to notice that the magnetic field lines are elongated, and a small triangularity can be seen.

%The magnetic configuration at \ac{jet} is controllable, and it is possible to modify it such that the separatrix intersects at different sections of the divertor, called tiles. These tiles are numbered according to their position and are shown in \cref{fig:divertor_tiles}. As different machines may have different numbering schemes and to help understanding, tile 5 is also called horizontal (H), tiles 4 and 6 are called corner (C) and tiles 3 and 7 vertical (V). As the separatrix intersects the divertor in two points, the inner region, i.e. the one with lower major radius, where the separatrix intersects is called the inner divertor, and similarly for the outer divertor. To clearly and easily define which configuration is referred to, a shorthand using the initials is used. As an example, VH would mean a configuration with a vertical inner divertor (tile 3) and a horizontal outer configuration (tile 5), while CC would be corner inner divertor (tile 4) and outer divertor (tile 6). \Cref{fig:divertor_tiles} highlights the outer divertor tiles, with red for the horizontal tile (5), blue for the corner tile (6) and purple for the vertical tile (7), and the positions of their limits.

\section{Heating - NBI and ICRH}

As mentioned before, to reach the conditions for fusion reaction to occur, the plasma must be heated to very high temperatures. While the induced plasma current does produce Ohmic heating, it is insufficient to reach the desired temperatures.
 \ac{jet} is equipped with two different external heating systems: \ac{nbi} and \ac{icrh}.

\Ac{nbi} works by generating a high energy - much higher than the desired plasma temperature - beam of neutral particles outside the vacuum chamber, usually by accelerating ions in a strong electric field and neutralising them before injecting them into the plasma \cite{agency_fundamentals_2023}. These neutral particles collide with the plasma, ionising and becoming confined. The beam thus transfers its energy to the plasma.  It also has the beneficial effect of driving current if the beam is aimed tangentially at the plasma. At \ac{jet}, the \ac{nbi} system can reach a heating power of up to 30MW.

\Ac{icrh} is a radio frequency heating methods that take advantage of the plasma having absorption resonances at specific frequencies, namely the ion cyclotron frequencies
where the wave energy can be transferred to the plasma ions or to the electrons.
\Ac{icrh} is based on the resonance of ions to waves of frequencies close to their \ac{wci}, given by
\begin{equation}
    \omega_{ci}^2 = \left(2 \pi f_{ci}\right)^2 = \frac{q_i B_0}{m_i}
\end{equation}
where $q_i$ is the ion charge, $m_i$ its mass and $B_0$ the total magnetic field. By sending high-energy electromagnetic waves of the correct frequency, it is possible to heat the plasma. As the magnetic field decreases with major radius and depending on the path of the waves, it is also possible to heat particles in a particular section of the plasma, also helping drive a current. The \ac{icrh} system at \ac{jet} can provide up to 8MW.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Diagnostics}

\begin{figure}
    \centering
    \includegraphics[width=.7\textwidth]{images/reflectometry/diagnostics_LOS_labels}
    \caption[JET Diagnostics Lines of Sight]{Lines of sights for the \ac{jet} interferometer (vertical blue lines) and reflectometer (horizontal red line) and the path of the \ac{hrts} laser (horizontal green line).}
    \label{fig:diagnostics_LOS}
\end{figure}


Fusion plasmas are analyzed using various diagnostics to measure key parameters such as magnetic equilibrium, plasma density, and temperature. These diagnostics are selected to minimize plasma perturbations, often employing low-power electromagnetic waves to probe the inner regions of the plasma.
As this work focuses on the edge density profile, this section provides an overview of the diagnostics used to measure the electron densities at \ac{jet}. The lines of sight for these diagnostics are shown in \cref{fig:diagnostics_LOS}, displaying a poloidal cross-section of JET together with a typical magnetic equilibrium. The interferometer's vertical lines of sight are marked in blue, the reflectometer's line of sight is in red, and the laser path of the \ac{hrts} is depicted in green.

%There are many diagnostics used in fusion plasmas to measure all the essential parameters, including magnetic field and equilibrium, plasma density and temperature, particle fluxes, transport, power balance, and many more. As fusion plasmas can be sensitive to external effects, diagnostics are chosen to try and minimise perturbations, so many measure the effects of the plasma on the surroundings and many others use low power electromagnetic waves to probe into the inner regions of the plasma. As the focus of this work is the edge density profile, this section will present an overview of the diagnostics used to measure densities at \ac{jet}. Lines of sights of the different density diagnostics are presented in \cref{fig:diagnostics_LOS}. The vertical lines of sight of the interferometer are marked in blue, red is the line of sight of the reflectometer, and the laser path of the \ac{hrts} is shown in green.

The most used diagnostic to measure plasma density at JET is the interferometer.
The interferometry makes use of the different optical properties in the plasma compared to a reference medium. In particular, the light of a probe laser beam experiences a different phase shift in the plasma than in air. This phase shift is proportional to the line integrated electron density of the plasma. By measuring the phase shift with an interferometer the line-integrated density along the laser beam can be derived.
The refractive index of a plasma depends on its electron density. Interferometry is a well-established technique to measure the plasma average density from the phase shift of the waves transmitted through the plasma. The variation of the phase with respect to the reference signal is used to determine the temporal evolution of the line-average density.
Interferometers are fast, accurate and robust diagnostics, with the downside that it gives little information about the density profile.

The JET far infrared (FIR) diagnostic uses a 118.8 $\mu m$ $CO_2$ laser \cite{boboc_upgrade_2012}. The diagnostic provides line-averaged electron densities along the lines of sight presented in \cref{fig:diagnostics_LOS}, four vertical and four horizontal, returning both core and edge densities, and a temporal resolution of \new{up to 1 ms} \cite{braithwaite_jet_1989,boboc_upgrade_2012}.

%The most used diagnostic to measure plasma density at JET is the interferometer, which works by passing a laser through the plasma and measuring its phase shift. As seen with reflectometry in \cref{eq:phase_shift}, this phase shift depends on the integration of the refractive index and thus the plasma density. While reflectometry focuses on the distance to a particular density by using lower frequency waves that are reflected by fusion plasmas, interferometry measures the density along the laser path in its line of sight, by using laser frequencies high enough that the plasma becomes transparent \cite{iter_physics_basis_editors_chapter_1999-7,boboc_recent_2010}. The result is a line integrated density, measured in $m^{-2}$, that makes the interferometer a very good baseline diagnostic as it is fast, accurate and robust, with the downside that it gives little information about the density profile. The \ac{jet} interferometer has eight lines of sights, four vertical and four horizontal, returning both core and edge densities, and a temporal resolution of 10 $\mu s$ \cite{braithwaite_jet_1989,boboc_upgrade_2012}.

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{images/jet/profiles_ne_Te_pe}
    \caption[Temperature, Density and Pressure Profiles]{Example of \textit{ELM-synchronised} edge profiles of electron temperature, density and pressure for JET shot \#84542 \cite{frassinetti_eurofusion_nodate}. The lines show the fits of two different models, \textit{mtanh} and \textit{linear} (called \textit{twoline} in this work, see \cref{sec:models}).}
    \label{fig:profiles_ne_Te_pe}
\end{figure}

The other common density diagnostic at JET is the \acf{hrts}, which is based on the Thomson scattering of electromagnetic waves by free electrons in the plasma. The scattered light holds information about the temperature and density of the electrons. The intensity of the scattered light depends on the laser energy and the plasma density. The spectral distribution of the detected light depends on the electron temperature. \cite{sheffield_plasma_2011,wesson_tokamaks_2004,iter_physics_basis_editors_chapter_1999-7}.
The \ac{hrts} system at \ac{jet} can produce profiles with up to 63 points in space, at a temporal resolution of 50ms (20 Hz repetition rate) \cite{pasqualotto_high_2004,stefanikova_pedestal_2020}. \Acs{hrts} provides simultaneous profiles for both density and temperature across the entire plasma radial range. Its main weaknesses are limited spatial and temporal resolution, and unreliable data in the SOL. The low temporal resolution also mean that to study fast events such as ELMs, techniques that group the profiles are required, called \textit{ELM-synchronised}, where profiles in similar time windows of the \ac{elm} cycle are grouped and analysed as a single profile. \Cref{fig:profiles_ne_Te_pe} shows examples of \textit{ELM-synchronised} edge profiles of electron temperature, density and pressure obtained with \ac{hrts}, together with the respective fits with two different models, \textit{mtanh} and \textit{linear} (called \textit{twoline} in this work, see \cref{sec:models}).


% When a wave collides with a charged particle, it is elastically scattered, and the resulting scattered radiation may be analysed to obtain the electron density and temperature. The radiation intensity is directly proportional to the electron density, and the width of the frequency spectrum comes from Doppler shifts and is dependent on the electron temperature \cite{sheffield_plasma_2011,wesson_tokamaks_2004,iter_physics_basis_editors_chapter_1999-7}. The laser is inserted horizontally along the midplane, and the scattered radiation is collected by a window in the upper part of the tokamak. The position of origin for the scattering radiation is defined by the intersection between the laser path and the line of sight of the detector. To generate density and temperature profiles, several optical fibres were placed at different angles on the vertical port.

\begin{figure}
    \centering
    \includegraphics[width=\linewidth]{images/reflectometry/diagnostic_comparison}
    \caption[Comparison of diagnostic resolutions]{Comparison of spatial and temporal resolutions for the interferometer, the \acs{hrts} and the reflectometer. Note that in this window, the reflectometer is using the low repetition rate (1 kHz), to make it possible to see any individual profile.}
    \label{fig:diagnostic_comparison}
\end{figure}

Reflectometry offers complementary insights into electron density profiles, providing high spatial resolution even in the \acs{sol}, which enables detailed studies of the plasma edge. Its configurable time resolution allows for tailored measurements, and at JET, it can achieve a repetition rate of up to 67 kHz within selected time windows of interest, making it ideal for examining the time evolution of \acp{elm}.
However, reflectometry has limitations, which will be addressed in subsequent chapters, along with strategies for mitigation. For example, while the  dependence of the \ac{xmode} on the magnetic field enables the reflectometer to probe deeper than would otherwise be possible, it also restricts the availability of a complete profile to specific ranges of magnetic field and density.
For instance, the pedestal top region is not assessable at lower magnetic fields (i.e. $B_T < 2.1$ T) or high pedestal density (e.g. $n_e > 10 \times 10^{19}$ m$^{-3}$) (see \cref{fig:kg10_range}). The challenge in determining the profile initialization (identifying the first fringe) primarily results in a profile shift, leading to significant errors in the absolute position. Additionally, noise, rapid plasma perturbations, and reconstruction issues can severely impact the profile, sometimes producing non-physical results (see \cref{sec:profile_selection}). In certain cases, these issues affect more than 50\% of the profiles.

In \cref{fig:diagnostic_comparison}, a comparison is shown of the spatial and temporal resolutions of the interferometer, the \ac{hrts} and the reflectometer. On the left, the density profiles are shown, and it is clear that the reflectometer offers a significantly spatial resolution, including at the \ac{sol}. On the right, the beryllium emission measured in the outer divertor is presented, a common proxy for the ELM behavior, together with the sampled times for the different diagnostics. The differences in temporal resolution are noticeable. It is important to note that in this window, the reflectometer was set to its low repetition rate of 1kHz, otherwise the individual profiles would be impossible to discern.


%Reflectometry then is a very good addition, as it has high spatial resolution, including in the \acs{sol}, allowing the study of the edge of the plasma in more detail. Its time resolution is configurable, and at JET, it is possible to set time windows of particular interest where it has a repetition rate of up to 67kHz, making it possible to examine the time evolution of \acp{elm}. However, reflectometry does have some downsides that will be explored further in the future chapters, with discussions on how to mitigate them. For instance the dependence on the magnetic field of \ac{xmode}, that means that the reflectometer can probe deeper than otherwise would be possible, also means that a complete profile is only available in restricted ranges of magnetic field and density. Density profiles in lower magnetic fields (i.e. $B_T < 2.1$ T) or high pedestal density (e.g. $n_e > 10 \times 10^{19}$ m$^{-3}$) will not be able to resolved up to the top of the pedestal (see \cref{fig:kg10_range}). The difficulty in measuring the first fringe and its induced absolute position shift means the absolute position can have errors too large to be analysed. Finally, noise, fast plasma perturbations and reconstruction issues can have a large effect on the profile, creating non-physical profiles (see \cref{sec:profile_selection}), that in some cases can reach more than 50\% of all profiles.

%In \cref{fig:diagnostic_comparison}, a comparison is shown of the spatial and temporal resolutions of the interferometer, the \ac{hrts} and the reflectometer. On the left, the density profiles are shown, and it is quite clear that the reflectometer can measure a more complete picture of the profile edge, including the \ac{sol}. On the right, the beryllium flux measured in the outer divertor is presented, a common indicator for \ac{elm} presence, and indicators of times for diagnostic measurements. It is also very noticeable the difference in the number of profiles for both diagnostics. It is important to note that in this window, the reflectometer was set to its low repetition rate of 1kHz, otherwise the individual profiles would be impossible to discern.
