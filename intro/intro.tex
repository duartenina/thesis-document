\chapter{Introduction} \label{sec:intro}

\section{Society requires ever more energy}

\begin{figure}[h!]
    \centering
    \includegraphics[width=\linewidth]{images/intro/global-energy-substitution}
    \caption[Global energy consumption]{Global energy consumption \cite{ourworldindata_energy}.}
    \label{fig:global_energy_consumption_history}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\linewidth]{images/intro/GlobalTemp}
    \caption[Global surface temperatures]{Global surface temperatures relative to 1951-1980 average temperatures \cite{nasa_temperature}.}
    \label{fig:land_temperature_history}
\end{figure}

As populations grew and technology improved, the energy needs of our world have dramatically increased (see \cref{fig:global_energy_consumption_history}) and are predicted to expand even further. As the Industrial Revolution progressed, steam machines, using coal, started powering every new invention and tool and were soon generating electricity. With new technologies and advances in materials, new energy sources appeared and flourished, such as petroleum oil, natural gas or nuclear fission.

However, the monumental increases in energy production and consumption also created massive gas emissions, having a large negative impact on our ecosystem. The most dangerous was, and is, the climate change created by the greenhouse gases, with effects ranging from an increase in temperature worldwide (see \cref{fig:land_temperature_history}) to more frequent and more powerful storms and weather events.

These issues, and the rising costs of the more common fuels, lead to the development and rise of new energy sources. These new technologies provided cleaner energy and a good share of global energy production is based on energy sources such as nuclear fission, hydroelectric, solar and wind. But unfortunately, they are not without drawbacks.

In the case of nuclear fission, accidents can be catastrophic, requiring complex and costly solutions and impacting very negatively on public opinion, while the spent nuclear fuel remains radioactive and dangerous for many generations. Renewable energy sources also have their issues, for instance, hydroelectric energy has very limited expansion, as most possible locations have already been built; or in general, the energy provided can be unreliable, due to natural fluctuations of their sources.

Nuclear fusion aims to add a new energy source that can replace the more polluting alternatives while supplementing the existing renewable power plants. By fusing hydrogen, and its isotopes deuterium and tritium into helium, it is possible to generate energy that can then be converted to electricity similarly to normal thermal power plants.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Nuclear Fusion}

There are three main advantages of nuclear fusion compared to other energy sources, particularly fossil fuels and nuclear fission.

The first large advantage is fuel. Relatively small amounts of fuel are required to generate energy compared to fossil fuels (around 6 or 7 orders of magnitude less) or even nuclear fission (almost an order of magnitude less). Hydrogen and deuterium are also quite plentiful, with deuterium being naturally available in the ocean; and while tritium has very few natural sources, there are methods to generate it using lithium in the power plant itself.

Nuclear fusion also has no dangerous or greenhouse emissions, as helium is an inert, harmless gas. The high radiation and heat produced by the reactions are stopped by the blanket material, that becomes radioactive. The radioactivity is contained to the machine, but requires automation and remote handling to perform maintenance, increasing the complexity of building a fusion machine.

The final major advantage is its safety, particularly in contrast with nuclear fission. In fission, relatively large quantities of fuel are in the reactor, creating the risk of a meltdown, as in Chernobyl. In nuclear fusion, however, the fuel is constantly fed into the reactor, with only the minimum amount required inside at any time, making it impossible to have a runaway accident.

These advantages mean that nuclear fusion is a very promising energy source. But, it also has its fair share of problems. Fusion requires extreme conditions - temperatures of hundreds of million degrees Celsius, hotter than the centre of the sun - to allow the atomic nuclei to overcome their Coulomb force repulsion and fuse.

To create, confine and control these extreme conditions are the main challenges that need to be solved to reach the goal of a commercial nuclear fusion power plant.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Fusion in a nutshell}

Nuclear fusion is based on the merging of light elements into heavier ones, using reactions that generate energy. While there are many possible reactions with different advantages and disadvantages, most fusion machines focus on the fusing of hydrogen isotopes, deuterium and tritium.

\begin{align}
    \text{D}^2 + \text{D}^2 \rightarrow  & \ \text{He}^3 + \text{n} + 3.3 \text{ MeV} \text{,}
    % \label{eq:dd_he_reaction}
    \\
    % \text{D}^2 + \text{D}^2 \rightarrow &\ \text{T}^3 + \text{p} + 4.1 \text{ MeV}
    % \label{eq:dd_t_reaction}
    % \\
    \text{D}^2 + \text{He}^3 \rightarrow & \ \text{He}^4 + \text{p} + 18.3 \text{ MeV} \text{,}
    % \label{eq:dhe_reaction}
    \\
    \text{D}^2 + \text{T}^3 \rightarrow  & \ \text{He}^4 + \text{n} + 17.6 \text{ MeV} \text{.}
    % \label{eq:dt_reaction}
\end{align}

Reactions such as deuterium-deuterium (D-D) and deuterium-helium (D-He$^3$) are studied and can theoretically be used, but the easiest to initiate is the deuterium-tritium (D-T) reaction (see \cref{fig:fusion_reactions_crosssections}), and thus is the common choice for commercial production \cite{wesson_tokamaks_2004}. While the optimal energy for any particular D-T collision is over 100 keV, the entire plasma does not need to be at that temperature, as there is a wide distribution of energies, and the high energy tail is large enough to sustain fusion. The optimal temperature for the plasma is then a more manageable, but still extremely high, temperature of around 15 keV or 175 million degrees Celsius.

\begin{figure}
    \centering
    \includegraphics[width=.6\textwidth]{images/intro/fusion_reactions_crosssections}%
    \caption[Fusion reactions cross-sections]{Cross-sections of the different fusion reactions \cite{wesson_tokamaks_2004}.}
    \label{fig:fusion_reactions_crosssections}
\end{figure}%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Since no material can withstand these temperatures, an alternative method is necessary to contain the plasma. Stars use their massive gravity to force the atoms to collide, which is impossible to use at a human scale. Another alternative is inertial confinement, where fuel pellets are fired upon by numerous laser beams, causing compression and triggering fusion.
Another widely used method is magnetic confinement, which employs magnetic fields to control the motion of charged particles and keep the plasma contained.


Before discussing one particular magnetic confinement concept, the tokamak, and the fusion reactor used in this work, the \ac{jet}, it is useful to introduce some physical concepts essential to understanding and evaluating fusion plasmas. The crucial quantities are the energy confinement time (\acs{taue}) and the densities (\acs{nm}), temperatures (\acs{tm}) and pressures ($p_m = n_m T_m$) for each species $m$. While all three vary in space and time and differently for each species, for the next analysis only their average value is necessary.

The energy confinement time is defined by the power balance of the plasma. There is an increase of energy through the fusion reactions (with $\alpha$ heating) and from external heating, while there are losses through radiation and transport. The energy confinement time is thus derived as

\begin{equation}
    \tau_{\text{E}} = \frac{W}{P_{loss}} \text{,}
    \label{eq:taue}
\end{equation}
where $W$ is the total energy of the plasma and $P_{loss}$ is the energy loss rate.

The ideal scenario for a fusion reactor, called ignition, is one where no external heating is necessary, meaning all losses are balanced solely by $\alpha$ heating. Lawson criterion \cite{lawson_criteria_1957}, or triple product, is a quantity that must be reached to achieve ignition and is given by the product $n T \tau_{\text{E}} = p \tau_{\text{E}}$. The minimum values are \cite{freidberg_plasma_2007}

\begin{align}
    T_{min}                   & = 15 \text{ keV,}    \\
    (p \tau_{\text{E}})_{min} & = 8.3 \text{ atm s.}
\end{align}

It is necessary to have external heating to reach ignition and it is even desirable to keep some external heating on an ignited plasma for better control. To characterise the resulting power balance, it is useful to define \acs{qe}, the physics gain factor, defined by

\begin{align}
    Q & = \frac{\text{net power out}}{\text{heating power in}}  \text{,} \\
      & = \frac{P_{f}}{P_{h}} \text{.}
\end{align}
where $P_f$ is the total fusion power (both alphas and neutrons) and $P_h$ is the total external heating power. While \acs{qe} is useful from a physics point of view, to compare different machines for instance, it is not the complete picture of a fusion reactor, particularly for commercial purposes. The engineering gain factor, \acs{qee}, is used to give a more realistic figure of the power balance and is defined as
\begin{equation}
    Q_\text{E} = \frac{\text{net electrical power out}}{\text{total electrical power in}} \text{.}
\end{equation}

\acs{qee} will always be smaller than \acs{qe}, as the generated fusion power is not converted with 100\% efficiency. Note that close to ignition, both \acs{qe} and \acs{qee} will both tend to infinity. The final important concept is breakeven, where the power out is equal to the power in, i.e., fusion power breakeven, $Q = 1$, and electrical power breakeven, $Q_\text{E} = 0$. To give some idea of the relation between both gains, the electrical power breakeven requires $Q \approx 3$, and $Q = 10$ is equivalent to $Q_\text{E} \approx 2$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Tokamaks} \label{sec:tokamaks}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{images/intro/tokamak_comparison}
    \caption[Comparison of large tokamaks]{Comparison of current and future large tokamaks: \acs{jet}, \acs{iter} and \acs{demo} \cite{dubus_tokamak_comp}.}
    \label{fig:tokamak_comparison}
\end{figure}

A tokamak is a magnetic confinement device first developed in the Soviet Union during the 1950s and 1960s and is the most promising device for commercial scale nuclear fusion. Since the first tokamaks, there have been many different iterations, with different purposes, shapes and sizes.
Large machines are required for achieving the elevated temperatures and pressures necessary for self-sustaining fusion. Consequently, the study and development of increasingly larger tokamaks has been a continuous focus of research.
The Joint European Torus (JET) was the largest operating tokamak until December 2023, when it finished operations \cite{rimini_jet_2024}. JET holds the record for achieving the highest fusion energy of 69.26 MJ of heat released during a single pulse \cite{eurofusion_jet_record}.

From the research done at \ac{jet} and other tokamaks, a worldwide collaborative effort was made to design and create a fusion device capable of generating more energy than the energy provided (Q $\gg$ 1), named \ac{iter}. It is now in the final stages of construction in the south of France and will be able to reach a Q of around 10, proving that nuclear fusion is feasible. There are already plans for an even larger machine, \ac{demo}, that will serve as a prototype commercial power plant. A comparison of their sizes and important numbers is found in \cref{fig:tokamak_comparison}.

\begin{figure}
    \centering
    \includegraphics[height=.4\textheight]{images/intro/tokamak_schematic}
    \caption[Tokamak Schematic]{Schematic of a tokamak \cite{ipp_tokamak}.}
    \label{fig:tokamak_schematic}
\end{figure}

In a tokamak, the main magnetic field to confine the plasma is a toroidal field. Electrically charged particles tend to move along magnetic field lines, orbiting around them. However,
%there are other effects that change their paths and can break confinement.
curving the magnetic field to form a torus will introduce several drifts, originating from the field curvature and gradient and the interaction between the magnetic and electric fields. % and their variation in time.
This creates a need for another magnetic field: a poloidal field created by a large current inside the plasma itself. This current is induced by transformer effect, with the plasma acting as the
secondary winding of the transformer.
%with  coils located in the centre of the torus.
A schematic of a tokamak can be found in \cref{fig:tokamak_schematic}.
%The last closed magnetic flux surface is called the separatrix and studying its characteristics can be essential to understand the overall plasma dynamics.

\begin{figure}
    \centering
    \includegraphics[width=\linewidth]{images/intro/plasma_regions_lit}
    \caption[Plasma Regions and Separatrix]{a) Schematic of a tokamak's plasma regions and magnetic field lines, including the separatrix; b) Pressure profiles for H-Mode and L-Mode \cite{ham_filamentary_2020}.}
    \label{fig:plasma_regions}
\end{figure}


The combination of toroidal and poloidal magnetic fields will twist the magnetic lines around the plasma (see the arrows in \cref{fig:tokamak_schematic}). The degree to which they are twisted can have a large impact on the plasma, particularly its stability. To measure it, a \ac{q_safety} is defined approximately as
\begin{equation}
    q(r) = \frac{r B_T(r)}{R B_p} \text{ ,} \label{eq:safety_factor}
\end{equation}
where $B_T$ and $B_p$ are respectively the toroidal and poloidal magnetic fields, and $r$ and $R$ are respectively the minor and the \acl{R}.

The axisymmetric toroidal geometry of a tokamak offers a key advantage: it minimizes particle and energy losses that typically occur at the ends of linear devices. Magnetic field lines in the torus lie on nested surfaces of constant poloidal flux, known as flux surfaces, normalized in cross-section to the last closed flux surface, the separatrix. The separatrix defines the boundary between the closed magnetic field surfaces, confining particles, and the open field lines beyond it.

The plasma is divided into distinct regions (see \cref{fig:plasma_regions}a). Outside the separatrix lies the scrape-off layer (SOL), where particles from the core plasma are directed toward plasma-facing components. The SOL features open magnetic field lines ending on material surfaces. Inside the SOL is the edge region, where atomic processes like ionization and recombination significantly affect particle and energy balances. Further inward lies the core plasma, where most fusion reactions occur.

To control edge plasma profiles and plasma-wall interactions, optimizing the shape of the magnetic flux surfaces near the separatrix is essential. The magnetic divertor plays a critical role, using coils to direct magnetic field lines to designated wall areas or plates, managing plasma-wall interactions far from the core. The field lines bend to intersect target plates, forming a poloidal magnetic null, the X-point. The separatrix intersects the vessel at strike points, where the plasma contacts plasma-facing components. The divertor directs SOL plasma to target plates, where heat is exhausted, and particles are recycled.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Motivation, Objectives \& Organisation}

Plasma confinement is not perfect, leading to heat and particle fluxes in the radial direction. These fluxes are primarily driven by plasma turbulence and large-scale magnetohydrodynamical (MHD) instabilities, which play a key role in transport processes within a tokamak fusion plasma. These transport processes ultimately determine the plasma's density and temperature radial profiles.
Understanding edge transport processes is critical for designing future fusion devices and predicting their performance. While the core plasma properties dictate fusion efficiency, the edge plasma plays a pivotal role in setting the conditions for the core. An example is the importance of the edge H-mode (high confinement mode). During the transition to H-mode, edge sheared flows significantly suppress turbulence, enhancing confinement. This creates a transport barrier at the edge for heat and particles, reducing losses and forming pedestals in the density and temperature profiles (\cref{fig:plasma_regions}b).
When the plasma enters H-mode, the gradient region expands until MHD modes become unstable. The resulting edge-localized mode (ELM) instability ejects heat and particles into the SOL, causing significant heat loads in the divertor. After an ELM, the gradients flatten and recovers until instabilities triggers the next ELM \cite{zohm_edge_1996}.

Despite decades of research, particle and heat transport in plasmas remain only partially understood. In cases where a first-principles theory is unavailable, empirical scaling relations are often employed. These are derived by collecting extensive data from various experiments and determining empirical fits to describe the observations. These fits are then used to predict the performance of larger devices.
For predicting the confinement performance of burning plasmas, a global scaling law for energy confinement time has been empirically developed using engineering parameters, known as the IPB98(y,2) scaling \cite{iter_physics_basis_editors_chapter_1999-2}. However, such scaling laws often exhibit significant scatter, reflecting the influence of hidden parameters not explicitly included in the model. Geometric factors like divertor configuration, seeding levels, and fuelling rates are examples of known parameters that affect plasma confinement but are often excluded from these scalings.
Future devices are expected to operate under conditions that fall outside the boundaries typically explored in existing scaling laws. As a result, it is crucial to deepen our understanding of the underlying physics of these influencing factors before extrapolating predictions for next-generation fusion devices.

Next-generation fusion devices must operate in regimes of high energy confinement and high density to achieve significant fusion power. Ongoing experiments across various facilities aim to meet the stringent requirements of a fusion reactor, including achieving high energy confinement while ensuring safe plasma-wall interactions. To protect plasma-facing components, it will be essential to reduce the expected power loads. This reduction can be achieved through substantial volumetric radiative losses, facilitated by the injection of impurities (seeding).

In preparation for ITER, JET's main plasma-facing components were fully replaced with a new ITER-Like Wall (ILW), featuring predominantly beryllium in the main chamber and tungsten in the divertor. This significant change in plasma-facing materials has led to notable modifications in plasma pedestal behaviour, and in particular to a reduction in global confinement compared to operations with the previous carbon wall \cite{giroud_impact_2013,beurskens_global_2014}. This degradation has been partly attributed to the need for increased gas fuelling during ILW operations \cite{leyland_h-mode_2014,nunes_compatibility_2014,nunes_plasma_2016}.
Increased fuelling reduces the edge temperature, which in turn decreases the core temperature due to profile stiffness. However, promising results have been achieved through seeding, which has demonstrated the ability to raise both pedestal density and temperature compared to unseeded pulses \cite{giroud_impact_2013,giroud_progress_2015}. Additionally, confinement in JET-ILW plasmas can be modified by modifying the divertor configuration \cite{joffrin_impact_2014,de_la_luna_comparative_2014,maggi_pedestal_2015,joffrin_impact_2017}.
These findings highlight the critical role of the edge plasma, alongside the core, in determining overall plasma confinement.

To evaluate the impact of different SOL and divertor conditions on plasma confinement, numerous experiments have been conducted at JET over the past decade. These studies explored variations in parameters such as divertor configuration, triangularity, fuelling rate, and seeding levels. One of the key goals of this research is to further characterise how SOL properties and plasma-wall interactions influence changes in the  characteristics of the pedestal.

The midplane electron density is a critical interface parameter connecting the core plasma, which drives fusion performance, to the divertor plasma, responsible for power exhaust control. However, fusion devices face conflicting density requirements: optimal plasma performance demands a low separatrix density, while effective heat exhaust at the divertor necessitates higher densities.
Studies have shown a correlation between divertor conditions, separatrix density, and core plasma confinement, highlighting their interdependence \cite{nunes_plasma_2016,giroud_progress_2015,joffrin_impact_2014,de_la_luna_comparative_2014,maggi_pedestal_2015,dunne_role_2017,guimarais_poloidal_2018,stefanikova_effect_2018}. Despite this, several aspects require further investigation, including the confinement degradation caused by gas fuelling, the role of neutral particles and seeding impurities, and the  influence of separatrix density.
To address these gaps, high-resolution measurements of midplane density—both temporally and spatially—are essential for advancing our understanding and optimizing plasma performance.

To measure plasma density, one of the most widely used diagnostics is reflectometry. Microwave reflectometry uses the radar principle: probing waves travel through the plasma and are reflected at a cut-off layer, effectively serving as remote sensors for plasma electron density \cite{mazzucato_microwave_1998}.
The JET multiband reflectometry system, operational since 2007, has provided an extensive dataset of density profile measurements under a wide range of experimental conditions. The JET profile reflectometer offers higher temporal and spatial resolution for electron density profiles compared most of the diagnostics available at JET.  This enables more detailed analyses of edge density profiles, particularly in the SOL region, and facilitates the study of the temporal evolution of the instabilities.
Key parameters such as pedestal density, pedestal width, separatrix density, and SOL width can be derived from these density profiles and correlated with global parameters like confinement or with specific mechanisms affecting divertor and SOL conditions. The ultimate goal of this work is to enhance understanding of the connection between the SOL and pedestal plasma by investigating how they are influence by boundary conditions—such as fuelling, seeding, triangularity, and divertor configuration.

In this work, reflectometry data from JET was analysed with a focus in the pedestal and SOL regions, with the following objectives:

\begin{itemize}
    \item Understand the reconstruction of density profiles from reflectometry and its limitations in order to validate the profiles to be used;
    \item Develop a database of reflectometry measurements for experiments with variations in several potential mechanisms known to affect the divertor/SOL conditions;
    \item Perform a correlation analysis of the parameters measured by reflectometry with the selected discharge and SOL/divertor related quantities and  conclude about possible physics mechanisms behind the correlations found;
 %   \item Conclude about possible physics mechanisms behind the correlations found between the different plasma parameters.
    \item Study the temporal dynamic of the edge density profile along the ELM cycle.
\end{itemize}

This thesis is structured as nine chapters. The first chapter is the current one and argues why nuclear fusion is an important tool for our society and gives a brief introduction to nuclear fusion machines. Chapter two is an overview of the relevant theory, and the unique characteristics at \ac{jet} will be discussed in chapter three. The main diagnostics used for this work are explored in chapter four. Methodology starts on chapter five, where an explanation of all the steps required to process the data and how it was organised; and it is followed by a comparison of the models developed, commenting on which are the most appropriate for this work, in chapter six. Chapter seven is the discussion of the main results of this work and presents the parameter scalings found, comparing with the literature and concluding about the possible physical mechanisms. Chapter eight highlights the capabilities and potential of the reflectometer by looking at the temporal evolution of the density profile during instabilities. Conclusions and options for future work are presented in chapter nine.
