\chapter{Methods \& Code} \label{sec:code}

% \section{Introduction}

As seen in the previous chapter, the reflectometer at \ac{jet} is capable of generating hundreds of thousands of density profiles for every experiment, and the study window of $1-2 s$ \textcolor{blue}{não é explicado que a duração da janela de analise é de 1-2 s, nem sequer o que é a janela de analise} may have up to tens of thousands of individual profiles. When considering that a complete analysis might involve up to fifty shots \textcolor{blue}{novamente não é apresentado contexto}, it is clear that a degree of automation was necessary, while still being possible to manually check and adjust the processes to the characteristics of any particular shot.
\textcolor{blue}{Começar com uma descrição de alto nivel do que se pretendo em termos de fisica, que parametros vão ser estimados, em que fase da descarga, como lidas com o ELM, como é que o codigo vai implementar isso. Assim não se percebe. O centro da tese não é o codigo é a fisica. O paragrafo abaixo deve aparecer no inicio mas bastante expandido. }

The primary objective of the code was to reduce all the profiles in a window of a shot to a set of averaged parameters that can then be used to study the scaling laws of the machine. A process to achieve that goal was created at the beginning of the work and expanded, improved and optimised as more data was studied and results verified and validated. The overall process can be summarised as a combination of individual sub-processes, each with multiple steps.
\begin{enumerate}
    \item Select shot and window to process --- Manual;
    \item Generate fit data:
          \begin{enumerate}
              \item Choose a model to fit --- Manual;
              \item Select valid profiles --- Automatic;
              \item Fit all valid profiles to the chosen model --- Automatic;
              \item Filter fit results --- Automatic;
              \item Save fit data for later analysis --- Automatic;
          \end{enumerate}
    \item Extract average parameters:
          \begin{enumerate}
              \item Tune \ac{elm} detection algorithm --- Manual;
              \item Calculate stable times --- Automatic;
              \item Average fit parameters from valid profiles, inside of the window chosen and in stable times --- Automatic;
              \item Add all averaged plasma and profile parameters to a database --- Automatic.
          \end{enumerate}
\end{enumerate}
A flowchart of the above steps is shown in \cref{fig:codeFlow}. Note that most of the automatic steps above can be tuned, but not easily to individual shots. The models used in this work are presented in \cref{sec:models}.

As this process is applied to more and more shots, a thorough database is created, that contains both the plasma parameters, e.g. plasma current, magnetic field, and density profile parameters, such as pedestal height and \ac{sol} width. To ensure that analyses using this database are valid, it is important to validate every step of the process, to decrease the number and impact of systematic errors. This was done in several ways: by comparing intermediate results to other diagnostics and codes; and by manually and automatically testing all algorithms used.

While the main focus of the code written was the calculation of the average parameters, other tools and analyses were also created. The saved fit parameters can also be used in more ways than just calculating an average, including studying the effects of \acp{elm} in the density profile (\cref{sec:evolutions_in_time}) or generating a representative, averaged density profile for that show and window (\cref{sec:average_profile}).

\begin{figure}
    \centering
    \input{intro/code_flowchart.tex}
    \caption[Data Processing Flow]{Flow of all the steps involved in data processing. Blue parallelograms represent manual steps, orange and green rectangles the automatic steps, and the red rounded rectangles the more complex analyses available at that stage.}
    \label{fig:codeFlow}
\end{figure}

This chapter is meant an in-depth explanation of how the code was designed and how the raw data is transformed into the average parameters used in \cref{sec:discussion}. It is also meant to help other researchers that might want to use the data processed by this code, or even the code itself.

\section{Code \& Data Requirements}

The code used for the processing and analysis of the data was created using Python (version 3.7.1) \cite{python}. The libraries NumPy (version 1.15.4), SciPy (version 1.1.0) and Pandas (version 0.23.4) were used for data manipulation \cite{numpy,scipy,pandas,pandas_paper}; MatPlotLib (version 3.0.2), PyQt5 (version 5.15.4) and PyQtGraph (version 0.10.0) were used for plotting and user interfaces \cite{matplotlib}. The internal JET libraries (PPF and SAL/JET) for data access were also used.

At JET, all data, raw and processed, can be saved to internal servers, and can then be accessed in the internal network or through certain channels from the outside. The data is divided into three types:
\begin{itemize}
    \item JPF, Jet Pulse File, for raw diagnostic data;
    \item PPF, Processed Pulse File, for all kinds of processed data, such as reconstructed profiles, fit parameters, magnetic equilibrium and more;
    \item CPF, Central Physics File, for sampled data.
\end{itemize}

The data used for this work is almost entirely available in PPFs and two libraries were used: PPF and SAL/JET. The PPF library is only available internally, and it allows for reading and writing PPFs, and is used mostly to publicly save results. The SAL/JET library is available everywhere, can access data through the internet and currently can read both PPF and JPF, with data writing planned for the future.

It is useful to know the basics of how the PPF system is organised. Data is organised first by the numeric \ac{shot}. Inside each shot, there are multiple \ac{dda}, representing the diagnostic or tool used to create the data. Each DDA may have multiple \ac{dtype}, that store the different results written. To prevent data overwriting, there are multiple SEQUENCEs, numbered from 1, where a new sequence is created every time a PPF is saved. The sequence 0 can be used to access the latest data.

There is also a distinction between public and private PPFs, depending on which \emph{USER} is saving the data. Public data is mostly generated automatically after every shot, and can be considered safer to use. It is saved under the user \textbf{JETPPF}. Private PPFs can be written by any user with the appropriate permissions and should be considered unvalidated. Data generated during this work is stored under user \textbf{DNINA}.

It is important to note here that from this section forward, data from the reflectometer might be marked as \acs{kg10}, which is the \acl{kg10}.

\section{Generating Fit Data}

While generating the fit data, any profile may be marked as invalid, to ensure that data is not used afterwards. This is done by setting an integer, called reason of invalidity, that references the why and when the profile was marked invalid. A valid profile will always have its reason of invalidity set to 0. The basic reasons are shown in \cref{tbl:base_reasons}, and every subsequent step will introduce its own reasons to mark a profile as invalid.

\begin{table}
    \centering
    \begin{tabularx}{\linewidth}{L{.3\linewidth}Xr}
        \toprule
        Reason                  & Description                              & Value \\
        \midrule
        \textbf{VALID}          & Profile is valid                         & 0     \\
        \textbf{OUT\_OF\_SCOPE} & Profile is outside of window of analysis & 5     \\
        \bottomrule
    \end{tabularx}
    \caption[Base reasons of invalidity]{Base reasons of invalidity.}
    \label{tbl:base_reasons}
\end{table}

\subsection{Profile Selection} \label{sec:profile_selection}

\begin{figure}
    \centering
    \includegraphics[width=\linewidth]{images/code/broken_profiles}
    \caption[Examples of problematic profiles]{Examples of problematic profiles: on the left, "broken" profiles; on the right, "backwards" profiles.}
    \label{fig:broken_profiles}
\end{figure}

While the KG10 data is great both in temporal and spatial resolution, it can have unphysical profiles, due to plasma fluctuations or problems in the reconstruction. For instance, in \cref{fig:broken_profiles}, profiles from two shots are shown. On the left, shot \#86538, two examples of "broken" profiles, where there is a jump in radius, which is usually due to issues between the bands. On the right, shot \#82234 shows two examples of "backwards" profiles, where the radius is not monotonic. This issue can be due to fluctuations when small; or in cases where the error is larger, it is usually due to noise issues in the lower band with values returning to expected in the upper band.

As there can be many profiles in each shot/window (from less than 100 profiles to more than 10,000 profiles) and there is a significant percentage of problematic profiles (in certain shots, it can go up to 60-70\%), it would be impossible to select profiles manually.

Thus, it is necessary to have a profile filtering option, that looks at all individual profiles and marks them as valid or not. Currently, there are four different methods available: \emph{rdiff}, \emph{ravgdiff}, \emph{fitdev}, \emph{nrdiff}. All methods try to find gaps larger than expected or negative gradients and will mark each profile as non-valid if any issue is found. In this step, there are several non-valid reasons available as shown in \cref{tbl:profile_filter_reasons}.

\begin{table}
    \centering
    \begin{tabularx}{\linewidth}{L{.3\linewidth}Xr}
        \toprule
        Reason                     & Description                                   & Value    \\
        \midrule
        \textbf{NOT\_ENOUGH\_DATA} & Not enough data points either in $R$ or $n_e$ & 10 to 13 \\
        \textbf{VALUES\_NAN}       & Too many invalid (NaN or infinite) values     & 15       \\
        \textbf{R\_GRAD\_NEG}      & Negative gradient detected                    & 20       \\
        \textbf{BROKEN\_PROFILE}   & "Broken" profile detected                     & 25       \\
        \bottomrule
    \end{tabularx}
    \caption[Reasons of invalidity in profile selection]{Reasons of invalidity in profile selection.}
    \label{tbl:profile_filter_reasons}
\end{table}

Since the profiles have equal $n_e$ spacing, it is possible to look only at the $R$ data. The data is restricted to 15\% to 70\% of the maximum density with $R > 3.65$m, selecting only the data in the high gradient region. All methods have the same underlying algorithm:
\begin{enumerate}
    \item Calculate $R$ steps;
    \item Calculate gap threshold by multiplying the mean of differences in $R$ with $k_{threshold}$;
    \item Calculate negative threshold (fixed at 1mm);
    \item Count how many points are below the negative threshold;
          \begin{enumerate}%[(i)]
              \item If more than $N_{neg}$ steps: profile is marked with \textbf{R\_GRAD\_NEG};
          \end{enumerate}
    \item Count how many steps are above the gap threshold;
          \begin{enumerate}%[(i)]
              \item If more than $N_{broken}$ steps: profile is marked with \textbf{BROKEN\_PROFILE};
          \end{enumerate}
    \item Profile is considered valid at this level and goes through to the next steps.
\end{enumerate}

The differences between the methods come from the definition of a step, the gap threshold multiplier and the number of points required to mark as non-valid. \Cref{tbl:filter_methods} shows the definition of the different methods.

\begin{table}
    \centering
    \begin{tabularx}{\linewidth}{rlXlll}
        \toprule
        \# & Method          & Steps                                                & $k_{threshold}$ & $N_{neg}$ & $N_{broken}$ \\
        \midrule
        1  & \emph{rdiff}    & $R$ difference between adjacent points               & 7.0             & 1/3       & 1            \\
        2  & \emph{ravgdiff} & $R$ difference between the average of every 2 points & 5.5             & 1/3       & 1            \\
        3  & \emph{fitdev}   & Distance to Linear Fit                               & 5.5             & 1/3       & 1            \\
        4  & \emph{Nrdiff}   & $R$ difference between point M and point M+5         & 6.0             & 1/3       & 3            \\
        \bottomrule
    \end{tabularx}
    \caption[Profile selection methods and their definition]{Profile selection methods and their definition.}
    \label{tbl:filter_methods}
\end{table}

The different methods have their advantages and disadvantages (see \cref{tbl:filter_methods_efficacy}), but none reach values close to what a human would select. However, it is possible to improve further the efficacy of the methods by using a consensus method. By running three of the methods in \cref{tbl:filter_methods} and selecting the valid/non-valid consensus of the three, selections are much closer to the human. \Cref{tbl:filter_methods_efficacy} shows also the consensus method, using the name \emph{agg\_XYZ}, where X, Y and Z are the number of the method as stated in \cref{tbl:filter_methods}. Of all the methods, simple and aggregate, the most robust is \emph{agg\_234}, with the least number of false positives and negatives, and was chosen as the default method.

\begin{table}
    \centering
    \begin{tabularx}{\linewidth}{Xrlrlrlrl}
        \toprule
        Method   & \multicolumn{2}{c}{\# \textbf{VALID}} & \multicolumn{2}{c}{Same as Human} & \multicolumn{2}{c}{False \textbf{VALID}} & \multicolumn{2}{c}{False non-\textbf{VALID}}                           \\
        \midrule
        human    & 592                                   & 44\%                              & 1348                                     & 100\%                                        & 0   & 0\%  & 0   & 0\%  \\
        rdiff    & 812                                   & 60\%                              & 914                                      & 68\%                                         & 327 & 24\% & 107 & 8\%  \\
        ravgdiff & 599                                   & 44\%                              & 1097                                     & 81\%                                         & 129 & 10\% & 122 & 9\%  \\
        fitdev   & 489                                   & 36\%                              & 1123                                     & 83\%                                         & 61  & 5\%  & 164 & 12\% \\
        Nrdiff   & 592                                   & 44\%                              & 1116                                     & 83\%                                         & 116 & 9\%  & 116 & 9\%  \\
        agg\_123 & 603                                   & 45\%                              & 1117                                     & 83\%                                         & 121 & 9\%  & 110 & 8\%  \\
        agg\_124 & 653                                   & 48\%                              & 1125                                     & 83\%                                         & 142 & 11\% & 81  & 6\%  \\
        agg\_134 & 584                                   & 43\%                              & 1146                                     & 85\%                                         & 97  & 7\%  & 105 & 8\%  \\
        agg\_234 & 552                                   & 41\%                              & 1156                                     & 86\%                                         & 76  & 6\%  & 116 & 9\%  \\
        \bottomrule
    \end{tabularx}
    \caption[Efficacy of profile selection methods]{Efficacy of profile selection methods. Methods \emph{agg\_XYZ} are the result of a consensus between the methods X, Y and Z (see \cref{tbl:filter_methods}).}
    \label{tbl:filter_methods_efficacy}
\end{table}

\subsection{Model Fitting}

The remaining profiles can now be fitted to the chosen model (see \cref{sec:models}). The steps for each profile are:
\begin{enumerate}
    \item Restrict the data to $3.65 \text{m} < R < 3.88$m (default values, can be modified);
    \item Select default parameters ($R_0$ is calculated from the position of the maximum gradient);
    \item Fit the data to the selected model;
    \item Calculate the errors and goodness-of-fit indicators;
    \item Check resulting parameters for problems.
\end{enumerate}

Some models (see \cref{sec:join_models}) have slightly different fitting procedures, using multiple fits. If the fitting algorithm fails, it is marked with \textbf{FIT\_CONVERGENCE}, or \textbf{FIT\_NAN} if the issue is due to invalid values. Some models require a higher number of points available for fitting, if there are not enough data points, the profile is marked with \textbf{NOT\_ENOUGH\_DATA\_FIT}.

\begin{table}
    \centering
    \begin{tabularx}{\linewidth}{L{.33\linewidth}Xr}
        \toprule
        Reason                          & Description                              & Value \\
        \midrule
        \textbf{NOT\_ENOUGH\_DATA\_FIT} & Not enough data points for the model     & 14    \\
        \textbf{FIT\_CONVERGENCE}       & Model fitting could not converge         & 30    \\
        \textbf{FIT\_NAN}               & Model fitting resulted in invalid values & 35    \\
        \bottomrule
    \end{tabularx}
    \caption[Reasons of invalidity in profile fitting]{Reasons of invalidity in profile fitting.}
    \label{tbl:fit_reasons}
\end{table}

\subsection{Filtering Fit Results}

After fitting all valid profiles, another validation check is done, this time to the fit parameters and goodness-of-fit values. These checks serve as yet another way to avoid unphysical profiles. The non-valid reasons for this step are shown in \cref{tbl:fit_filter_reasons}.

\begin{table}
    \centering
    \begin{tabularx}{\linewidth}{lXcr}
        \toprule
        Reason                & Description                                                     & Enabled & Value \\
        \midrule
        \textbf{MEDIAN\_LOW}  & Parameter value is lower than 0                                 & YES     & 40    \\
        \textbf{MEDIAN}       & Parameter value is too far from the median (total of 40 and 42) & N/A     & 41    \\
        \textbf{MEDIAN\_HIGH} & Parameter value is larger than 100$\times$ the median           & YES     & 42    \\
        \textbf{ERROR\_RATIO} & Parameter error is much larger than value                       & NO      & 45    \\
        \textbf{CHI\_SQ}      & $\chi_{red}^2 > 100$                                            & YES     & 50    \\
        \textbf{R\_SQ}        & $R^2 < 0.75$                                                    & NO      & 60    \\
        \bottomrule
    \end{tabularx}
    \caption[Reasons of invalidity in fitting parameters]{Reasons of invalidity in filtering fit parameters.}
    \label{tbl:fit_filter_reasons}
\end{table}

A median of each fit parameter is calculated for the entire window or shot, and then each successful fit parameter is compared to the median. If the parameter value is lower than a certain fraction of the median (by default 0.0), the profile is marked with \textbf{MEDIAN\_LOW}. If the parameter is higher than a certain multiple of the median (by default 100$\times$), the profile is marked with \textbf{MEDIAN\_HIGH}. A combination value, \textbf{MEDIAN}, is available to serve as a combination of \textbf{MEDIAN\_LOW} and \textbf{MEDIAN\_HIGH}.

Finally the goodness-of-fit parameters are compared. If the parameter error is too large (by default 10$\times$ the parameter value), the profile is marked with \textbf{ERROR\_RATIO}. If the reduced chi-square, $\chi_{red}^2$, is larger than 100, the profile is marked with \textbf{CHI\_SQ}; and if the coefficient of determination, $R^2$, is smaller than 0.75, the profile is marked with \textbf{R\_SQ}.

These checks can be enabled and disabled with the median checks and the chi-square check enabled by default. The error check is usually disabled due to imprecision in the calculation of errors. The $R^2$ check is disabled as it is redundant with the chi-square check.

\subsubsection{Internal Structure of Fit Data}

\begin{table}
    \centering
    \begin{tabularx}{\linewidth}{lXr}
        \toprule
        Index            & Description                                      & Unit \\
        \midrule
        \verb|[T, P, 0]| & Value of Parameter \verb|P| and trigger \verb|T| & a.u. \\
        \verb|[T, P, 1]| & Error of Parameter \verb|P| and trigger \verb|T| & a.u. \\
        \verb|[T, 0, 2]| & Time of trigger \verb|T|                         & s    \\
        \verb|[T, 1, 2]| & $\chi^2_{red}$ of trigger \verb|T|               & N/A  \\
        \verb|[T, 2, 2]| & $R^2$ of trigger \verb|T|                        & N/A  \\
        \verb|[T, 3, 2]| & Reason of invalidity of trigger \verb|T|         & N/A  \\
        \bottomrule
    \end{tabularx}
    \caption[Structure of \texttt{fit\_pars}]{Structure of \texttt{fit\_pars[T, P, V]}, the array storing information of profile fitting.}
    \label{tbl:fit_pars_structure}
\end{table}

Internally, fit parameters are stored in a 3-dimensional array, \verb|fit_pars[T, P, V]|, where \verb|T| is the index for the trigger/time, \verb|P| is the index of each fit parameter (see \cref{sec:model_comparison}), and \verb|V| is the index to access the parameter values (\textbf{0}), errors (\textbf{1}) or extra values (\textbf{2}). These indexes and values are shown in \cref{tbl:fit_pars_structure}.

\begin{table}
    \centering
    \begin{tabularx}{\linewidth}{llX}
        \toprule
        \# Dimensions & Shape of Array   & Description                                                          \\
        \midrule
        1             & \verb|[P]|       & Only parameters values for a single time instance                    \\
        2             & \verb|[P, 3]|    & Parameter values, errors and extra values for a single time instance \\
        2             & \verb|[T, P]|    & Only parameters values for several time instances                    \\
        3             & \verb|[T, P, V]| & Complete \verb|fit_pars|                                             \\
        \bottomrule
    \end{tabularx}
    \caption[Possible dimensions of \texttt{fit\_pars} and their meanings]{Possible dimensions of \texttt{fit\_pars} and their meanings.}
    \label{tbl:fit_pars_dimensions}
\end{table}

The first dimension, \verb|T|, can have from 0 length to the total length of triggers in the shot. The second dimension, \verb|P|, can have a length varying from a minimum of 4 to the number of parameters of the model. Finally, the third dimension, \verb|V|, has a fixed length of 3. Dimensions can be suppressed depending on uses, as shown in \cref{tbl:fit_pars_dimensions}.

\subsection{Making the data available for other research}

There are currently two options to save the processed data calculated in the previous sections: files and PPFs. Both methods save the same data, though in different forms. Files have the advantage of being faster and stored locally; PPFs have the advantage of being available through the internet and by anyone with the appropriate permissions.

Compressed NumPy files (\emph{.npz}) are used as they store the data easily and compactly. They can be read using NumPy's \emph{load()} function that returns a dictionary (here called \emph{data}) with multiple keys, shown in \cref{tbl:saved_data_structure}. There are multiple \emph{DDA} and \emph{DTYPE} used in the PPFs, with the \emph{DTYPE}s saved shown in \cref{tbl:saved_data_structure}. The \emph{DDA} depends on the model used for the fitting and is shown in \cref{tbl:model_overview}. The PPF also includes an array for all time instances with the reference profile parameters (see \cref{tbl:dens_prof_parameters}).

\begin{table}
    \centering
    \begin{tabularx}{\linewidth}{Xll}
        \toprule
        Data                      & File Key                        & PPF DTYPE or other         \\
        \midrule
        Shot                      & \verb|data['shot']|             & N/A                        \\
        Time Window               & \verb|data['time_window']|      & N/A (always entire shot)   \\
        Model Used                & \verb|data['func'].item()|      & Stored in DDA              \\
        KG10 PPF User             & \verb|data['kg10_user'].item()| & INFO description           \\
        KG10 PPF Sequence         & \verb|data['kg10_seq'].item()|  & INFO value and description \\
        Complete \verb|fit_pars|  & \verb|data['fit_pars']|         & N/A                        \\
        Fit Parameters Values     & N/A                             & FPAR                       \\
        Fit Parameters Errors     & N/A                             & FERR                       \\
        Fit Reasons of Invalidity & N/A                             & CODE                       \\
        Ref. Profile Parameters   & N/A                             & PARS                       \\
        Code Version              & \verb|data['version'].item()|   & PPF version (IPVERS)       \\
        \bottomrule
    \end{tabularx}
    \caption[Saved data structure for both files and PPFs]{Saved data structure for both files and PPFs.}
    \label{tbl:saved_data_structure}
\end{table}

\section{Selecting Windows and Stability} \label{sec:elminfo}

\begin{figure}
    \centering
    \includegraphics[width=.95\linewidth]{images/code/shot_overview_example}
    \caption[Example of Shot Overview]{Shot overview for shot \#86538. The various plasma parameters are plotted in time. The window has been selected to have small variations of the parameters.}
    \label{fig:shot_overview}
\end{figure}

The selection of the window to analyse is also important since the plasma parameters can vary throughout the shot. The window should be chosen to minimise the variation of the most influential plasma parameters, such as heating power or gas fuelling. In \cref{fig:shot_overview}, a shot overview is shown, with some of the most relevant plasma parameters plotted in time. The window is at a time when the parameters do not change considerably.

With the large variations in plasma parameters taken into account, it is then necessary to look at the fast events, mostly ELMs. A peak-finding algorithm \cite{scipy} is used to detect ELMs by looking at the Beryllium flux arriving at the divertor, a known ELM indicator. The peak-finding algorithm detects peaks and returns both the point where a peak goes above a threshold (onset) and the point where it returns to baseline (offset). With those two points, it is possible to identify the times when ELMs are occurring.

During the window chosen to analyse, there may be none to several hundred ELMs occurring. To try and select the most stable periods during that window, the algorithm looks at the onset and offsets of the ELMs and selects only periods between ELMs, by default 70\% to 90\% of the time between the end of an ELM and the start of the next.

\begin{table}
    \centering
    \begin{tabularx}{\linewidth}{lXcr}
        \toprule
        Parameter              & Description                                                                   & Default & Unit                                          \\
        \midrule
        \textbf{Prominence}    & Minimum prominence of peaks                                                   & 0.090   & $\times 10^{14}$ s$^{-1}$ cm$^{-2}$ sr$^{-1}$ \\
        \textbf{Height}        & Minimum height of peaks                                                       & 0.000   & $\times 10^{14}$ s$^{-1}$ cm$^{-2}$ sr$^{-1}$ \\
        \textbf{Distance}      & Minimum distance (time) between peaks                                         & 5.000   & ms                                            \\
        \textbf{Width}         & Minimum width (time) of a peak                                                & 0.000   & ms                                            \\
        \textbf{Bound} (Start) & Fraction between ELMs when stable period starts                               & 0.700   & N/A                                           \\
        \textbf{Bound} (End)   & Fraction between ELMs when stable period ends                                 & 0.900   & N/A                                           \\
        \textbf{Min Duration}  & Minimum duration of stable period                                             & 1.000   & ms                                            \\
        \textbf{Max Duration}  & Maximum duration of stable period (negative values signify infinite duration) & -1.000  & ms                                            \\
        \bottomrule
    \end{tabularx}
    \caption[Reasons of invalidity in fitting parameters]{Reasons of invalidity in fitting parameters.}
    \label{tbl:elm_info_parameters}
\end{table}

However, this automatic method needs tuning, as every shot can have completely different ELM frequencies and behaviour. To allow for tuning, several parameters can be adjusted, changing both the peak-finding algorithm and the algorithm to find the stable periods. \Cref{tbl:elm_info_parameters} shows the parameters available to tune and their defaults.

To simplify the storing of these tunable parameters in the databases (see \cref{sec:databases}), they are saved as a single string of format "\textbf{Prominence} : \textbf{Height} : \textbf{Distance} : \textbf{Width} : \textbf{Bound Start} : \textbf{Bound End} : \textbf{Min Duration} : \textbf{Max Duration}". Any parameters missing are considered to be the default, which means the string ":::::::" returns the default values for all parameters. This allows simple storage of the specific changes for each shot since in most cases it is enough to change between one and three of the available parameters.

In \cref{fig:elm_tuning_before,fig:elm_tuning_after}, four examples of how to tune the ELM detection are shown. \Cref{fig:elm_tuning_before} shows the default detection, while \cref{fig:elm_tuning_after} depicts the detection with manually chosen values. The first column of each displays the Beryllium flux and an average of the pedestal half-width, calculated from the fitted profiles; the second column notes the frequency of detected ELMs and selected stable times. The averaged pedestal half-width is a good indicator of how much each ELM is affecting the plasma, since strong ELMs will have a very noticeable impact on the edge density profile, namely increasing the pedestal width.

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{images/code/elm_tuning_default}
    \caption[Examples of ELM Detection Tuning - Default]{Examples of the default ELM detection. The two columns on the left show the default values, and the two columns on the right show the manually chosen values. The first and third columns show the Beryllium flux and an average of the pedestal half-width; the second and fourth columns show the frequency of detected ELMs and selected stable times.}
    \label{fig:elm_tuning_before}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{images/code/elm_tuning_tuned}
    \caption[Examples of ELM Detection Tuning - Tuned]{Examples of tuned ELM detection. The two columns on the left show the default values, and the two columns on the right show the manually chosen values. The first and third columns show the Beryllium flux and an average of the pedestal half-width; the second and fourth columns show the frequency of detected ELMs and selected stable times.}
    \label{fig:elm_tuning_after}
\end{figure}

Each shot in \cref{fig:elm_tuning_before,fig:elm_tuning_after} have different ELMs frequencies and behaviour and thus require different tunings. In each case, the objective of the tuning is to obtain a more consistent frequency in stable times, to minimise the impact of the ELMs on the averaged results. Usually, the changes are to eliminate smaller ELM peaks and to require a minimum time for the stable times. In shot \#83307, the minimum prominence has increased to more than double and the minimum duration increased; in shot \#84772, the minimum height and duration were increased, and the minimum distance was decreased, as there are series of medium size ELMs that were not being detected; and finally in shot \#86689, the minimum height and duration were again increased, and the minimum width was also increased, to ignore very short peaks.

High frequencies, like in shot \#86638, can be frequent enough that the plasma reaches a quasi-stability where individual ELMs do not have a large impact on the overall plasma. In such cases, the best option is usually just to consider the entire high frequency period as stable.

\section{How to Use the Fit Data}

\subsection{Extracting the Averages} \label{sec:distilling_shots}

With the profiles fitted, the windows selected, and the stable times defined, it is possible to summarise each profile parameter into a single averaged value. Profiles used in the calculation of the average are all valid profiles inside the window and in a stable time. As it is frequent for very large outliers to exist, the average selected was the median, to improve the robustness of the entire process. \Cref{fig:extract_average_pars} shows a window of a JET shot with the most important steps present: the valid profiles are marked in grey circles, and their rolling average is an orange line; the valid profiles on stable times are black circles and their median value is highlighted by a red horizontal line. The ELMs are shown through the Beryllium signal and the stable times are represented by a light blue background.

These distilled parameter averages can then be stored in a database (see \cref{sec:databases}) to then be used for more complex analyses, including the main results in \cref{sec:discussion}.

\begin{figure}
    \centering
    \includegraphics[width=\linewidth]{images/code/extract_average_pars.png}
    \caption[Extracting Average Parameters]{The various steps to extract the average pars: all valid fits parameters in grey, their rolling average in orange, the profiles in stable times, i.e. the used profiles, in black and the median of the used data in a red dashed line. The stable times are also shown in a light blue background, with the ELM signal being the Beryllium flux.}
    \label{fig:extract_average_pars}
\end{figure}

\subsection{Evolutions in time} \label{sec:evolutions_in_time}

The raw profile fits can still be useful, particularly to study the time evolution of the profile. As the reflectometer can have a time resolution down to 15$\mu$s, it is possible to investigate entire events like \acp{elm}, that happen in the time range of the millisecond. \Cref{fig:extract_average_pars} is an example of such an analysis, where it becomes obvious that the profile parameters are strongly affected by the occurrence of \acp{elm}.

This type of analysis is the basis for \cref{sec:time_evo}.

\subsection{Average Profiles} \label{sec:average_profile}

\begin{figure}
    \centering
    \includegraphics[width=\linewidth]{images/code/average_profile_example.png}
    \caption[Average Profile Example]{Example of average profiles: the calculated average profile is shown in red, and its deviation is the orange background; the profile defined by the median parameters of the MTanh\^{}Exp is presented in black dashed line. The lower plot contains the frequency profile and the KG10 bands.}
    \label{fig:average_profile_example}
\end{figure}

Another interesting analysis possible is calculating an average profile. There are two ways of creating an average profile: by using the average parameters or by averaging profiles. The median parameters allow for an easy creation of an average profile as they can be applied directly to the model to obtain a density profile. To average all profiles in a window, more care is required. As seen in \cref{sec:reflectometry}, KG10 profiles can have large jumps in absolute position, which would drastically increase the errors if averaging in a naive way. Since the reconstruction of a reflectometry density profile requires fixed frequencies/densities, with the radius being the free variable, the averaging should be on the radius and for each frequency/density. And to decrease the impact of the position shifts, it is possible to use the calculated $R_0$ and shift all profiles according to their pedestal centre. \Cref{fig:average_profile_example} compares the two average profiles, with the result of the averaged profiles shown in red, and its standard deviation in an orange background; and with the profile created from the MTanh\^{}Exp model and its average parameters in black dashes. The two profiles are close, as expected, though the profile from average parameters has a less pronounced pedestal top, due to the smoothness of the hyperbolic tangent.

These average profiles are a good and quick way to manually evaluate if the data, in particular the fitted data, is correct. For example, if the two average profiles, from the average of all profiles and from the fit parameters, have a noticeable disagreement, it indicates a problem somewhere either with the data or with the process. All shots used in \cref{sec:discussion} were screened in this way to remove problematic shots.

%«%»%«%»%«%»%«%»%«%»%«%»%«%»%«%»%«%»%«%»%«%»%«%»%«%»%«%»%«%»%«%»%«%»%«%»%«%»%«%»%«%»%

\input{results/databases}