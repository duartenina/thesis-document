| Article    | Citation                                                        | Section                | Citations linked          |
| ---------- | --------------------------------------------------------------- | ---------------------- | ------------------------- |
| Discussion | divertor configuration on the pedestal parameters               | horvath_isotope_nodate | 53                        |
| Discussion | ne(div pos)                                                     | maggi_pedestal_2015    |                           |
| Discussion | divertor affects plasma performance (best is CC)                | da_la_luna_recent_2016 | tamain_investigation_2015 |
| Discussion | effective pumping is only possible through the divertor corners | da_la_luna_recent_2016 |                           |
