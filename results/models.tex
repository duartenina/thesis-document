\chapter{Profile Models} \label{sec:models}

As stated in the previous chapter, the main objective of the code used was to extract average parameters that defined the characteristic density profile for that particular shot and window. It is necessary to define the parameters that the fitting models should be able to describe. It is possible to divide the density profiles studied into three regions, each defined by its own set of parameters: core, pedestal and \ac{sol}.

The core is simply defined by its \ac{acore}, a linear interpretation of the behaviour of the core plasma close to the edge.

The pedestal can be defined by four different parameters: its \ac{R0}, usually defined as the position with the highest gradient; the \ac{h} and the \ac{b}, the densities at the start and end of the pedestal; and the \ac{W} - and the related \ac{d} - defined as the distance between start and end of the pedestal. The \ac{aped} is derived from the above and is the average gradient of the region.

The \ac{sol} is mostly defined by a single parameter, the \ac{lsol}, defined as the length of the exponential drop in the density of the region. There is also the \ac{rsol}, which is fixed at the pedestal end ($R_0 + d$) for most models, but kept free for others.

An example of a reflectometer density profile and its important profile parameters is illustrated in \cref{fig:density_profile_example} and a description of those parameters, including unit and range, is presented in \cref{tbl:dens_prof_parameters}.

It is important to note that while not all models directly use these parameters, they can still be derived from the model parameters.

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{images/intro/density_profile_example.png}
    \caption[JET Density Profiles]{Example of a JET edge density profile and the parameters that define it.}
    \label{fig:density_profile_example}
\end{figure}

\begin{table}
    \centering
    \begin{tabularx}{.8\linewidth}{XcrC{5pt}ll}
        \toprule
        Parameter           & Symbol     & \multicolumn{3}{l}{Usual Range} & Unit                                     \\
        \midrule
        Pedestal center     & $R_0$      & 3.75                            & -    & 3.85 & m                          \\
        Pedestal width      & $W$        & 1.0                             & -    & 6.0  & cm                         \\
        Pedestal half-width & $d$        & 0.5                             & -    & 3.0  & cm                         \\
        Pedestal height     & $h$        & 3                               & -    & 7    & $\times 10^{19}\ $m$^{-3}$ \\
        Pedestal bottom     & $b$        & 0                               & -    & 3    & $\times 10^{19}\ $m$^{-3}$ \\
        Pedestal gradient   & $a_{ped}$  & 50                              & -    & 450  & $\times 10^{19}\ $m$^{-4}$ \\
        Core gradient       & $a_{core}$ & 2                               & -    & 22   & $\times 10^{19}\ $m$^{-4}$ \\
        SOL start           & $R_{SOL}$  & 3.78                            & -    & 3.88 & m                          \\
        SOL width           & $l_{SOL}$  & 2                               & -    & 6    & cm                         \\
        \bottomrule
    \end{tabularx}
    \caption[Density Profile Parameters]{Description of density profile parameters.}
    \label{tbl:dens_prof_parameters}
\end{table}

\section{Available Models}

\begin{table}[h!]
    \centering
    \begin{tabularx}{\linewidth}{llclXc}
        \toprule
        Model                   & Type     & DDA & SOL    & Parameters                                                        & \# Fits \\
        \midrule
        Tanh                    & MTanh    & TNH & ---    & $b$, $h$, $R_0$, $d$                                              & 1       \\
        \textbf{MTanh}          & MTanh    & MTH & ---    & $b$, $h$, $R_0$, $d$, $s_{core}$                                  & 1       \\
        MTanhLin                & MTanh    & MTL & Linear & $b$, $h$, $R_0$, $d$, $s_{core}$, $s_{sol}$                       & 1       \\
        MTanhPoly               & MTanh    & MTP & Poly.  & $b$, $h$, $R_0$, $d$, $a_{core}$, $s_0$, $s_1$, $s_2$, $s_3$      & 1       \\
        MTanhExp                & MTanh    & MTE & Exp.   & $b$, $h$, $R_0$, $d$, $a_{core}$, $L_{sol}$                       & 1       \\
        \midrule
        OneLine                 & Linear   & LI1 & ---    & $b$, $h$, $R_0$, $d$                                              & 1       \\
        TwoLine                 & Linear   & LI2 & ---    & $n_{ped}$, $R_{ped}$, $a_{ped}$, $a_{core}$                       & 1       \\
        ThreeLine               & Linear   & LI3 & ---    & $b$, $h$, $R_0$, $d$                                              & 1       \\
        \midrule
        \textbf{MTanh\^{}Exp}   & Compound & MJE & Exp.   & $b$, $h$, $R_0$, $d$, $s_{core}$, $L_{sol}$, $R_{sol}$            & 2       \\
        \textbf{TwoLine\^{}Exp} & Compound & TJE & Exp.   & $n_{ped}$, $R_{ped}$, $a_{ped}$, $a_{core}$, $L_{sol}$, $R_{sol}$ & 2       \\
        \bottomrule
    \end{tabularx}
    \caption[Overview of models]{Overview of models. The default models in the code are marked in bold.}
    \label{tbl:model_overview}
\end{table}

There are three types of models used: the hyperbolic tangent based models, which start at the simple Tanh and try to expand and improve it for all regions; the line models, which try to use simple linear fits to describe the regions; and the more complex compound models, that use two or more different models to improve its behaviour in the different regions.

\Cref{tbl:model_overview} presents an overview of all the available models, their type, their \ac{dda}, their \ac{sol} behaviour, their parameters and the number of fits necessary for each profile. In bold are marked the default models used in the code. These defaults were chosen because they are either the current standard (MTanh) or the best models for reflectometry data (both compound models).

\section{Hyperbolic Tangent Based Models} \label{sec:tanh_models}

An edge density profile in an H-mode plasma has a very distinct shape that resembles a hyperbolic tangent (Tanh), with two separate regions with different behaviours connected through a high gradient. It is then a good basis to create more complex models that are closer to the data.

\Cref{fig:tanh_models} shows the different models that use a Tanh as the basis. The left plot shows how the modified Tanh (MTanh) resembles more closely the core profile than the simple Tanh. The right plot shows a zoomed picture of the SOL region and how the Tanh models with a SOL component behave in that region.

\begin{figure}
    \centering
    \includegraphics[width=\linewidth]{images/models/tanh_models}
    \caption[Simple and modified hyperbolic tangent models]{Simple and modified hyperbolic tangent models. Models were slightly shifted to separate them. The left plot shows the entire edge density profile and how the models try to describe the core region. The right plot focuses on the SOL region and how the different models describe it.}
    \label{fig:tanh_models}
\end{figure}

\subsection{Simple Tanh} \label{sec:tanh}

\begin{equation} \label{eq:tanh}
    n(R) = b + \frac{h-b}{2} \left[
        \tanh\left(\frac{R_0-R}{d}\right) + 1
        \right]
\end{equation}

The simple hyperbolic tangent (Tanh, \cref{eq:tanh}) is a simple but robust model to calculate two very important profile parameters: the centre position of the high gradient zone (known as $R_0$) and the width of that same region (usually known as $W$ or $d$ where $d = W/2$). While it can be a good choice when a quick model for calculating $R_0$ or $d$, it describes poorly the core and SOL regions, as it assumes a flat profile for both: $h$ for the pedestal density and $b$ for the SOL density.

\subsection{MTanh} \label{sec:mtanh}

\begin{align} \label{eq:mtanh}
    n(R) =   & b + \frac{h-b}{2}\left[\frac{e^{Q}\left(1 + s_{core}\cdot Q\right) - e^{-Q}}{e^{Q} + e^{-Q}}+1\right] \\
    Q =      & \frac{R_0-R}{d}                                                                                       \\
    a_{core} & = \frac{h - b}{2 d} s_{core} \label{eq:acore}
\end{align}

One of the most common models used with H-mode density profiles is the modified tanh (MTanh) \cite{frassinetti_eurofusion_nodate}, where a linear component in the core is added (\cref{eq:mtanh}), $s_{core}$. It is easier to use a different R-axis, named $Q$, where the centre of the high gradient region is $0$. In the core $Q << 0$, and in the SOL $Q >> 0$, allowing for simplifications.

As the core density profile, especially close to the edge, behaves linearly, this model is quite good at describing the density profile, for diagnostics that have poor or no data in the SOL. Since the reflectometer has good data of the SOL region, the MTanh by itself is not a good choice to fit its data.

It is possible to convert $s_{core}$ into the more useful $a_{core}$ by using \cref{eq:acore}.

\begin{figure}
    \centering
    \includegraphics[width=.9\linewidth]{images/models/tanh_d_comp}
    \caption[Comparison of pedestal width of MTanh and MTanhExp]{Comparison of pedestal width of MTanh and MTanhExp models.}
    \label{fig:mtanh_d_comp}
\end{figure}

\subsection{MTanhLin} \label{sec:mtanhlin}

\begin{align} \label{eq:mtanhlin}
    n(R)        & = \frac{e^{Q}\cdot n_{core}(R) + e^{-Q}\cdot n_{SOL}(R)}{e^{Q} + e^{-Q}} \\
    n_{core}(R) & = h + \frac{h - b}{2} \cdot s_{core} \cdot Q \label{eq:mtanhlin_core}    \\
    n_{SOL}(R)  & = b + \frac{h - b}{2} \cdot s_{SOL} \cdot Q \label{eq:mtanhlin_sol}
\end{align}

An SOL expansion of the MTanh model is required. It is possible to rewrite \cref{eq:mtanh} into a more clear form: \cref{eq:mtanhlin}, dividing it into several components that describe the core (\cref{eq:mtanhlin_core}) and SOL (\cref{eq:mtanhlin_sol}) regions. The simplest expansion is a linear profile for the SOL, with another parameter $s_{SOL}$. The MTanhLin can be useful, as a simple and fast model to roughly describe the three regions of the density profile, but it is not very accurate in the SOL region.

\begin{align}
    L_{SOL} & = \frac{b - b/e}{a_{SOL}} - d = \frac{e - 1}{e} \frac{2 d}{h - b} \frac{b}{s_{SOL}} - d \label{eq:lsol_from_ssol} \\
    a_{SOL} & = \frac{h - b}{2 d} s_{SOL}
\end{align}

It is possible to convert $s_{SOL}$ into $L_{SOL}$ by calculating the distance required for the SOL density to drop to $1/e$ of the pedestal bottom, similar to the definition of an exponential length. The factor $-d$ arises from the fact that $n_{SOL}$ is defined around $R_0$ and not $R_0 + d$.

\begin{table}
    \centering
    \begin{tabular}[b]{lrrrr}
        \toprule
        Parameter     & mtanh & mtanhexp \\
        \midrule
        $R_0$ (m)     & 3.810 & 3.803    \\
        $d$ (m)       & 0.017 & 0.013    \\
        $R_0 - d$ (m) & 3.793 & 3.790    \\
        $R_0 + d$ (m) & 3.828 & 3.815    \\
        \bottomrule
    \end{tabular}
    \caption[Comparison of pedestal width of mtanh and mtanhexp]{Comparison of pedestal width of mtanh and mtanhexp models.}
    \label{tbl:mtanh_d_comp}
\end{table}

\subsection{MTanhPoly} \label{sec:mtanhpoly}

\begin{align}
    n_{core}(R) & = h + a_{core} \cdot \left(R_0 - R\right) \label{eq:mtanhpoly_core}                         \\
    n_{SOL}(R)  & = b \left(s_3 \cdot Q^3 + s_2 \cdot Q^2 + s_1 \cdot Q + s_0\right) \label{eq:mtanhpoly_sol}
\end{align}

The next increase in complexity is trying a higher order polynomial for the SOL (in \cref{fig:tanh_models} and \cref{eq:mtanhpoly_sol}, a polynomial of the third order is shown). By rewriting \cref{eq:mtanhlin_core} into \cref{eq:mtanhpoly_core}, the parameter $a_{core}$ is used, describing the gradient of the core region.

While polynomials of the second and third order can describe the SOL region well, the MTanhPoly model is not very robust and requires increased care when fitting.

\begin{figure}
    \centering
    \includegraphics[width=.9\linewidth]{images/models/line_models}
    \caption[Line models]{Comparison of Line Models. Models were slightly shifted to separate them.}
    \label{fig:line_models}
\end{figure}

\subsection{MTanhExp} \label{sec:mtanhexp}

\begin{align}
    n_{core}(R) & = h + a_{core} \cdot \left(R_0 - R\right)                                        \\
    n_{SOL}(R)  & = b \cdot \exp\left[\frac{(R_0 + d) - R}{L_{SOL}}\right] \label{eq:mtanhexp_sol}
\end{align}

As seen in \cref{sec:fusion_plasmas}, the \ac{sol} is described by an exponential profile. The MTanhExp model (\cref{eq:mtanhexp_sol}) adds an exponential component to the SOL region, introducing an important parameter, $L_{SOL}$, which is the width of the SOL region.

While the MTanhExp closely follows both the data and the literature, it has one large problem. Since the SOL is now an exponential that interacts more strongly with the exponentials of the hyperbolic tangent, the MTanhExp model usually has a much smaller pedestal half-width ($d$) than the other MTanh models (see \cref{fig:mtanh_d_comp,tbl:mtanh_d_comp}). The MTanhExp model gives the same position of the top of the pedestal ($\approx R_0 - d$), but underestimates both $R_0$ and $d$, resulting in a SOL starting higher on the profile than expected. It is clear that MTanhExp is a bad choice to describe the high gradient region.

\section{Line Models} \label{sec:line_models}

There are other simple models, using linear functions, that can describe part or all of the edge density profile. These models mostly try to be fast and accurate predictors of the high gradient region ($R_0$ and $d$).

\subsection{OneLine} \label{sec:oneline}

\begin{figure}
    \centering
    \includegraphics[width=.9\linewidth]{images/models/oneline}
    \caption[OneLine Model]{Steps for the OneLine model.}
    \label{fig:oneline}
\end{figure}

\begin{equation} \label{eq:oneline}
    n(R) = h - \frac{h - b}{2\cdot d} \cdot \left[R - (R_0 - d)\right]
\end{equation}

The OneLine method (\cref{eq:oneline}) restricts the data by ordering in radius ($R$) and selecting all data points between two indexes, selected empirically (usually between $2/10$ and $5/10$ of points with $R > 3.7$m). It then calculates a linear fit of these points and finds the points where the data deviates from the linear fit more than a specified distance (usually $2 \sigma$ for the top of the pedestal and $1 \sigma$ for the top of the SOL). These points of divergence define $h$, $b$, $R_0$ and $d$. \Cref{fig:oneline} shows the relevant steps and parameters of the OneLine model.

The OneLine is a fast and fairly accurate method to find $R_0$ and $d$. However, it is not very robust and requires a good choice of limits for the data selection.

\subsection{TwoLine} \label{sec:twoline}

\begin{equation} \label{eq:twoline}
    n(R) =
    \begin{cases}
        n_{ped} - a_{core} \cdot (R - R_{ped}) & \text{if } R < R_{ped}    \\
        n_{ped} - a_{ped} \cdot (R - R_{ped})  & \text{if } R \geq R_{ped}
    \end{cases}
\end{equation}

\begin{equation} \label{eq:twoline_convert}
    \begin{split}
        R_0 = R_{ped} + d
    \end{split}
    \qquad
    \begin{split}
        d = \frac{h}{2 \cdot a_{ped}}
    \end{split}
    \qquad
    \begin{split}
        h = n_{ped}
    \end{split}
    \qquad
    \begin{split}
        b = 0
    \end{split}
\end{equation}

For the TwoLine model (\cref{eq:twoline}), the first step is calculating a rough $R_0$ by smoothing the data and finding the position of the largest gradient $d R/d n_e$. Data is selected around the approximate $R_0$ (usually from $R_0 - 12.5$cm to $R_0 + 1.5$cm) and fitted to a step function model with two linear slopes, one for the core and one for the high gradient region.

The TwoLine model is quite robust, fast and accurate for the core and high gradient regions. It is possible to extract the regular profile parameters with \cref{eq:twoline_convert}.

\subsection{ThreeLine} \label{sec:threeline}

\begin{equation} \label{eq:threeline}
    n(R) =
    \begin{cases}
        h                                     & \text{if }  R < R_0 - d              \\
        h - \frac{(h-b) (R - (R_0 - d))}{2 d} & \text{if }  R_0 - d \leq R < R_0 + d \\
        b                                     & \text{if }  R \geq R_0 + d
    \end{cases}
\end{equation}

Since $R_0$ is a very useful parameter that can be used in more complex analyses, the ThreeLine model (\cref{eq:threeline}) was created. It calculates an $R_0$ by finding the position of the largest gradient of the smoothed data and then fits a step function model with flat regions in the core and SOL and a linear function connecting them.

The ThreeLine should be used only in cases where a fast and rough $R_0$ is required.

\begin{figure}
    \centering
    \includegraphics[width=\linewidth]{images/models/join_models_steps.png}
    \caption[Compound Models Steps]{Steps to obtain the final fit of a compound model, in this case MTanh\^{}Exp.}
    \label{fig:join_models_steps}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\linewidth]{images/models/join_models}
    \caption[Compound Models]{Compound models: mtanh\^{}exp (left) and twoline\^{}exp (right). Each plot shows the different regions and parameters for each fit.}
    \label{fig:join_models}
\end{figure}

\section{Compound Models} \label{sec:join_models}

The tested base models above, while having advantages, all have large difficulties accurately describing the exponential character of the \ac{sol}. More complex models are required, models that have multiple fitting steps and use step functions. At first, a step function with a fixed joining position, \acs{rsol}, was tried. However, this required manual adjusting of \acs{rsol} for every experiment. Having \acs{rsol} as a free parameter in a fit had the same issue as MTanhExp, of a smaller pedestal width.

To solve these problems, the current method was developed, with the steps shown in \cref{fig:join_models_steps} using MTanh as the base model. First, the raw profile is fitted with the base model (either MTanh or TwoLine) and the pedestal and core parameters are obtained. The \ac{sol} data is defined using \ac{R0} and \ac{d} and is used to fit a simple exponential model, obtaining \ac{lsol}. \Ac{rsol} is calculated by finding the intersection of the base model and the exponential model. If there is more than one intersection, the closest to $R_0 + d$ is selected; and if there are no intersections, the profile is marked as invalid.

\subsection{MTanh+exp / MTanh\^{}exp} \label{sec:mtanhjoinexp}

\begin{equation}
    n(R) =
    \begin{cases}
        n_{mtanh}(R) & \text{if } R < R_{SOL}    \\
        n_{SOL}(R)   & \text{if } R \geq R_{SOL}
    \end{cases}  \label{eq:mtanhpexp_parts}
\end{equation}
\begin{align}
    n_{mtanh}(R) & = b + \frac{h-b}{2}\left[\frac{e^{Q}\left(1 + s_{core}\cdot Q\right) - e^{-Q}}{e^{Q} + e^{-Q}}+1\right] \\
    n_{SOL}(R)   & = \exp\left(- \frac{R - R_{SOL}}{L_{SOL}}\right)                                                        \\
    R_{SOL}      & = R_0 + d \cdot frac_{d} \label{eq:mtanhpexp_rsol}
\end{align}

As the MTanh model describes well the core and high gradient regions, it is a good option as the first part of a compound model. The MTanh+Exp and MTanh\^{}exp models are the results of a step function joining an MTanh with an exponential function, joining at a position $R_{SOL}$. Both models first fit the MTanh as usual and then fit an exponential to the SOL data (defined as $R > R_0 + d$).

MTanh+Exp uses a fixed \ac{rsol}, while MTanh\^{}exp uses the three-step process and finds the intersection.

MTanh\^{}exp is a very good model for the KG10 data, accurately describing all regions of the edge density profile. However, while it is continuous, its derivative is not. Nevertheless, it is one of the best choices for this data.

\subsection{TwoLine+exp / TwoLine\^{}exp} \label{sec:twolinejoinexp}

\begin{equation}
    n(R) =
    \begin{cases}
        n_{ped} - a_{core} \cdot (R - R_{ped})         & \text{if } R < R_{ped}              \\
        n_{ped} - a_{ped} \cdot (R - R_{ped})          & \text{if } R_{ped} \leq R < R_{SOL} \\
        \exp\left(- \frac{R - R_{SOL}}{L_{SOL}}\right) & \text{if } R \geq R_{SOL}
    \end{cases}
\end{equation}

Since the TwoLine model is also quite accurate in the core and high gradient regions, it is another good choice for the compounding method described in the previous section (\ref{sec:mtanhjoinexp}). As before, trying to fix $R_{SOL}$ requires manual tuning for each shot and window, so TwoLine\^{}exp follows the same method, and finds the interception between the TwoLine and exponential models.

The TwoLine\^{}exp model is another good option for the KG10 data, with similar advantages and disadvantages as the MTanh\^{}exp model.

\section{Which Model and When} \label{sec:model_comparison}

% \note{add example shot with parameter evolution for both, possibly with histogram to see scatter }

For a good number of interesting analyses, such as the average profile (\cref{sec:average_profile}), only the $R_0$ is required (besides the original profile data). For those cases, there is no need for a heavier and more complex model, and simple and fast models like the Tanh (\cref{sec:tanh}), OneLine (\cref{sec:oneline}) or ThreeLine (\cref{sec:threeline}) are good choices.

\begin{figure}
    \centering
    \includegraphics[width=\linewidth]{images/models/join_models_comp}
    \caption[Parameter Comparison of Compound Models]{Parameter comparison for MTanh\^{}exp and TwoLine\^{}Exp. Each plot shows a different parameter with MTanh\^{}exp on the X axis and TwoLine\^{}exp on the Y axis. A linear trendline of their correlation is also plotted in dashed.}
    \label{fig:join_models_comp}
\end{figure}

For the main topic of this work, scaling laws of plasma and profile parameters, all parameters are used, especially the SOL parameters. As such, it is necessary to use a model that describes well all regions, which means the two compound models MTanh\^{}exp and TwoLine\^{}exp (\cref{sec:mtanhjoinexp,sec:twolinejoinexp}). Both models are a good option, but with some differences.

In \cref{fig:join_models_comp}, a comparison between MTanh\^{}Exp and TwoLine\^{}Exp of some of the most important parameters is shown. For most of them, they correlate quite closely, with no large deviations. However, there are some parameters where the two models diverge.

The core slope ($a_{core}$) is consistently higher in the TwoLine\^{}exp model, mostly due to fitting more closely the density close to the pedestal where the gradient is higher than further into the core.

Of more interest are the pedestal half-width ($d$) and slope ($a_{ped}$), which are connected to each other and to the pedestal height ($h$), which is quite consistent between models. The TwoLine\^{}exp gives a larger width at low pedestal widths ($< 1.5$ cm), and a similar width at medium widths ($\approx 1.5$ cm). At larger widths, there is less data which makes the trend unclear. The same, but opposite, effect happens with the pedestal slope: low widths mean high pedestal slopes and the TwoLine\^{}exp relatively larger widths mean smaller slopes.

Overall, both models can be used to describe well the edge density profile as measured by the reflectometer. For this work, the MTanh\^{}exp has been used in most cases, both because the base model of the MTanh is more common in the literature, allowing for easier comparison; and because the MTanh tends to have a small advantage in fit quality and robustness.

\begin{figure}
    \centering
    \includegraphics[width=\linewidth]{images/models/sep_pos}
    \caption[Finding the Separatrix]{Example of density profile in linear (top) and logarithmic (bottom) scales. An exponential model shown as a red line and relevant positions from an MTanh\^{}Exp model are marked as blue lines}
    \label{fig:sep_pos}
\end{figure}

\section{SOL Densities and the Separatrix} \label{sec:sep_dens}

% \begin{figure}
%     \centering
%     \includegraphics[width=\linewidth]{images/models/sol_densities.png}
%     \caption[SOL Densities Comparison]{Comparison of the different SOL densities for both MTanh\^{}Exp and TwoLine\^{}Exp, shown in different rows. Each column has a different SOL density for its x-axis, and all SOL densities are plotted against that one. Linear trends are shown. Note that for TwoLine\^{}Exp, $b$ is defined as exactly $n(R_{SOL})$.}
%     \label{fig:sol_densities}
% \end{figure}

The separatrix density is crucial to understand the behaviour of edge plasma. Unfortunately there is no simple and robust method to measure the separatrix position (and thus density) with high accuracy.
The position calculated from magnetic diagnostics has an uncertainty in the order of a couple of centimetres, which translates to large variations in density. \Cref{fig:sep_pos} presents a density profile in linear (top) and logarithmic (bottom) scales, with an exponential model shown as a red line and relevant positions from an MTanh\^{}Exp model marked as blue lines.
Looking particularly at the logarithmic scale, there is no clear marker for the separatrix, with no changes seen in the density profile between the confined and \ac{sol} regions. As illustrated, quite often exponential behaviour is seen from the far-SOL almost to the top of the pedestal. This shows how hard it is to extract the separatrix density from such a profile. To facilitate this, different fitting parameters were considered as estimates of the separatrix density: the density at the \acf{b}, the density at the \acs{rsol} ($n(R_{SOL})$) and the density at the end of the high gradient region ($n(R_0+d)$).

% The relation between these three \ac{sol} densities is shown in \cref{fig:sol_densities}, calculated for both MTanh\^{}Exp and TwoLine\^{}Exp. Each model has its own row and each density has its own column. In each plot, $b$ is marked as blue circles, $n(R_{SOL})$ as orange squares, and $n(R_0+d)$ as green diamonds. Remember that for TwoLine\^{}Exp, $b$ is defined as exactly $n(R_{SOL})$. Looking at the relations, it is clear that all densities are strongly correlated with each other, particularly $n(R_{SOL})$ and $n(R_0+d)$. $b$ has a weaker correlation and is always lower than the other two in MTanh\^{}Exp, since $b$ is defined solely by the original hyperbolic tangent, without taking into consideration the exponential character of the \ac{sol}. Overall, while it is interesting to see how all three change in different conditions, it should be expected that all have similar behaviours.

A large database of plasma, density, temperature and pressure profile parameters of \ac{jet} experiments has been developed: the JET Pedestal Database \cite{frassinetti_eurofusion_nodate}. It is possible to compare some of its results with the ones obtained in this work and \cref{fig:ne_sep_comp} presents a comparison between the separatrix density as measured by the \ac{hrts} and the different \ac{sol} densities obtained from both models. Linear trends are shown in every combination, and their slopes and quality of fit are available in the legends of the figures. While the quality of fit, as measured by the R$^2$, is low, it is possible to see that there is some correlation between the separatrix density and the \ac{sol} densities, in particular $n(r0+d)$. There is a group of points that seem to be outliers, around $n_{sep} \approx 2.5$, whose cause is still not understood. It is reasonable to use $n(r0+d)$ as a possible proxy for the separatrix density, but always with some degree of caution.

\begin{figure}
    \centering
    \includegraphics[height=.8\textheight]{images/models/ne_sep_comp.png}
    \caption[Comparison between Separatrix and SOL densities]{Comparison between separatrix and SOL densities for both compound models. Separatrix densities from \cite{frassinetti_eurofusion_nodate}. A linear trend is shown for each combination, and its slope and quality of fit are presented in the legend.}
    \label{fig:ne_sep_comp}
\end{figure}