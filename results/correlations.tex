\chapter{Parameter Scalings}
\label{sec:discussion}

\newcommand{\parplotcaption}[2]{
  \caption[#1 scalings]{#1 scalings. #2 Top row shows the most relevant device parameters; the overall plasma scalings are presented in the second row, the pedestal scalings in the third row, and the SOL scalings in the fourth row.}
}
\newcommand{\profplotcaption}[2]{
  \caption[Density profiles and scale lengths for #1 scalings]{Representative density profiles (first and second rows) and density scale lengths for #1 scalings. #2 The profiles are generated from the average profile parameters used in the scalings.}
}

% \textcolor{blue}{Adicionar legendas detalhadas em todas as figuras. Deixar claro se a densidade da separatrix foi obtida para R0+d fixo em cada scan ou variável. }

In this chapter, the parameters derived from the fits to the density profiles for the different datasets presented in the previous chapter are studied and correlated with the relevant main plasma parameters, aiming at contributing to a better understanding of quantities such as the pedestal and \ac{sol} widths and the pedestal and separatrix densities. Numerous \ac{hmode} discharges were performed during recent \ac{jet} experimental campaigns, including for instance input power and fuelling scans and the application of nitrogen seeding. In this chapter, the following main plasma parameters were selected to serve as indicators of the overall plasma performance: \acf{h98}, \acf{taue} and \acf{wp}. Based on the availability of reflectometry data, and the experiments performed, the selected datasets include scans of the divertor configuration, gas fuelling and seeding, plasma current,  heating power, and  triangularity. The final section presents some of the correlations that are possible to observe using the entire validated database. All parameters derived from the average density profiles presented in this section were obtained with the MTanh\^{}Exp model.

%\textcolor{blue}{Talvez seja útil definir abreviaturas neste capitulo para a posição do outer strike-point como é feito frequentemente: HT, C, VT. }

%\textcolor{blue}{No capítulo do fits discutes as incertezas? Qual é a incerteza típica em percentagem nos vários parâmetros. }

%«================================================================================»%
\section{Divertor Configuration}
\label{sec:disc:div_out}

\begin{figure}
  \centering
  \includegraphics[width=1.\textwidth]{images/correlations/div_out_DivertScan_01.png}
  \parplotcaption{Divertor configuration}{Low power shots in orange squares, high power shots in red circles.}
  \label{fig:div_out_DivertScan_01}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{images/correlations/div_out_DivertScan_01_profiles.png}
  \profplotcaption{divertor configuration}{Horizontal configuration shot in solid light blue line, corner in dashed dark blue line, and vertical in dotted purple line.}
  \label{fig:div_out_DivertScan_01_profiles}
\end{figure}

Previous studies at JET (e.g. \cite{maggi_pedestal_2015,joffrin_impact_2014,joffrin_impact_2017,de_la_luna_recent_2016}.) have shown that the divertor configuration influences not only the global plasma confinement but also other physics mechanisms such as the L-H power threshold. Modifications in the divertor configuration are often associated with changes in the lower triangularity, distance between the X-point and the strike-points, location of the strike-points and their distance to the pump throat. These in turn may result in a large number of modifications that are difficult to distinguish including the connection length, \ac{sol} flows, pumping efficiency, fuelling efficiency and recycling patterns.

In this section, the scan studied was based on a dataset from the JET task T17-05, "Pedestal analysis and isotope effect" and first discussed in \cite{joffrin_impact_2017}. It is composed of two sub-scans with different heating powers, and \cref{fig:div_out_DivertScan_01} highlights the different heating powers and divertor configurations. The scan includes discharges with the outer strike-point in the horizontal target (H, tile 5), vertical target (V, tile 7) and in the corner configuration (C, tile 6). As illustrated, parameters characterising the confinement, \ac{h98}, \ac{taue} and \ac{wp}, improve the most on the corner configuration, with the horizontal target configuration having slightly higher values than the vertical target. This is a known result and is believed to arise from the better pumping efficiency available in the corner configuration \cite{maggi_pedestal_2015,tamain_investigation_2015,joffrin_impact_2014}.

The most relevant parameters derived from the density profile are presented in \cref{fig:div_out_DivertScan_01}. The pedestal height decreases when the position of the outer strike-point changes from the horizontal target, to corner, and then to the vertical, as previously reported in scans with constant gas fuelling and heating power \cite{maggi_pedestal_2015,joffrin_impact_2014,joffrin_impact_2017}.
Although the pedestal height is largest for the horizontal target configuration, the confinement is not the best for this configuration as it is associated with a lower pedestal temperature.
%A different behaviour was reported when considering shots with different gas fuelling rate and heating power, where the corner configuration has the lowest pedestal height \cite{maggi_pedestal_2015,frassinetti_pedestal_2021}. A similar behaviour has been reported in the literature for the separatrix density, where gas scans at different divertor configurations show that the density is lowest when in a corner configuration \cite{frassinetti_pedestal_2021}.
These results also show that the separatrix density has a similar behaviour to that of the pedestal height, decreasing as the outer strike-point is moved from the horizontal target, to the corner, and then to the vertical target.

The pedestal width is the parameter with weaker dependence on the divertor configuration ($\approx 20$\% variation between minimum and maximum values), showing a higher value in the corner configuration. The pedestal slope follows the behaviour of the pedestal height, as the pedestal width shows a modest variation with the divertor configuration.
Finally, the \ac{sol} width shows a very clear behaviour: the corner configuration has a much lower width that is very well correlated with the behaviour of the main plasma parameters characterising the plasma confinement and suggesting a possible relation between the \ac{sol} physics and the behaviour of the main plasma.

A representative average electron density radial profile is presented in \cref{fig:div_out_DivertScan_01_profiles} for each divertor configuration, which allows the visualisation of the impact of the divertor configuration on the density profile from the far-\ac{sol} up to the region inside the pedestal top. As illustrated, the density is higher for the horizontal target configuration across most of the profile, with a broader \ac{sol} clearly seen. On the contrary, the \ac{sol} density profile is notable steeper for the corner configuration, as revealed by the smaller density scale length. This is likely associated with reduced SOL radial transport, which appears to influence the entire plasma behaviour.
The density scale length is larger for the horizontal target configuration across most of the profile, suggesting increased radial transport for this configuration.
%also varies with divertor configuration, with most changes occurring in the \ac{sol}. \note{review this}

As suggested in \cite{joffrin_impact_2017}, a possible explanation for the effect of the divertor configuration is that the stronger pumping efficiency in the corner configuration leads to a reduced neutral pressure in the divertor and \ac{sol} regions, directly influencing the main plasma parameters. Parameters such as the edge toroidal rotation that influences plasma confinement may be directly modified by the interaction with neutrals. The lower \ac{sol} neutral pressure could also lead to reduced ionisation sources and, therefore, a lower \ac{sol} density. These results suggest that the changes in the divertor configuration, associated with the changes in the \ac{sol} density profiles, may impact the overall plasma behaviour. As shown in \cite{joffrin_impact_2017}, the neutral pressure in the main chamber varies inversely with the energy confinement across the different divertor configurations, which was later also associated with changes in the ionisation distribution that extends over a broader region in the horizontal target configuration \cite{lomanowski_parameter_2023}. This appears to be consistent with the far-SOL density profiles that is highest for the horizontal target configuration and lowest to the corner configuration. Despite the lower pedestal height in the corner configuration, the core electron density and pressure are the highest of the three configurations \cite{joffrin_impact_2014,maggi_pedestal_2015}, which is reflected in the plasma stored energy.

%«================================================================================»%
\section{Gas Fuelling}
\label{sec:disc:majr}

\begin{figure}
  \centering
  \includegraphics[width=1.\textwidth]{images/correlations/majr_scans_0X.png}
  \parplotcaption{Gas fuelling}{Low triangularity and corner configuration in blue diamonds, high triangularity and horizontal configuration in green triangles.}
  \label{fig:majr_scans_0X}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{images/correlations/majr_scans_0X_profiles.png}
  \profplotcaption{gas fuelling}{Low gas fuelling shot in solid orange line, high gas in dashed red line.}
  \label{fig:majr_scans_0X_profiles}
\end{figure}

The influence of fuelling in \ac{hmode} experiments has been extensively investigated \cite{maggi_pedestal_2015,leyland_h-mode_2014,frassinetti_pedestal_2021,dunne_role_2017,lomanowski_experimental_2022,saarelma_self-consistent_2019,urano_characterization_2016,stefanikova_pedestal_2020,schneider_characterization_2012,leyland_pedestal_2013,leyland_pedestal_2014}. High fuelling rates are typically applied to achieve a detached operation, reducing the expected power loads on the plasma-facing components due to the narrow width of the \ac{sol} power flux profile characteristic of the \ac{hmode}. Additionally, fuelling also influences plasma confinement. The structure of the pedestal density plays an important role in the pedestal stability and hence in the confinement, as previously observed in \ac{jet} and \ac{aug} \cite{dunne_role_2017,stefanikova_effect_2018}. Fuelling also impacts the ELM dynamics \cite{frassinetti_dimensionless_2016}, with the ELM frequency typically reduced with increasing fuelling rate. However, fuelling induces many and complex effects and the physics mechanisms such as confinement degradation with gas fuelling, the role of the neutrals, and the importance of the separatrix density are not fully understood.

\Cref{fig:majr_scans_0X} shows the two scans used in this section that span different ranges of the gas fuelling rate, one at low triangularity and in corner configuration (blue diamonds) and another at high triangularity and with the outer strike-point in the horizontal target (green triangles). The low triangularity scan was based on a dataset from the JET task T17-05, "Pedestal analysis and isotope effect", first discussed in \cite{nunes_compatibility_2014}; while the high triangularity scan is part of a dataset discussed in \cite{leyland_h-mode_2014}. The overall plasma confinement has a clear trend, with the three parameters, \ac{h98}, \ac{taue} and \ac{wp}, decreasing with increasing gas rate, as observed before \cite{leyland_h-mode_2014,lomanowski_experimental_2022}.

The changes in the density profile also have some clear trends. The pedestal height presents a small decrease with the fuelling rate that appears to be consistent with the confinement degradation observed in the scan studied here. However, different experiments at \ac{jet} \cite{leyland_h-mode_2014,saarelma_self-consistent_2019,frassinetti_pedestal_2021,dunne_role_2017,urano_characterization_2016,schneider_characterization_2012} and in other devices such as \ac{aug} and \ac{diiid} \cite{schneider_characterization_2012} have shown the opposite trend or no changes with the fuelling.

The separatrix density, defined as the density at the end of the high gradient region ($R_0 + d$), presents a small positive correlation with the fuelling rate (not shown). However, the position $R_0 + d$ moves outward with fuelling, leading to a lower density as it is measured farther into the \ac{sol}. To compensate for this effect, a better proxy for the separatrix density was estimated using a fixed radial position across each scan (the average value of the interception point for each scan). This separatrix density shows a clear increase with fuelling, as expected \cite{frassinetti_pedestal_2021,lomanowski_experimental_2022}. Another relevant parameter is the ratio between the separatrix proxy density and the pedestal density, that shows a strong positive correlation with the gas rate as expected \cite{frassinetti_role_2021}.

The pedestal and \ac{sol} density widths are also presented in \cref{fig:majr_scans_0X}, showing strong positive correlations with the fuelling rate, particularly in the \ac{sol}. This increase in the pedestal density width is also reflected in the decrease of the pedestal slope, in agreement with previous observations \cite{leyland_h-mode_2014,stefanikova_pedestal_2020,beurskens_effect_2013}.

Density profiles at low and high fuelling rates are compared in \Cref{fig:majr_scans_0X_profiles}, revealing the strong broadening of the profiles with the fuelling rate. The density scale length increases from the pedestal region up to the \ac{sol}.

All these effects combined indicate that the increased gas fuelling degrades particle confinement due to enhanced radial transport, which causes a broadening of the density profile across a large radial region, from the pedestal top up to the far-SOL. As the fuelling rate is increased, the pedestal density is eroded as the pedestal height decreases and the separatrix density increases. Fuelling appears to have a rather similar effect on the \ac{sol} and on the confined region, which may suggest that the physics mechanics driving changes in these two regions are connected in the case of the fuelling scans.

It has been observed in different devices that the SOL width increases with collisionality due to enhanced radial transport associated with filaments \cite{carralero_experimental_2014,wynn_investigation_2018}. This is in agreement with our observations as an increase in the fuelling rate is expected to lead to an increase in the SOL collisionality.

By comparing the two scans, it is also clear that the impact of triangularity and divertor configuration is generally larger than the impact of the fuelling rate, as the differences between the scans are, on average, larger than the change along the two fuelling scans, particularly in parameters such as the pedestal height and the pedestal end density. Differences between the two scans are discussed in the respective sections (effect of divertor configuration and upper triangularity).

%«================================================================================»%
\section{Nitrogen Seeding}
\label{sec:disc:seed}

\begin{figure}
  \centering
  \includegraphics[width=1.\textwidth]{images/correlations/seed_n2_scans_0X.png}
  \parplotcaption{Nitrogen seeding}{Nitrogen seeding scan in red squares.}
  \label{fig:seed_n2_scans_0X}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{images/correlations/seed_n2_scans_0X_profiles.png}
  \caption[nitrogen seeding]{No seeding shot in dotted grey line, low seeding in solid light blue line, and high seeding in dashed dark blue line.}
  \label{fig:seed_n2_scans_0X_profiles}
\end{figure}

In the next generation of fusion devices, a reduction of the expected power loads on the plasma facing components is mandatory, requiring a detached operation with high densities and low temperatures achieved at high fuelling rates associated with large volumetric losses resulting from seeding. Therefore, operation with a high radiated power fraction is of great interest. Impurities, such as nitrogen, are standardly used to cool the divertor plasma via line radiation. Furthermore, seeding often leads to confinement enhancement \cite{dunne_role_2017,dunne_impact_2016,frassinetti_effect_2015,kallenbach_impurity_2013}.

\Cref{fig:seed_n2_scans_0X} shows the results of the nitrogen seeding scan used, from a subset of some scans discussed in \cite{leyland_h-mode_2014}. Looking at the plasma confinement factors, it is clear all three parameters, \ac{h98}, \ac{taue} and \ac{wp}, remain roughly constant with the increase in seeding.
The density profile parameters are also presented in \cref{fig:seed_n2_scans_0X}. The pedestal density height decreases with seeding, as reported before \cite{leyland_h-mode_2014} and also observed in other machines \cite{dunne_role_2017}. The pedestal density width remains roughly constant across the scan, leading to a decrease in the pedestal slope with seeding. The separatrix density also decreases with seeding while the \ac{sol} width does not shown a clear trend.

\Cref{fig:seed_n2_scans_0X_profiles} shows three density profiles, one with no seeding, in a grey dotted line, one with low seeding, in a light blue line, and one with a high value of seeding, in dark blue dashes. As expected, the main difference is in the pedestal region, as seen at \ac{aug} \cite{schneider_characterization_2012}, with the rest of the profile not showing significant changes.

The main parameter influence of seeding appears to be a decrease in the density pedestal height, leading to a decrease in the pedestal slope, and the pedestal width is not significantly modified. The SOL width is also not significantly modified by seeding. The discharge without seeding appears to have a different behaviour from those with seeding, which may be attributed to worse plasma confinement. In terms of the density profile, it is also characterised by a broader pedestal and a broader SOL. Looking at the density profiles, it is clear that the density decay length is not significantly modified by nitrogen seeding, suggesting that for the scan studied here, radial transport in the pedestal and SOL regions is weakly influenced by seeding.

%«================================================================================»%
\section{Triangularity}
\label{sec:disc:triang}

\begin{figure}
  \centering
  \includegraphics[width=1.\textwidth]{images/correlations/triang_TriangScan_11.png}
  \parplotcaption{Triangularity}{Deuterium scans of 2013/2014, in pink squares, and of 2019 in light blue hexagons, and D-T scan with seeding in green triangles.}
  \label{fig:triang_TriangScan_11}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{images/correlations/triang_TriangScan_11_profiles.png}
  \profplotcaption{triangularity}{Low triangularity shot in solid light blue line, high triangularity in dashed dark blue line.}
  \label{fig:triang_TriangScan_11_profiles}
\end{figure}

Plasma shaping, particularly the triangularity, strongly influences pedestal stability, consequently influencing plasma confinement. While the effect of triangularity on the density profile is often clear, the impact on the overall plasma confinement and on the pedestal pressure is more complex \cite{ frassinetti_pedestal_2021}. \Cref{fig:triang_TriangScan_11} shows the three different scans used in our analysis: two similar scans performed at different dates, 2013/14 in pink squares and 2019 in blue hexagons; and a D-T scan in green triangles. The D-T scan also has low seeding rate of neon and was performed in the vertical divertor configuration. Scan "D 2019" was based on a dataset from \cite{horvath_pedestal_2023}, while the other two scans were created for this work. Note that, due to the changes mentioned in \cref{sec:jet_reflectometer}, the density profiles in the 2013/14 scan were reconstructed using the previous code, resulting in differences between the 2013/14 and 2019 scans, more noticeably in the increased widths. While the values of the different scans cannot be compared directly, it is still possible to compare the trends.

The pedestal density height, gradient and the separatrix density exhibit a large increase with triangularity, as reported before \cite{maggi_pedestal_2015,frassinetti_pedestal_2021,horvath_pedestal_2023,urano_characterization_2016}. Both the pedestal and \ac{sol} widths show a clear decrease with triangularity. The trends are coherent across the three scans analysed here, with the variations in the different density profile parameters across the scan being larger than the variations between scans.
The density profiles illustrated in \cref{fig:triang_TriangScan_11_profiles} confirm the large impact of the triangularity on the density profile: increasing the triangularity leads to a high pedestal density and to narrow pedestal and \ac{sol} widths. These changes are likely attributed to the improved pedestal stability at high triangularity, suggesting reduced transport in the pedestal region that extends to the \ac{sol}. This is highlighted by the decreased density scale length in the pedestal and \ac{sol} regions at high triangularity.
As observed with the fuelling rate scan, triangularity appears to have a similar effect on the \ac{sol} and on the confined region, which may suggest that the physics mechanics driving changes in these two regions are connected.

The effect of triangularity on the overall plasma confinement is more complex. As illustrated in \Cref{fig:triang_TriangScan_11}, while the 2019 scan shows a slight improvement in the overall confinement with triangularity, the contrary is observed in the other two scans. This apparent contradiction between the changes in the density profile and those in the overall plasma confinement results from the changes in the electron temperature profile. In the 2019 scan, the temperature profile remains roughly constant across the triangularity scan, leading to higher pressure pedestals (since the density is higher) \cite{horvath_pedestal_2023}. In the other two scans, the temperature profile deteriorates with triangularity, leading to similar or even lower pedestal pressures as the triangularity is increased \cite{maggi_pedestal_2015,frassinetti_pedestal_2021}. The effect of triangularity impacts both density and temperature profiles, and therefore the entire picture cannot be obtained with the density profile alone.

%«================================================================================»%
\section{Heating Power}
\label{sec:disc:pheat}

\begin{figure}
  \centering
  \includegraphics[width=1.\textwidth]{images/correlations/pheat_PowerScan_11.png}
  \parplotcaption{Heating power}{Horizontal configuration scan in green stars, corner configuration in purple hexagons.}
  \label{fig:pheat_PowerScan_11}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{images/correlations/pheat_PowerScan_11_profiles.png}
  \profplotcaption{heating power}{Low heating power shot in solid light blue line, high power in dashed dark blue line.}
  \label{fig:pheat_PowerScan_11_profiles}
\end{figure}

Increasing the heating power generally leads to a larger pedestal pressure. However, scaling laws of global energy confinement time exhibit a degradation with heating power.
Two heating power scans were created and analysed here with the results shown in \cref{fig:pheat_PowerScan_11}: a horizontal target divertor configuration scan (H/5) in green stars and corner scan (C/6) in purple hexagons. Other plasma characteristics are similar. Both scans also have a similar dependence on \ac{betan} and \acl{wp}, with both parameters increasing with heating power. On the contrary, \ac{taue} decreases with heating power as expected from the power degradation of the plasma confinement. It is also interesting to note that \ac{h98} increases with heating power in the corner scan, suggesting that in this case the power degradation is weaker than predicted by the \ac{h98} scaling, which is consistent with the better confinement observed in the corner configuration.

As presented in \cref{fig:pheat_PowerScan_11}, the pedestal density profile parameters exhibit clear trends with heating power. The pedestal density height and slope decrease with heating power, while the pedestal width increases. This behaviour was observed previously \cite{maggi_pedestal_2015,frassinetti_pedestal_2021,horvath_isotope_nodate}, although in \ac{aug}, an increase of pedestal height with the heating power was reported \cite{schneider_characterization_2012}. A decrease in the separatrix density is seen, again in agreement with previous results \cite{frassinetti_pedestal_2021}.
The \ac{sol} width presents a different behaviour in the two scans; it increases slightly with heating power in the horizontal target scan, while it shows a clear decrease in the corner scan. This apparent contradiction may be due to the different divertor configurations. The divertor configuration strongly influences recycling and pumping, and therefore it is anticipated that it can strongly influence the \ac{sol} density profiles. As shown previously in \cref{sec:disc:div_out}, the \ac{sol} decay length tends to be smaller for the corner configuration.

As described above, the SOL width has been observed to increase with collisionality. This is again in agreement with our observations, as an increase in heating power is expected to lower the SOL collisionality, therefore reducing the SOL width.
Furthermore, according to the two-point model, the power e-folding length of the \ac{sol} in the high recycling regime is expected to increase with density and to decrease with input power \cite{stangeby_plasma_2000}. Our observation of the \ac{sol} width in corner configuration appears to be consistent with the models of the divertor \ac{sol}.


It is important to note that the heating power affects mostly the plasma temperature profile and thus the pressure \cite{frassinetti_pedestal_2021,maggi_pedestal_2015}. The decrease in pedestal density with heating power is likely due to the changes in pedestal stability resulting from the large increase in pedestal temperature.

The density profiles presented in \cref{fig:pheat_PowerScan_11_profiles} for the corner scan are characterised by a high heating power profile having a noticeable larger pedestal width. Interestingly, the density scale length has opposite behaviour in the \ac{sol} and in the pedestal regions. While in the \ac{sol}, it decreases with heating power; in the pedestal region, it increases. This is not the case for the horizontal configuration. This suggests that heating increases transport both in the pedestal and \ac{sol} for discharges with the strike-point in the horizontal target, while in the corner configuration, the two regions appear to be decoupled, with transport increasing only in the pedestal region. Again, the corner configuration appears to have a distinct behaviour characterised by a reduced SOL transport.

%«================================================================================»%
\section{Plasma Current}
\label{sec:disc:curr}

\begin{figure}
  \centering
  \includegraphics[width=1.\textwidth]{images/correlations/curr_CurrScan_51.png}
  \parplotcaption{Plasma current}{Current scan in green stars.}
  \label{fig:curr_CurrScan_51}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{images/correlations/curr_CurrScan_51_profiles.png}
  \profplotcaption{plasma current}{Low plasma current shot in solid orange line, high current in dashed red line.}
  \label{fig:curr_CurrScan_51_profiles}
\end{figure}

The pedestal density is generally observed to have a strong dependence on the plasma current. The plasma current also influences the maximum achievable density through the well-known Greenwald limit. In the experiment, it was difficult to isolate changes in the plasma current as they were often associated with changes in the fuelling rate, heating power, or toroidal magnetic field.

The plasma current scan created for this work, presented in \cref{fig:curr_CurrScan_51}, also includes noticeable changes in gas fuelling rate and heating power. However, the impact of the plasma current is expected to be the dominant factor in this scan, when compared to the effect of the fuelling rate and the heating power described previously (\cref{sec:disc:majr,sec:disc:pheat}).
The overall plasma confinement is improved with the increase in plasma current, with both the \acl{taue} and \acl{wp} increasing. It is also interesting to note that the \ac{h98} decreases with plasma current, which suggests that it might overestimate its impact on the scaling.

The effect of the plasma current on the density profiles parameters is presented in \cref{fig:curr_CurrScan_51}. Both the pedestal height and the separatrix density increase strongly with plasma current, as reported before \cite{frassinetti_pedestal_2021,schneider_characterization_2012,urano_characterization_2016}. It is interesting to note that the pedestal width increases in such a way that the pedestal slope is roughly constant. In contrast, the \ac{sol} width decreases with plasma current, which may be, at least partially, explained by the effect of the \ac{sol} connection length. The \ac{sol} connection length has an inverse dependence on the plasma current. \ac{sol} profiles are expected to become broader as the connection length increases as particles have to travel longer distances to reach the divertor.

\Cref{fig:curr_CurrScan_51_profiles} shows the density profiles for two values of plasma current. Differences in the pedestal height and in the \ac{sol} width are very clear. The density is larger for the high plasma current discharge except in the far-SOL.
The density scale length is lower has opposite behaviour in the \ac{sol} and in the pedestal regions. While in the \ac{sol}, it decreases with plasma current, in the pedestal region, it increases. The fact that the pedestal gradient does not change significantly with plasma current suggests that the transport is also not substantially modified. This may be also the case in the \ac{sol}, as the observed changes are likely due to geometric effects (changes in the connection length).

%«================================================================================»%
\section{Summary of the different scans:}

Reflectometry measures the density profiles from the far-\ac{sol} up to inside the pedestal top, which may allow assessing a possible coupling between the different plasma regions. This can be achieved, for instance, by studying how the density decay length varies across the density profile. The changes induced by the different plasma parameters investigated here on the density profiles are summarised below.

\paragraph*{Divertor Configuration}
The position of the outer strike-point leads to complex effects in the density profiles. Changes in the \ac{sol} and pedestal density profiles appear to be partially decoupled.
%, suggesting that different physics are governing the two regions with
The \ac{sol} transport strongly depends on the divertor configuration and the  \ac{sol} width is well correlated with the behaviour of the main plasma parameters characterising the plasma confinement, suggesting a possible relation between the \ac{sol} physics and the behaviour of the main plasma.
The corner configuration has a lower \ac{sol} width and appears to have a distinct behaviour in some of the scans analysed here. The density is larger for the horizontal target configuration across the entire profile.

\paragraph*{Fuelling Rate:}
Changes in the density profile induced by an increase in the fuelling rate suggest enhanced radial transport in both the \ac{sol} and pedestal regions, resulting in a larger   \ac{sol} and pedestal widths. The density scale length increases with fuelling from the pedestal top up to the far-SOL. An  increase in the separatrix density is associated with the degradation of pedestal performance. The increase of the ratio of the separatrix and pedestal densities is associated with a reduction of the pedestal gradients and increased transport.

\paragraph*{Seeding:}
Nitrogen seeding leads to modest changes in the density profiles for the scan analysed here.

\paragraph*{Triangularity:}
Changes in the density profile suggest decreased transport in both the \ac{sol} and pedestal regions. The density scale length decreases with triangularity  from the pedestal top up to the far-SOL.  Triangularity leads to an increase in both pedestal density (and pressure) and separatrix density due to improved pedestal stability.

\paragraph*{Heating power:}
Both the pedestal and separatrix densities decrease with heating power. On the contrary, pedestal pressure increases with heating power. The slope of the pedestal density and pressure decrease with power, suggesting increased transport. The \ac{sol} behaviour is strongly dependent on the divertor configuration. There are other factors at play, such as the relative shift of the density and temperature profiles, that influence pedestal stability.

\paragraph*{Plasma current:}
Its increase leads to an increase in both pedestal density (and pressure) and separatrix density. However, geometric effects in the \ac{sol} appear to decouple the physics governing the \ac{sol} and pedestal region with the density scale length exhibiting an opposite trend in the two regions.

\paragraph*{Multiple Variable Scalings}
Scalings for the pedestal and separatrix densities derived in \cite{frassinetti_pedestal_2021,urano_characterization_2016,schneider_characterization_2012} (\cref{tbl:scalings_exponents}) show good agreement with the dependencies reported in this work. A similar dependency was also observed between the pedestal density and separatrix density, exhibiting a strong positive dependence on the plasma current and triangularity, a weak positive dependence on the fuelling rate, and a negative dependence on the input power.

\begin{table}
  \centering
  \begin{tabularx}{\textwidth}{Xcccc}
    \toprule
    Parameter                                         & Gas Fuelling    & Triangularity   & Heating Power    & Current         \\
    \midrule
    Ped. Dens. \cite{frassinetti_pedestal_2021} \#1   & 0.08 $\pm$ 0.04 & 0.62 $\pm$ 0.14 & -0.34 $\pm$ 0.11 & 1.24 $\pm$ 0.19 \\
    Ped. Dens. \cite{frassinetti_pedestal_2021} \#2   & ---             & 0.57 $\pm$ 0.16 & -0.34 $\pm$ 0.11 & 1.40 $\pm$ 0.18 \\
    Ped. Dens. \cite{urano_characterization_2016} \#1 & 0.11            & 0.56            & -0.08            & 1.14            \\
    Ped. Dens. \cite{urano_characterization_2016} \#2 & 0.10            & 0.54            & -0.07            & 1.28            \\
    Ped. Dens. \cite{schneider_characterization_2012} & ---             & ---             & ---              & 0.8 $\pm$ 0.1   \\
    \midrule
    Sep. Dens. \cite{frassinetti_pedestal_2021}       & 0.23 $\pm$ 0.07 & 0.82 $\pm$ 0.27 & -0.43 $\pm$ 0.16 & 1.1 $\pm$ 0.3   \\
    \bottomrule
  \end{tabularx}
  \caption[Scaling Laws Exponents]{Exponents of multiple variable scalings in the literature.}
  \label{tbl:scalings_exponents}
\end{table}


%«================================================================================»%

\section{Overall Database Scalings}

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{images/correlations/all_db_1.png}
  \caption[Database Scalings \#1]{Scalings present in the entire database. Shots are divided by divertor configuration, with horizontal configuration shots in green triangles, corner in red circles, and vertical in blue crosses. Continues in \cref{fig:all_db_2}.}
  \label{fig:all_db_1}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{images/correlations/all_db_2.png}
  \caption[Database Scalings \#2]{Scalings present in the entire database. Shots are divided by divertor configuration, with horizontal configuration shots in green triangles, corner in red circles, and vertical in blue crosses. Continuation of \cref{fig:all_db_1}.}
  \label{fig:all_db_2}
\end{figure}

When looking at trends using all shots in the entire validated database, no clear correlations are expected since the density profile depends in a complex way on the different scanned parameters. In particular, parameters such as the divertor configuration strongly impact the \ac{sol} and pedestal characteristics, hiding trends that otherwise may be clear. However, some trends are still visible when looking at all shots, as seen in \cref{fig:all_db_1}.

The \ac{sol} width presents a clear negative correlation with the heating power, but no obvious correlation with the fuelling rate, in spite of the clear trends in individual scans. The trends are slightly clearer in the case of the separatrix density, which exhibits an increase with the fuelling rate as expected, although with a substantial scatter.

As demonstrated in the different plots in \cref{fig:all_db_1}, it is generally found that reduced SOL transport (narrow SOL width) is  associated with a large stored energy.
This reduced transport can be achieved by a low collisionality (low fuelling rate or high input power), a short connection length (high plasma current), or even by operating in a corner configuration, likely due to the better pumping. SOL transport appears therefore to have an impact on the overall plasma behaviour.
This result highlights the difficulties in operating at low collisionalities, which is associated with large heat loads into the divertor, not only due to the higher power entering the SOL but also due to the narrow SOL width. In contrast, lower stored energy plasmas are associated with a broad SOL and pedestal widths and a high separatrix to pedestal density ratio.

Using the ratio of the fuelling rate to the heating power as a proxy to the collisionality, it is possible to conclude that collisionality increases the SOL and pedestal widths, the separatrix density, separatrix to pedestal density ratio, and degrades plasma stored energy. The SOL and pedestal widths are well correlated with the separatrix to pedestal density ratio.
The pedestal height also appears to have some negative correlation with the SOL width for the corner and horizontal target configurations, but not for the vertical target configuration.
A good positive correlation is also found between the pedestal density and the separatrix density.
Changes in the density profile appear therefore to influence similarly the pedestal and SOL regions.

A model to explain the pedestal width based on the ionisation of neutrals, \ac{npm}, has been proposed \cite{groebner_role_2002}. This model predicts that the pedestal width is related to the pedestal height with an inverse correlation $W = k/h$. However, this trend is marginally seen in our dataset, although with a significant scatter. There is, however, a strong correlation between the pedestal height and its slope, as seen in \cite{schneider_characterization_2012}, which is likely due to the lack of correlation with the pedestal width.
Discharges with high pedestal density are associated with a large pedestal slope and a slightly narrow pedestal, which may be explained by improved pedestal stability and reduced edge turbulent transport.

%«================================================================================»%






