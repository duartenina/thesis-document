\section{Databases \& Scans} \label{sec:databases}

\subsection{Why a Database and What to Store}

\begin{table}
    \centering
    \begin{tabularx}{\linewidth}{lXrr}
        \toprule
        Parameter                  & Description                                                                                   & Default  & Unit \\
        \midrule
        \emph{shot}                & shot number                                                                                   & N/A      & N/A  \\
        \emph{start} \& \emph{end} & time window                                                                                   & N/A      & s    \\
        \emph{tags}                & text field that is used to store information such as the scan(s) the shot belongs to          & filename & N/A  \\
        \emph{kg10ppf}             & field with \emph{user}/\emph{seq} defining the PPF used for the KG10 signals                  & JETPFF/0 & N/A  \\
        \emph{elminfo}             & field that has the non-default settings for the ELM characterisation (see \cref{sec:elminfo}) & :::::::  & N/A  \\
        \bottomrule
    \end{tabularx}
    \caption{"shot" database parameters.}
    \label{tbl:shot_db_parameters}
\end{table}

All the data needed to define the different shots to be analysed, and all the data obtained from those analyses, needed to be stored and organised to be able to do further studies. To organise all the information available and required, three levels of databases were developed, to be used at different steps of the process: "shot", "plasma" and "results" databases. All databases were stored in a \ac{csv} format in separate folders.

The first level is the "shot" database. This level is meant to define shots, their windows and any other information necessary to obtain the same results. The parameters stored are shown in \cref{tbl:shot_db_parameters}; some of the parameters are optional with their defaults stated.

The next level is the "plasma" database. This level is used as a stepping stone between the other two database types, usually to analyse the plasma conditions of a shot of interest and check if it can be used in the studies needed. It has all the parameters of the "shot" database plus the plasma parameters shown in \cref{tbl:plasma_db_parameters}. It contains both the plasma parameters that define the conditions of the experiment, for instance the plasma current or the heating power, and also more complex parameters, such as plasma energy or \ac{taue}, that can be used to evaluate the plasma performance.

The final, and most important, level of databases is the "results" database. In this database is stored, along all the previous parameters from the "shot" and "plasma" databases, the summary of the information obtained with the fitting step. All the parameters stored are presented in \cref{tbl:results_db_parameters}. The first half of the information describes where the data used can be found, which model was used (see \cref{sec:models}) and how many data points were available and used. The remaining information is the average of the physical parameters obtained from the fits and their standard error ("err\_" prefix). These databases are the ones used for scaling laws and other analyses.

\input{results/table_plasma_pars.tex}

\input{results/table_results_pars.tex}

\subsection{Selection of shots and scans}

To analyse how a particular parameter influences the plasmas, it is necessary to study groups of shots where all other physical quantities have the minimum variation possible, and thus reducing their impact on any variations of the profile parameters. These groups are usually called parameter scans, for example, current scans or heating power scans. In this work, scans were defined in one of two different ways: by using existing scans or by creating a new scan. There are existing scans in the literature, both those that were created \textit{a priori} while developing new experiments and studies; and those defined after the experiments were realised, normally by selecting shots with similar plasma parameters.

In several cases, it was not possible to use existing scans of a particular parameter, mostly due to the shots being outside the optimal plasma conditions, for instance, with low enough magnetic fields that the reflectometer could not measure the complete edge density profiles. It was then necessary to create new scans, a process with several steps that tries to find a grouping of experiments with the necessary plasma conditions and good quality reflectometer data. To begin, a large database of potentially interesting shots was created. One or more windows of stability were selected for each shot and a "plasma" database was generated. This database was then used to slowly restrict physical parameters (e.g, only shots with corner configuration) until a group of about four to ten shots was obtained, where only a single plasma parameter has a large variation. This method can create useful scans but requires close attention to the restrictions chosen and can take many iterations until a satisfactory scan is finalised. For parameters that are harder to isolate, particularly the plasma current, other parameters might still vary, and thus the scan requires extra care when analysing the results. From a larger set of scans, a smaller selection was done to present in this work, chosen for their data quality and robustness.

% \section{Database description}

% \input{results/table_scans.tex}

% \Cref{tbl:all_scans} shows an overview of the parameters of all the scans used during the development of this work, which are listed in the next section. The second column has both the number of shots used in the analysis of \cref{sec:discussion} and the total number of shots in the complete database. Most scans do not use all shots due to the aforementioned problems, including some scans that have no valid shots at all.

% There is a good spread of parameters being studied, the most common being the fuelling rate. From these gas scans, it is also possible to have an insight on other parameters, by comparing the behaviour of multiple gas scans with different parameters, such as current, simultaneously. Thus, it was possible to do studies on fuelling, seeding, divertor configuration, current, heating power and triangularity.

% Each scan has a straightforward name. The first part states what parameter is being studied, while the number is split into two digits: the first refers to which database the shots are included (0 for pre-upgrade, 1 for post-upgrade) and the second is just an increasing index.

% \section{Selected Scans} \label{sec:selected_scans}

% \input{results/scans.tex}

