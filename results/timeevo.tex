\chapter{Temporal dynamics of the edge density profiles} \label{sec:time_evo}

\section{Introduction}

The energy transported from the pedestal by ELMs imposes significant operational constraints on the divertor of next-generation tokamaks like ITER. ELMs typically affect a region of the plasma that extends several times the width of the pedestal, with similar impacts on both temperature and density perturbations \cite{leonard_survey_2006,laggner_divertor_2018}.

The complex spatial structure of an ELM and its short time scale have motivated upgrades over the time in several diagnostic systems, aiming to capture measurements with both high spatial and temporal. The duration of the ELM instability and the associated pedestal collapse occurs in time scales well below the millisecond. Numerous fluctuations are observed throughout the ELM cycle, with particular interest in precursor signals that may either contribute to or trigger the burst of magnetic fluctuations and enhanced transport associated with an ELM event. Extensive experimental studies on fluctuations localised at the pedestal preceding ELM events have shown that the onset of these inter-ELM fluctuations correlates with changes in pedestal gradient evolution (e.g., \cite{laggner_inter-elm_2019}).

The multiband reflectometry system used on the JET tokamak, which enables high-temporal-resolution density profile measurements, offers valuable insights into the evolution of electron density profiles over the ELM cycle. This chapter seeks to assess the capabilities of the JET reflectometer in capturing transient behaviour in density profiles throughout the ELM cycle, potentially enhancing our understanding of the ELM dynamics. The parameters derived from the fits to the density profiles will be used to characterise the temporal dynamic of the density profile along the ELM cycle.

This chapter provides a glimpse into the range of observations and analyses possible using the data reflectometry. The chapter begins by illustrating the typical evolution of the density profile along the ELM cycle. Then, the methods used to fit the profiles and its validation are presented, including a discussion on selecting an appropriate density profile model. It then examines in detail the density profile evolution during an \ac{elm}. Next, a more complex scenario with varying \ac{elm} frequency is analysed to highlight differences across various profile parameters. To conclude, the chapter explores the impact of several factors on the \ac{elm} cycle, such as divertor configuration, triangularity, and gas fuelling rate.

\section{Evolution of the density profiles}

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{images/timeevo/example_raw_prof.png}
  \caption[Contour Plot of the JET Reflectometer Density Profile]{Contour plot of the temporal evolution of the density profiles obtained with the JET reflectometer during an ELMy period.}
  \label{fig:example_raw_prof}
\end{figure}

Before deriving the density profile fitting parameters, it is helpful to gain an overall perspective on the profile evolution throughout the ELM cycle by directly analysing the density profiles. The \ac{jet} reflectometer can measure density profiles at a very high temporal resolution, high enough to be able to investigate in detail \acp{elm} as illustrated in \cref{fig:example_raw_prof} displaying the contour plot of the temporal evolution of the density profiles during a period with two \acp{elm}. A discharge with low frequency \acp{elm} and with the reflectometer operated with a high sweep rate (15 us) was selected. The first notable aspect is the complex temporal dynamics of the edge electron density characterised by significant fluctuations along the entire ELM cycle, even in the inter-ELM period that is often assumed to be characterised by low turbulence levels. The two \acp{elm} are clearly seen, around 47.14s and 47.18s, associated with a noticeable widening of the density pedestal. While there appear to be some problematic profiles, like the sharp peak around 47.135s that may be associated with an incorrect initialization, the potential of this diagnostic for studying the behaviour of the density profile in time is obvious.

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{images/timeevo/example_raw_prof_contours.png}
  \caption[Contour Plot of Density, Gradient and Scale Length]{Contour plot of the temporal evolution of the densities (top), gradients (middle) and scale lengths (bottom) from the data smoothed in windows of around $1 ms$.}
  \label{fig:example_raw_prof_contours}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=.5\textheight]{images/timeevo/example_raw_prof_evo_3D.png}
  \caption[3D Temporal Evolution of the Density Profile]{Temporal evolution in 3D of the smoothed density profile (smoothing in the order of the $10 ms$) during an \ac{elm}.}
  \label{fig:prof_evo_3D}
\end{figure}


\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{images/timeevo/example_raw_std_dev.png}
  \caption[Contour Plot of Density Mean and Deviations]{Contour plot of the temporal evolution of the non-smoothed density profiles (top), mean densities (middle) and standard deviations of densities (bottom), calculated in time windows of $1 ms$.}
  \label{fig:example_raw_std_dev}
\end{figure}

It is important to note that even the data presented in \cref{fig:example_raw_prof} required some processing of the density profiles, as each profile from \ac{kg10} has its own position and density ranges. The first step was thus to create a more structured version of the data, by selecting a range of radii and calculating the density at each radius and time by interpolation of the original profiles. It is also possible to smooth this data, to focus on the average effects of the \ac{elm}. This smoothing was performed with a Savitzky–Golay filter along the time axis, for every radius. The  analyses presented in the next sections used different windows for the filter, which can be found in the figure captions.

To further characterize the ELM dynamics, the evolution of profiles of the density, the density gradient and the density gradient length is presented in \cref{fig:example_raw_prof_contours}. For clarity in calculating derivatives, the density profiles were smoothed over a timescale of $1 ms$. As illustrated, the ELM crash is characterised by a collapse of the pedestal density, decrease of the density gradient and increase of the density gradient length across the entire edge region in a timescale below 1 ms. The recovery of the density profile occurs at different time scales depending on the radial location. It is very fast in the SOL (millisecond timescale) but can take more than 10 ms in the pedestal top and radially further inside. After this profile recovery, no significant changes are seen up to the next ELM (for about 25 ms) in either the density profile or its gradients. However, the effects of the density fluctuations are seen throughout the ELM cycle. The profile evolution along the ELM cycle can also be seen in the 3D representation of smoothed (windows of around $10 ms$) density profiles, as shown in \cref{fig:prof_evo_3D}.

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{images/timeevo/example_raw_prof_zoom.png}
  \caption[Contour Plot of the Density Profile during an ELM Crash]{Contour plot of the temporal evolution of density profiles during an ELM crash.}
  \label{fig:example_raw_prof_zoom}
\end{figure}

Swept reflectometers are typically dedicated to radial density profile measurements. However, the high temporal resolution of present-day diagnostics allows for the determination of the plasma density fluctuation and its evolution in time.
To evaluate potential evolution in density fluctuations, the standard deviation of the density at each radial location is calculated using 1 ms time windows. This approach should serve as a proxy for local density fluctuations within a frequency range of 1 to 33 kHz, considering the profile sweep rate of 15 $\mu$s. As illustrated in \cref{fig:example_raw_std_dev}, density fluctuations are reduced during the ELM crash, recovering in a timescale of about 10 ms and then also remaining roughly unchanged up to the next ELM. Density fluctuations are largest near the pedestal top as this is a particularly sensitive region where the density gradient changes rapidly. This could be a real effect due to the plasma turbulence but may also be influenced by the uncertainties in the density profile initialization. The latter would influence mainly the steep gradient region that does not often appear to be the case.

Finally, \cref{fig:example_raw_prof_zoom} displays a zoom of the data presented in \cref{fig:example_raw_prof} around the beginning of the ELM event ($\approx (47.137 - 47.142)$ s) for a more detailed analysis of the pedestal collapse. As illustrated, the evolution of the density profiles suggests a propagation of the ELM perturbation starting in the steep gradient region and propagating to the core and SOL with a velocity in the order of 100 m/s inside the pedestal top and much  faster in the SOL, particularly in the near SOL. Large density fluctuations with different times scales are clearly visible before the ELM that are then reduced during the ELM crash.

%«=========================================================================================================================================»%

\section{Density Profile Fitting Method}

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{images/timeevo/example_plasma_pars.png}
  \caption[Temporal Evolution of Plasma Parameters]{Temporal evolution of different plasma parameters.}
  \label{fig:plasma_pars}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{images/timeevo/example_profile_pars.png}
  \caption[Temporal Evolution of Density Profile Parameters]{Temporal evolution of the different density profile parameters.}
  \label{fig:profile_pars}
\end{figure}

Fitting the electron density profiles using the methods outlined earlier in this thesis is expected to enable a detailed investigation into the effects of an \ac{elm} on various density profile parameters, such as pedestal width and height, with high temporal resolution. As demonstrated in the previous section, most parameters exhibit an expected pattern along the ELM cycle with a rapid variation (ELM crash) followed by a gradual return to a stable value (recovery).

The different models introduced in \cref{sec:models} can be used to fit the density profiles. However, as shown before the MTanh\^{}Exp and TwoLine\^{}Exp methods are of particular interest. A methodology was developed to more accurately study the evolution of fitting parameters throughout the ELM cycle. With high temporal resolution profiles available, a single representative \ac{elm} period is typically selected. The density profiles are then fitted across the ELM cycle, allowing for the derivation of the evolution of the fitting parameters. Due to significant scatter in this evolution, the data is subsequently smoothed using a Savitzky–Golay filter. Relevant main plasma parameters with high enough temporal resolution to resolve ELMs, such as the beryllium and D-alpha emission, stored energy and edge line-averaged density were smoothed with a simple rolling average and used as reference for the study of the evolution of density profiles.

\Cref{fig:plasma_pars} illustrates the evolution of key main plasma parameters considered in this study along the ELM cycle. In each case, an ELM crash is clearly identifiable, though the specific characteristics vary and will be explored in detail later in this chapter. For consistency, beryllium emission from the outer regions --- commonly used as an \ac{elm} detection diagnostic --- served as reference for the analyses. Plasma energy was selected over diamagnetic energy for our study for its superior data quality.

\Cref{fig:profile_pars} presents the evolution of key parameters derived from the density profile fits as an illustrative example. Despite significant scatter in the derived parameters, the averaged data reveal a consistent trend across the ELM cycle. As discussed in previous chapters, data for absolute positions, specifically \ac{R0} and \ac{rsol}, is less reliable and shows greater scatter than any apparent parameter changes; hence, these values were excluded from the analyses. Additionally, it is important to note that all parameters exhibit substantial error bars, particularly in the \ac{sol} width and separatrix density.

\begin{table}
  \centering
  \begin{tabular}{llr}
    \toprule
    Parameter             & Description                                                                & Units \\
    \midrule
    $v_{stable}$          & median value in stable times                                               & a.u.  \\
    $v_{peak}$            & maximum value of the crash                                                 & a.u.  \\
    $t_{peak}$            & time of the peak                                                           & s     \\
    $\Delta t_{crash}$    & time between the start of the crash and its peak                           & ms    \\
    $\Delta t_{recovery}$ & time between the peak of the crash and the return to the stable value      & ms    \\
    $k_{c}$               & magnitude of the crash, calculated as $(v_{peak} - v_{stable})/v_{stable}$ & \%    \\
    \bottomrule
  \end{tabular}
  \caption[Temporal Evolution Parameters]{Overview of the different parameters describing the \ac{elm} crash and recovery.}
  \label{tbl:time_evo_pars}
\end{table}

To determine the typical timescales associated with the ELM and the variations in different quantities, several additional steps are applied. First, a median value is calculated from data during a stable period just before the ELM to serve as a reference ($v_{stable}$). A peak-finding algorithm \cite{scipy} is then used to identify the peak instant, from which the extreme value is measured ($v_{peak}$). To establish the duration of the crash and recovery phases, a threshold value --- typically set to the deviation from the median --- is manually selected. The start of the crash and the end of the recovery are defined to the instant where the smoothed parameter crosses this threshold. The parameters extracted are summarised in \cref{tbl:time_evo_pars}. The values obtained through this algorithm are shown in \Cref{fig:plasma_pars} and \Cref{fig:profile_pars}.

\subsection{Validation of the analysis method}

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{images/timeevo/example_reasons.png}
  \caption[ELM Reasons of Invalidity]{Reasons of invalidity during a stable time (left) and during an ELM (right). The beryllium flux signal is shown at the top to identify the ELM.}
  \label{fig:example_reasons}
\end{figure}

An important factor to consider in these analyses is how accurately the models represent the actual density profiles during an \ac{elm}. These models were specifically developed and optimised for a particular density profile shape, namely the \ac{hmode}, inter-ELM profiles characterised by a steep pedestal. Applying these models throughout the \ac{elm} cycle, or to an \ac{lmode} profile, may be less suitable and could lead to a lower fitting success rate compared to their application during stable inter-ELM periods.

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{images/timeevo/example_profile_pars_combined.png}
  \caption[Temporal Evolution of Density Profile Parameters]{Temporal evolution of the different density profile parameters.}
  \label{fig:profile_pars_combined}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{images/timeevo/example_raw_line_plot.png}
  \caption[Temporal Evolution of Density, Gradient and Scale Length]{Temporal of evolution of the densities (top), gradients (middle) and scale lengths (bottom) at fixed positions, for both raw (solid lines) and fit (dotted and dashed lines) data.}
  \label{fig:example_raw_line_plot}
\end{figure}

\Cref{fig:example_reasons} illustrates the reasons for fit invalidity (see \cref{sec:code}) during two periods: a stable time before the ELM (left) and during an \ac{elm} (right), with the beryllium signal shown at the top for reference. While the overall fraction of valid profiles only slightly decreases from 65\% before the ELM to 57\% during the ELM, a distinct temporal effect is evident. Specifically, nearly no valid profiles are observed for approximately one millisecond immediately after the ELM peak. Most of these profiles were filtered out and marked as \textbf{NOT\_ENOUGH\_DATA}, largely due to assumptions about the location of the steep density region and issues with profile reconstruction. This effect is also apparent in \cref{fig:profile_pars}, where data gaps, especially noticeable in the pedestal density, are visible.

An additional step in validating the density profile fitting methods involves comparing results from the two models under consideration. \Cref{fig:profile_pars_combined} displays the evolution of the derived fitting parameters for both models after applying a smooth, which show similar trends with only minor differences. The variations in pedestal width and gradient align with the discussion in \cref{sec:model_comparison}. The difference in pedestal centre stems from the width differences, as both models agree on the pedestal top position. This variation in the pedestal centre also affects the separatrix density, which is calculated based on both pedestal centre and width. A data gap in \acl{rsol} during the \ac{elm} results from how each model identifies the exponential joining location. The TwoLine model, with a simple straight line intersecting an exponential, generally yields a single solution. In contrast, the MTanh model, due to its more complex shape, often has multiple potential intersections, especially during an \ac{elm}, leading to occasional fitting ambiguities. The MTanh\^{}Exp is the chosen model for the rest of the chapter due to the same reasons as the previous chapter (discussed in \cref{sec:models}), namely familiarity --- both in this work and in the literature --- and a small advantage in fit quality and robustness.

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{images/timeevo/example_prof_evo_2D.png}
  \caption[2D Temporal Evolution of the Density Profile]{Temporal evolution in 2D of the density profile during an \ac{elm}. A selection of representative profiles is shown stacked, with the original profiles on top, and profiles from the smoothed parameters (with a fixed equal \Ac{R0}) seen in \cref{fig:profile_pars} in the middle. Some of the temporal evolutions of profile parameters is shown at the bottom, with vertical lines indicating the times of the profiles shown, and large black dots to show the specific profile.}
  \label{fig:prof_evo_2D}
\end{figure}

As a final validation step, it is useful to compare the results obtained from the fits with those directly derived from the density profile evolution, ensuring that both approaches show similar behaviour and trends. \Cref{fig:example_raw_line_plot} presents the temporal evolution of densities (top), density gradients (middle), and density gradient lengths (bottom) at fixed radial positions. Both the raw density profile data (solid lines) and the fit parameter data (dotted and dashed lines) are displayed for key radial locations: the pedestal top (R0-d), the steep gradient region (R0), the separatrix (R0+d), and the near SOL (R0+2d). Here, density gradient length serves as a proxy for the local width of the density profile, aiding in estimating both the pedestal and SOL widths. Overall, there is good agreement between the quantities derived directly from the density profiles and those obtained from the fits. However, the fitted data does not exhibit peaks as high as those in the raw data at the two outermost radii, primarily due to fitting challenges, which will be further explored in the next section.

\Cref{fig:prof_evo_2D} presents evolution of the density profiles along the \ac{elm} cycle, both the original density from the reflectometer (on top) and as well as profiles created from the MTanh\^{}Exp model using the smoothed fit parameters seen in \cref{fig:profile_pars} (in the middle). The temporal evolution of the most relevant parameters is shown at the bottom, providing information context for the \ac{elm} phase during which the profiles were captured, as indicated by the large black dots. Note that the fit profiles have a fixed \ac{R0} as the scatter  in the absolute position can make it harder to visualise the changes in the profile. \Cref{fig:prof_evo_3D} is both a heatmap of density along time and position, and a 3D representation of the raw profile in time. Note that the profile was smoothed with a large window to make the 3D plot clearer.

%«=========================================================================================================================================»%

\section{Intra-ELM Behaviour}
\label{sec:intra_elm}

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{images/timeevo/timeevo_baseline.png}
  \caption[Temporal Evolution of Parameters Ordered by Start Time]{Temporal evolution of the most relevant parameters for a shot with good quality data (\#86538), ordered by the crash start time.}
  \label{fig:timeevo_baseline}
\end{figure}

With this technique, it is interesting to now turn to the study of the evolution of the main parameters derived from the density profile fits. \Cref{fig:timeevo_baseline} shows the crash and recovery of the chosen parameters of a shot with good quality data, ordered by the time of crash for each parameter. The timeline of an \ac{elm} becomes easy to follow just by going down the rows of parameters. First, the profile widens, both in the pedestal and the \ac{sol}, with the weakening of the confinement. The pedestal density starts decreasing right after, followed soon by the plasma energy, as particles escape confinement. Finally, the escaped particles hit the walls and beryllium impurities are released. Note that the temporal resolutions for other signals, such as the plasma energy and particularly the edge line density, are lower, which means the start time is detected with worse precision.

The separatrix density has a less clear behaviour, and it can be better understood by looking back at \cref{fig:example_raw_line_plot} and at \cref{fig:example_raw_prof_contours}. Deeper within the plasma ($R << R_0$), the density exhibits a clear drop followed by a slow recovery. Conversely, further into the \ac{sol} ($R >> R_0$), the density increases, with a relatively faster recovery. Intermediate positions display a more complex behaviour, combining aspects of both effects but with smaller magnitudes. Typically, the separatrix lies between $R_0$ and $R_0 + 2\cdot d$, meaning it can vary depending on its relative position to the density profile. Regarding the density gradient, the pedestal region experiences a significant drop, while the regions on either side see much smaller changes, with a brief increase in the \ac{sol}.

Upon analysing the time scales associated with the ELM event, it is possible to conclude that most parameters exhibit similar durations for the crash, around 4 ms. However, there are notable exceptions: the beryllium emission undergoes a very brief crash, while the crash in stored energy takes longer.
Recovery times for the various parameters are quite distinct. Parameters related to the pedestal and SOL width recover much faster than those associated with the pedestal top density and plasma stored energy. The pedestal height does not seem to reach saturation, continuing to increase until the next ELM is triggered. This is likely due to the crossing of a density/pressure limit.
Regarding the relative variations in plasma parameters, these also show different behaviours between the pedestal and SOL width compared to the pedestal top density. The widths exhibit a much larger variation, on the order of 50\%. The stored energy drops by about 2\%, while the ELM causes a loss of approximately one-quarter of the pedestal top density.

The information presented here allows for a detailed assessment of the temporal and spatial evolution of the density profile associated with the ELM. This includes studying the rapid collapse of the pedestal and the subsequent slower recovery of the profile. Additionally, a fast broadening of the pedestal and \ac{sol} widths is observed, followed by a quick return to their pre-ELM values. A key feature of the density profile is a pivotal point near the separatrix, where opposite trends are evident on either side.

%«=========================================================================================================================================»%

\section{Inter-ELM Behaviour}
\label{sec:inter_elm}

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{images/timeevo/intra_inter_window.png}
  \caption[Intra and Inter ELM Behaviour]{Temporal evolution of selected parameters during a shot (\#84804) with varying frequencies. Gray dots show the data points, solid red lines the intra-ELM averages (10-point) and dashed black lines the inter-ELM averages (100-point). Two windows of interested are shown enlarged in the bottom row.}
  \label{fig:intra_inter_window}
\end{figure}

The \ac{elm} frequency in most shots remains relatively stable during the window of interest under steady plasma conditions. However, there are cases where the \ac{elm} frequency exhibits significant changes even within a short timeframe. \Cref{fig:intra_inter_window} illustrates one such example, showing a variation in the \ac{elm} frequency by up to a factor of three. The plot displays the temporal evolution of the beryllium emission signal, the \ac{elm} frequency, the pedestal density, and the pedestal and \ac{sol} widths over a window of approximately half a second. The data derived from the fits is shown in grey, complemented by intra-\ac{elm} averages (10 points) and inter-\ac{elm} averages (100 points) for additional clarity. Enlarged views of two regions with significant \ac{elm} frequency variation are provided at the bottom. Additionally, the edge line-averaged density is displayed alongside the pedestal top density. The signal for the \ac{nbi} heating is also shown, as it was the plasma parameter with the most variations during this period, and thus the most likely cause of the changing \ac{elm} frequency.

As discussed in the previous section, during each \ac{elm}, the density profile exhibits a rapid crash followed by a slower recovery, with recovery times varying significantly. During periods of low \ac{elm} frequency, all parameters have sufficient time to recover to their pre-\ac{elm} values, resulting in overall stable parameter averages, as reflected in the inter-\ac{elm} data. In this regime, plasma parameters remain approximately stationary on timescales longer than the \ac{elm} period. However, as the \ac{elm} frequency increases, the plasma parameters have less time to recover, often failing to return to their pre-\ac{elm} values. This effect is particularly evident in the pedestal top density, which exhibits the slowest recovery.
In the bottom left panel, it is clear that as \acp{elm} become more frequent, the pedestal top density progressively decreases, despite the crashes having similar magnitudes. This leads to a reduction in the inter-\ac{elm} average density. In the bottom right panel, when \acp{elm} occur at very high frequencies, the time between events becomes so short that neither the pedestal nor the \ac{sol} widths have enough time to fully recover. Consequently, both show increases in their inter-\ac{elm} averages. High-frequency \ac{elm} phases are thus characterised by a reduction in pedestal top density and an increase in both pedestal and \ac{sol} widths, indicating a degradation in particle confinement.
At very high \ac{elm} frequencies, the density profiles exhibit smaller variations throughout the \ac{elm} cycle and approach conditions similar to those at the \ac{elm} crash in low frequency ELMs.

%«=========================================================================================================================================»%

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{images/timeevo/timeevo_long_elm.png}
  \caption[Temporal Evolution of Parameters in a Long ELM]{Temporal evolution of the most relevant parameters for a shot with a long ELM.}
  \label{fig:timeevo_long_elm}
\end{figure}

\section{Long ELMs --- Transitory L-Modes}
\label{sec:timeevo_long_elms}

The evolution of the density profile throughout the \ac{elm} cycle under different plasma conditions is also an interesting topic. While most \acp{elm} exhibit behaviour similar to what was described earlier, some \acp{elm} deviate from this pattern. One such example is presented in \cref{fig:timeevo_long_elm}, showcasing a "long" \ac{elm}, identifiable by the extended tail in the beryllium emission signal. During this event, the pedestal width and gradient enter a phase lasting approximately 20 ms with nearly constant values, indicating that the plasma reaches a quasi-stable state, likely corresponding to an \ac{lmode}. The \ac{sol} width also stabilises at a larger, nearly constant value.
Notably, while the width and gradient remain constant during this stage, the pedestal density continues to decrease until the phase concludes. This is accompanied by a drop in plasma energy, providing further evidence that this type of \ac{elm} induces a back-transition to \ac{lmode}. Comparing this case, where approximately 14\% of the plasma energy is lost, to the example in \cref{sec:intra_elm}, which exhibits a significantly smaller energy loss of around 2\%, highlights the pronounced changes associated with these "long" \acp{elm}.

%«=========================================================================================================================================»%

\begin{table}
  \footnotesize
  \begin{tabularx}{\linewidth}{Xllcllr}
    \toprule
    Shot & \acs{ip} (MA) & \shortstack{D2 Rate \\ ($\times 10^{22}$ s$^{-1}$) } & \acs{divout} & \acs{triu} & $P_{heat}$ (MW) & \shortstack{$\Delta t_{\text{ELM}}$ (ms) \\ before, after} \\
    \midrule
    H tile       &  2.48  (+0\%) &  2.16    (+0\%) &    5    &  0.16   (-7\%) & 16.22  (+0\%) &   53,  53  \\
    C tile (B)   &  2.48  (+0\%) &  2.17    (+0\%) &    6    &  0.17   (+0\%) & 16.21  (+0\%) &  103,  74  \\
    V tile       &  2.47  (+0\%) &  2.16    (+0\%) &    7    &  0.16   (-5\%) & 16.17  (+0\%) &  106,  44  \\
    \midrule
    Low Triu (B) &  2.47  (+0\%) &  4.29    (+0\%) &    6    &  0.14   (+0\%) & 23.57  (+0\%) &   33,  34  \\
    High Triu    &  2.47  (+0\%) &  4.27    (+0\%) &    6    &  0.27  (+89\%) & 23.68  (+0\%) &   52,  46  \\
    \midrule
    Low Gas (B)  &  2.49  (+0\%) &  2.11    (+0\%) &    6    &  0.10   (+0\%) & 16.19  (+0\%) &   44,  42  \\
    High Gas     &  2.48  (+0\%) &  3.11   (+47\%) &    6    &  0.09   (-7\%) & 16.43  (+1\%) &   15,  14  \\
    \bottomrule
  \end{tabularx}
  \caption[Plasma Parameters of Shots]{Overview of the most relevant plasma parameters for all the shots used in this section, separated by scans. Values in parentheses show the variation of that parameter to the baseline shot of the scan marked with (B).}
  \label{tbl:timeevo_shots}
\end{table}

\section{Impact of Plasma Parameters}

It is valuable to investigate whether key plasma parameters influence \acp{elm} properties, particularly their effect on the density profile. To this end, shots with varying plasma parameters were selected, as summarised in \cref{tbl:elm_info_parameters}. The analysis focused on shots where the reflectometer operated at a sufficiently high acquisition rate and the \ac{elm} frequency was low enough, which excluded many shots discussed in \cref{sec:discussion}. Despite these constraints, a smaller subset of shots was identified to explore the impact of specific plasma parameters. \Cref{tbl:timeevo_shots} lists the most relevant plasma parameters for the selected shots. Note that the \ac{elm} frequency is not constant during each shot and only one \ac{elm} cycle is being analysed per shot, with the major factor of choice being data quality. This means that the \ac{elm} durations pictured should not be taken as the average \ac{elm} frequency of the plasma. In the following section, the influence of these parameters will be assessed, and the temporal evolution of the density profile will be qualitatively compared across the shots. Three parameters will be analysed: the outer divertor configuration, the triangularity, and the gas fuelling rate.

\subsection{Divertor Configuration}

The first step was to study experiments with the outer divertor strike-point at different positions: horizontal target; vertical target; and in the corner configuration (between the horizontal and vertical targets close to the cryopump opening). The temporal evolution of the parameters of three shots with different divertor configurations is presented in \cref{fig:timeevo_divert_combined} with the horizontal configuration in solid orange lines, corner in dashed red lines and vertical in dotted brown lines. Data was normalised to its initial pre-ELM value, defined by the average in the stable time, and time was normalised to the \ac{elm} period.
Representative \acp{elm} are selected for each divertor configuration.
The most important distinction in the different shots is the shape of the \acp{elm} as seen particularly in the beryllium emission: the corner configuration has a sharper peak, with the vertical configuration having the longest ELM perturbation. The absolute times for the recovery of the ELM signal associated with the beryllium signal is around 5.5 ms for the corner configuration, 9.3 ms for the horizontal and 13.3 ms for the vertical.

Modifications in the divertor configuration are often associated with changes many geometrical parameters as well as in the pumping efficiency and recycling, which will impact in a complex way on the ELM properties.
As expected, the vertical configuration characterised by a longer \ac{elm} results in larger losses and overall stronger effects on the density profiles. On the contrary, the effect of the \ac{elm} appear to be smaller for the corner configuration that is also associated with the fastest ELM crash.

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{images/timeevo/timeevo_divert_combined.png}
  \caption[Temporal Evolution in Different Divertor Configurations]{Temporal evolution of selected plasma and density profile parameters during three different shots with similar plasma configuration, and different outer divertor tiles. Data was normalised both in time, according to the ELM cycle position, and in value, with the average value in stable times becoming 100\%.}
  \label{fig:timeevo_divert_combined}
\end{figure}

% \subsection{Gas Seeding}

% \begin{itemize}
%   \item \warning{not sure what to say, maybe remove seeding?}
%   \item more energy lost in the no seeding shot, though it recovers to similar values
% \end{itemize}

% \begin{figure}
%   \centering
%   \includegraphics[width=\linewidth]{images/timeevo/timeevo_seeding_combined.png}
%   \caption[Temporal Evolution With Different Seeding Rate]{Temporal evolution of selected plasma and density profile parameters during three different shots, with similar plasma configuration, and different seeding rates. Data was normalised both in time, according to the ELM cycle position, and in value, with the average value in stable times becoming 100\%.}
%   \label{fig:timeevo_seeding_combined}
% \end{figure}

\subsection{Triangularity}

\Cref{fig:timeevo_triu_combined} shows the temporal evolution of the parameters for the two shots with different triangularity, with low triangularity in solid orange lines and high in dashed red lines.
Overall the triangularity appears to have a modest impact of the ELM characteristics with a relatively larger drop in the pedestal density and a larger increase of the \ac{sol} width seen at high triangularity.
There was a phenomenon seen in the low triangularity shot, a small drop in the plasma energy during the \ac{elm} crash, a few milliseconds before the larger crash. This was observed across all the ELMs of the time window studied here and its origin is still unclear. However, a similar behaviour cannot be detected in the rest of the parameters, which might suggest the density profile is not impacted with the same magnitude.

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{images/timeevo/timeevo_triu_combined.png}
  \caption[Temporal Evolution With Different Triangularity]{Temporal evolution of selected plasma and density profile parameters during two different shots, with similar plasma configuration, and different triangularity. Data was normalised both in time, according to the ELM cycle position, and in value, with the average value in stable times becoming 100\%.}
  \label{fig:timeevo_triu_combined}
\end{figure}

\subsection{Gas Fuelling}

The temporal evolution of the parameters for the two shots in the gas fuelling scan is shown in \cref{fig:timeevo_majr_combined}, with low gas depicted by solid orange lines and high gas by dashed red lines. The most prominent effect of increased gas fuelling on \acp{elm} is a significant rise in \ac{elm} frequency, from an average of 20 Hz in the low gas case to approximately 65 Hz in the high gas case.
Due to the reflectometer not operating at its maximum acquisition rate during the high-gas shot, three consecutive \acp{elm} are displayed to ensure that the overall behaviour is captured. Non-normalised values are presented to clearly highlight the impact of gas fuelling on the edge profile characteristics.

The trends observed in the previous chapter relative to the changes in the density profile are evident here as well: lower pedestal density and gradient, coupled with increased pedestal and \ac{sol} widths at high gas fuelling. The magnitude of the pedestal crash at high gas fuelling is notably smaller, with density and gradient experiencing less pronounced drops. Additionally, the pedestal and \ac{sol} widths also exhibit a smaller increase at the ELM crash at high fuelling. In fact, the increase in \ac{sol} width during the \ac{elm} crash is relatively modest—just above the profile-to-profile variation along the \ac{elm} cycle.
Overall, the main effect of increased fuelling on the density profile is a diminished influence of \acp{elm}, particularly in the \ac{sol}.

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{images/timeevo/timeevo_majr2_combined.png}
  \caption[Temporal Evolution With Different Fuelling Rate]{Temporal evolution of selected plasma and density profile parameters during three different shots. The largest difference between the two triangularity scan shots and the gas scan shot is the gas fuelling rate. Data was normalised both in time, according to the ELM cycle position, and in value, with the average value in stable times becoming 100\%.}
  \label{fig:timeevo_majr_combined}
\end{figure}

%«=========================================================================================================================================»%

% \section{Summary}