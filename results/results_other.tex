\chapter{Temporal Evolution} \label{sec:results_time_evo_old}

\section{Introduction}

The \ac{jet} reflectometer can measure density profiles at a very high temporal resolution, high enough to be able to investigate how the density profile changes during a slower instability like the \acp{elm}. This chapter is focused on the temporal analysis of a small selection of shots, chosen to show some of the different effects that \acp{elm} can have on the plasma. The first shot serves as a baseline, to which the others are compared. \Cref{tbl:shots_time_evo} gives an overview of the shots selected, and the parameter of interest for each comparison is marked in bold.

\begin{table}[h!]
  \small
  \centering
  \input{images/timeevo/shots_time_evo.tex}
  \caption[Shots used in Temporal Evolution]{Overview of the shots used in the parameter temporal analysis. The parameter of interest is marked in bold.}
  \label{tbl:shots_time_evo}
\end{table}

%«================================================================================»%

\section{Method}

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{images/timeevo/example_plasma_pars.png}
  \caption[Temporal Evolution of Plasma Parameters]{Temporal evolution of different plasma parameters.}
  \label{fig:plasma_pars_old}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{images/timeevo/example_profile_pars.png}
  \caption[Temporal Evolution of Density Profile Parameters]{Temporal evolution of the different density profile parameters.}
  \label{fig:profile_pars_old}
\end{figure}

For diagnostics with high enough temporal resolution, it is possible to see the effects of an \ac{elm}. By plotting a parameter of interest, it can be clear how it changes during the instability cycle. Usually, there is a rapid decrease/increase (the crash) followed by a slower return to the stable value (the recovery). For some parameters, the recovery is slow enough that a new \ac{elm} cycle starts before reaching saturation.

To be able to more accurately describe the details, some processing is required. To start, a single \ac{elm} is chosen. As in previous chapters, a median and its deviation are calculated from stable times to serve as the baseline. The data is then smoothed and a peak-finding algorithm \cite{scipy} is run. From it, it is possible to extract various parameters that describe the crash and recovery processes, shown in \cref{tbl:time_evo_pars_old}.

\begin{table}
  \centering
  \begin{tabular}{llr}
    \toprule
    Parameter             & Description                                                                & Units \\
    \midrule
    $v_{stable}$          & median value in stable times                                               & a.u.  \\
    $v_{peak}$            & maximum value of the crash                                                 & a.u.  \\
    $t_{start}$           & time when the crash starts                                                 & s     \\
    $\Delta t_{crash}$    & time between the start of the crash and its peak                           & ms    \\
    $\Delta t_{recovery}$ & time between the peak of the crash and the return to the stable value      & ms    \\
    $k_{c}$               & magnitude of the crash, calculated as $(v_{peak} - v_{stable})/v_{stable}$ & \%    \\
    \bottomrule
  \end{tabular}
  \caption[Temporal Evolution Parameters]{Overview of the different parameters describing the \ac{elm} crash and recovery.}
  \label{tbl:time_evo_pars_old}
\end{table}

\Cref{fig:plasma_pars_old} shows some of the plasma signals that can be analysed in such a way. In all of them, a crash is easily recognisable, though with very different characteristics. As the various inner/outer signals for both Beryllium and D-Alpha have similar behaviours, only one of them, the outer Beryllium, was used in the rest of the analyses. From the two possible system energies, the plasma energy was chosen over the diamagnetic energy, due to better data quality.

\Cref{fig:profile_pars_old} shows most of the relevant density profile parameters calculated with the methods mentioned in past chapters. As this data is more noisy, a Savitzky–Golay filter was applied to further smooth the data. Unfortunately, in most cases, the data for the absolute positions, both \ac{R0} and \ac{rsol}, has a larger spread than any apparent changes and was not used in the analyses.

In the following graphs, the parameters are ordered according to their $t_{start}$, with the earliest on top and the latest at the bottom, giving an intuition of the sequence of events.

%«================================================================================»%

\section{Baseline Shot}

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{images/timeevo/example_prof_evo_2D.png}
  \caption[Temporal Evolution of the Density Profile]{Temporal evolution of the density profile during an \ac{elm}. On the left, a selection of representative profiles is shown stacked, while a 3D projection is shown on the right. These profiles were created with the smoothed parameters seen in \cref{fig:profile_pars}. \Ac{R0} was fixed at the same value for all profiles, as the noise in the absolute position can mask the changes in the profile.}
  \label{fig:prof_evo}
\end{figure}

% \begin{figure}
%   \centering
%   \includegraphics[width=\linewidth]{images/timeevo/shot_86538.png}
%   \caption[Temporal Evolution of Baseline Shot]{Temporal evolution of the parameters of the baseline shot, \#86538.}
%   \label{fig:shot_86538}
% \end{figure}

The baseline shot represents a usual \ac{elm} with particularly good quality data. In \cref{fig:shot_86538}, the crash and recovery of the different parameters are shown. The timeline of an \ac{elm} becomes easier to discern by looking at the different $t_{start}$. First, the pedestal widens, followed by the \ac{sol}, with the weakening of the confinement. The densities, both pedestal height and edge line density, start decreasing after, along with the plasma energy, as particles escape confinement. Finally, the escaped particles hit the walls and Beryllium impurities are released. Interestingly, the \ac{hg} end density, a proxy for the separatrix density (see \cref{sec:sep_dens}), seems to have the latest crash, more than 2 ms after the pedestal width.

The various properties take relatively similar durations to crash (4 to 7 ms), except for the Beryllium signal. However, their recovery times are quite different. Both the pedestal and the \ac{sol} narrow quickly, around 7 or 8 ms, as confinement is restored. It takes longer to recover the lost energy and particles, and in particular, the pedestal height never seems to reach saturation, as another \ac{elm} is triggered before, possibly due to the crossing of a density limit.

Some of the negative effects of the \acp{elm} are also present: the drop in plasma energy is a noticeable 3\% and the pedestal loses about a quarter of its density. The large load on the walls comes from this loss of energy and particles and is also seen in the rapid increase in impurities.

To try and give a more global vision of the effects of an \ac{elm} on the density profile, \cref{fig:prof_evo} shows the evolution of the profile itself. On the left, profiles along the \ac{elm} recovery are presented, while on the right, it is a 3D representation of the profile in time. On both, the \ac{elm} is clear: there is a large drop in the pedestal height, that recovers slowly. It is also possible to observe the increase in pedestal and \ac{sol} widths, that quickly recover to the previous state. It is important to note that densities in the \ac{sol} mostly increase during an \ac{elm}, and the transition point is close to the end of the pedestal and start of the \ac{sol}, and to the separatrix.

%«================================================================================»%

\section{Divertor Configuration --- Horizontal Tile}

% \begin{figure}
%   \centering
%   \includegraphics[width=\linewidth]{images/timeevo/shot_86689.png}
%   \caption[Temporal Evolution of Shot with Horizontal Configuration]{Temporal evolution of the parameters of the shot with a horizontal configuration, \#86689.}
%   \label{fig:shot_86689}
% \end{figure}

There are a few differences between the baseline shot and the shot with a horizontal configuration, presented in \cref{fig:shot_86689}. The crash of the pedestal and \ac{sol} parameters is closer, without the slight delay in pedestal height seen before. The plasma energy also takes considerably longer to crash (about 7 ms after the first), though this effect may be due to noise in the data. The \ac{hg} end density is particularly different, increasing from the stable value while in the baseline shot, it decreased. The data is noisy with a large spread, but the increase does not appear to be fictitious. The closeness to the transition point might explain the apparent ups and downs that this parameter has.

%«================================================================================»%

\section{Long ELM}

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{images/timeevo/timeevo_long_elm.png}
  \caption[Temporal Evolution of Shot with Long ELM]{Temporal evolution of the parameters of the shot with a vertical configuration and a long \ac{elm}, \#86690.}
  \label{fig:shot_86690}
\end{figure}

The shot in this section was chosen not only for having a vertical configuration but also for having an unusual \ac{elm}, distinguished by the long recovery tail of Beryllium. It is clear by looking at the different parameters in \cref{fig:shot_86690} that it does not have the same behaviour as the baseline shot. The flat sections of the pedestal slope and width suggest that the plasma returned to \ac{lmode} during this instability. Both the pedestal height and end density decrease during this period, and the plasma energy drops a much larger 14\%. The \ac{sol} width has a large crash and recovers to an intermediate stage before recovering again. Note that in some parameters, in particular the plasma energy, the $t_{start}$ is marked later because that parameter never reaches the previous stable value, and the start and end of the peak are calculated at the same value.

%«================================================================================»%

\section{High Triangularity}

% \begin{figure}
%   \centering
%   \includegraphics[width=\linewidth]{images/timeevo/shot_85385.png}
%   \caption[Temporal Evolution of Shot with High Triangularity]{Temporal evolution of the parameters of the shot with high triangularity, \#85385.}
%   \label{fig:shot_85385}
% \end{figure}

Unfortunately, the shot with high triangularity has data with more noise than usual, increasing the spread of most profile parameters. However, it is still possible to observe some of the differences. This shot also seems to have a brief period in \ac{lmode}, seen in the pedestal slope and width. The crashes are staggered, with almost 8 ms between the pedestal slope and the height. The \ac{hg} end density fluctuates around its average value, again likely due to the closeness to the transition point. The plasma energy also has a small peak before the crash, during the transition to \ac{lmode}.

%«================================================================================»%

\section{High Heating Power}

% \begin{figure}
%   \centering
%   \includegraphics[width=\linewidth]{images/timeevo/shot_96838.png}
%   \caption[Temporal Evolution of Shot with High Heating Power]{Temporal evolution of the parameters of the shot with high heating power, \#96838.}
%   \label{fig:shot_96838}
% \end{figure}

The final shot selected is the shot with high heating power; and \cref{fig:shot_96838} shows the crash and recovery of its parameters. This shot presents similar behaviours as the baseline shot. Here, there is a less noticeable timeline between the parameters, the \ac{elm} crash propagates faster, possibly due to the stronger heating power. The \ac{sol} width also takes longer to recover. Note that, again, the plasma energy does not recover completely before the next \ac{elm} occurs.

%«================================================================================»%

\section{Comparison of Parameters Change with ELM}

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{images/timeevo/elm_scatters.png}
  \caption[Correlations of Temporal Evolution Parameters]{Correlations between crashes of the different parameters.}
  \label{fig:elm_scatters}
\end{figure}

It is possible to use the results from the crashes and recoveries of the different parameters and shots to create more complex analyses. One is looking at the correlations between the $k_c$ (the percentual change in value) of the various parameters for each shot. \Cref{fig:elm_scatters} shows some of the more interesting correlations found in the selected shots. Note that when the parameter decreases during the crash, $k_c$ is negative, and the axis follows its absolute value. It is important to remember that while some trends can be seen since this selection of shots is not a parameter scan and since there are some shots with unusual behaviours, these trends may disappear with a more comprehensive scan.

There is a strong correlation between the drops in plasma energy and edge line density, as the loss of confinement results in a loss of both particles and power. However, this correlation is not seen with the pedestal height. The pedestal height drop has a strong correlation with the drop in \ac{hg} end density, which suggests that the loss of particles is in both the pedestal and \ac{sol}. The pedestal slope drop correlates well with the pedestal height and width, as expected. It is also correlated with the drop in \ac{hg} end density, which again indicates that the entire profile density decreases.

The \ac{sol} width increase has good correlations with multiple different parameters. The correlation with the drop in plasma energy suggests that the \ac{sol} may be able to absorb some of the energy lost by the pedestal. The correlation with the drop in edge line density suggests a similar factor with the particles lost by the pedestal, and likewise with the drop in pedestal height. The increase in \ac{hg} end density is connected to the increase in \ac{sol} width, as a broader \ac{sol} is able to contain more particles.